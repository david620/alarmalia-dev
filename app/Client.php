<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Config;
use Session;

class Client extends Model{
	protected $table = 'clients';
	protected $connection = 'mysql';
	protected $fillable = ['lang','logo','country','region','address','cp','email','phone','cif','name','acronym','domain_name','date_format','favicon'];

	public static function find($id, $columns = array('*')){

		$client = self::select($columns)->where('id',$id)->first();

		if($client){
			Session::set('model.client',$client);
		}

        if($client && !Config::has('database.connections.dynamic')){
			Config::set('database.connections.dynamic', array(
			    'driver'    => 'mysql',
			    'host'      => 'localhost',
			    'database'  => $client->db_name,
			    'username'  => $client->db_user,
			    'password'  => $client->db_pass ? \Crypt::decrypt($client->db_pass) : '',
			    'charset'   => 'utf8',
			    'collation' => 'utf8_general_ci',
			    'prefix'    => '',
			    'options'   => [
			        \PDO::ATTR_EMULATE_PREPARES => true
			    ]
			));
		}
		return $client;
	}

	public static function findByAcronym($a){
		$id = self::where('acronym',$a)->value('id');
		if($id) return self::find($id);
		return null;
	}

	public static function acronymToId($a){
		return self::where('acronym',$a)->value('id');
	}

	static function getCached(){
		return Session::get('model.client',null);
	}

	function countUsers(){
		return $this->hasMany('App\Models\User','client_id','id')->count();
	}

	function __toString(){
		return $this->name;
	}

	function languages(){
		return array(
			'es' => 'app.spanish',
			'en' => 'app.english',
			'fr' => 'app.french',
			'ca' => 'app.catalan',
			'gl' => 'app.galician'
		);
	}

	function configuration($key){
		return ClientConfig::where('client_id',$this->id)->where('config',$key)->first();
	}

	function saveConfigurations(){
		ClientConfig::saveConfigurations($this->id);
	}

	function checkFS($subfolder = ''){
		$path = base_path() . '/public/client/' . $this->id;
		if(!file_exists($path)){
			@mkdir($path,0777);
		}
		if(!empty($subfolder)){
			$newPath = $path .'/'. $subfolder;
			@mkdir($newPath,0777);

			return $newPath;
		}

		return $path;
	}

	static function toSelector(){
		return self::select('id','acronym','name')->get();
	}

	static function toAdminList($ids = array()){
		return self::select('id','acronym','name','db_name')->whereIn('id',$ids)->get();
	}
	
}