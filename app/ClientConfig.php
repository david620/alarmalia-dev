<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Database\Eloquent\Builder as Builder;

class ClientConfig extends Model{
	protected $table = 'clients_config';
	protected $connection = 'mysql';
	public $primaryKey = false;
	public $incrementing = false;
	protected $fillable = ['*'];

	protected function setKeysForSaveQuery(Builder $query)
    {
      
      	$query->where('client_id',$this->client_id)
      	->where('config', $this->config);

        return $query;
    }

	static function availables(){
		return array(
			'api_public_key' => trans('app.publicKey'),
			'api_secret_key' => trans('app.secretKey'),
			'wp_db' => trans('app.wpDB'),
			'wp_username' => trans('app.wpUsername'),
			'wp_password' => trans('app.wpPassword'),
			'lead_cost_instantly' => trans('contracts.costInstantly'),
			'lead_cost_deferred' => trans('contracts.costDeferred'),
			'lead_percent_invalid' => 'Porcentaje Inválido'
		);
	}


	static function encrypted(){
		return array('wp_password','wp_username');
	}

	function getValueAttribute($value){
		if(!empty($value) && in_array($this->config,self::encrypted())) return \Crypt::decrypt($value);
		return $value;
	}

	function setValueAttribute($value){
		if(!empty($value) && in_array($this->config,self::encrypted())) $value = \Crypt::encrypt($value);
		$this->attributes['value'] = $value;
	}

	static function getValue($key,$overrideClientId = 0){
		if(!in_array($key,array_keys(self::availables()))) return null;

		$clientId = $overrideClientId > 0 ? $overrideClientId : Auth::user()->client_id;

		return self::where('client_id',$clientId)->where('config',$key)->first();
	}

	static function byValue($value,$clientId){
		return self::whereRaw('value = ? and client_id = ?',array($value,$clientId))->first();
	}

	static function saveConfigurations($overrideClientId = 0){
		$clientId = $overrideClientId > 0 ? $overrideClientId : Auth::user()->client_id;

		foreach(self::availables() as $k => $t){
			$value = Input::get($k,false);
			if($value !== false){
				$cc = self::getValue($k,$clientId);
				if(!$cc){
					$cc = new ClientConfig;
					$cc->client_id = $clientId;
					$cc->config = $k;
					$cc->value = $value;
					$cc->save();
				}
				else{
					$cc->value = $value;
					$cc->save();
				}
			}
		}
	}

	function __toString(){
		return $this->value ? $this->value : '';
	}
}