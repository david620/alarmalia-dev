<?php

namespace App\Exports;

use App\Models\CallCenter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class CallCenterExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    public function collection()
    {
        return CallCenter::where('calls', '>=', 3)
        ->orWhere('answered', '=', 1)->get();
    }

    public function headings(): array
    {
        return [
            '#Identificador',
            'Teléfono',
            'Número de Llamadas',
            'Hora y Fecha de la Última llamada',
            'Llamada Constestada o no contestada',
            'Nombres y Apellidos',

        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


}