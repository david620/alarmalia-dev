<?php

namespace App\Exports;

use App\Models\Lead;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class LeadExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    public function collection()
    {
        return Lead::all();
    }

    public function headings(): array
    {
        return [
            '#Identificador',
            'Código Postal',
            'Teléfono',
            'Origen',
            'Validado',
            'Validado en',
            'Creado en',
            'Actualizado en',
            'De Acuerdo a',
            'De Acuerdo con',
            'Eliminado en',
            'Actualizado en',
            'De Acuerdo a',
            'De Acuerdo con',
            'Creado Realmente en',
            'Identificador Ext',
            'Dirección IP',
            'País'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:R1'; // 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


}