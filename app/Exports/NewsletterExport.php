<?php

namespace App\Exports;

use App\Models\Newsletter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class NewsletterExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    public function collection()
    {
        return Newsletter::all();
    }

    public function headings(): array
    {
        return [
            '#Identificador',
            'Email Usuario',
            'Url donde fue registrado',
            'Estado: 0 Unsuscrito, 1 Suscrito',
            'Created at',
            'Updated at'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


}