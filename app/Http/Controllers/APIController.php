<?php

namespace App\Http\Controllers;

use App\Models\Clients\Client;
use App\ClientConfig;
use App\Libraries\Pipe;

use Request;
use App;
use Response;

class APIController extends Controller{

	protected $success = 200;
	protected $clientError = 400;
	protected $serverError = 500;
	private $authorized = false;
	protected $client = null;
	protected $locale = 'en';
	protected $jsVersion = '1.1';
	protected $platform = '';

	public function __construct(){

		$signature = Request::header('X-Pipe-Signature');
		$timestamp = Request::header('X-Pipe-Timestamp');
		$publicKey = Request::header('X-Pipe-PublicKey');
		$acronym = Request::header('X-Pipe-Acronym');
		$this->platform = Request::header('X-Pipe-Platform','');

		if(empty($acronym) || empty($publicKey) || empty($signature) || empty($timestamp) || !Pipe::validateTimestamp($timestamp)) $this->authorized = false;

		else{

			$clientId = Client::acronymToId($acronym);
			$value = ClientConfig::byValue($publicKey,$clientId);
			if($value){
				$secretKey = ClientConfig::getValue('api_secret_key',$clientId);
				if($secretKey){
					$signatureOk = hash('sha256', $acronym.$secretKey.$timestamp.$publicKey);
					
					if(strcasecmp($signature,$signatureOk) === 0){
						$this->authorized = true;
						$this->client = Client::find($clientId);
					}
				}
			}
			
		}

		/*if(App::environment('local')){
			$this->authorized = true;
			$this->client = Client::find(1);
		}*/

		if(!$this->authorized || !$this->client) $this->notAuthorized();

		$this->fillLocaleInfo();

	}

	protected function fillLocaleInfo(){
		$this->locale = Request::header('X-Pipe-Lang','en');
		App::setLocale($this->locale);
	}

	protected function isAuthorized(){
		return $this->authorized;
	}

	protected function notAuthorized(){
		throw new \Exception('Access denied', $this->clientError);
	}

	protected function missingParameters(){
		throw new \Exception('Missing parameters',$this->clientError);
	}

	protected function notFound(){
		throw new \Exception('Service not found',$this->clientError);
	}

	protected function makeResponse($status,$data){
		if(!$this->isAuthorized()) return $this->notAuthorized();
		return Response::json($data,$status,[],JSON_NUMERIC_CHECK);
	}

}