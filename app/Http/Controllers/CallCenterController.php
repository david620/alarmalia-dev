<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CallCenter;
use App\Models\LeadResponse;
use App\Models\LeadInvalid;
use App\Models\Clients\ContractLead;
use App\Models\BlackListMobile;
use App\Models\Leads\Lead;
use App\Models\LeadCost;
use App\Models\Clients\Contract;
use DB;
use Carbon\Carbon;
use App\Exports\CallCenterExport;
use Maatwebsite\Excel\Facades\Excel;

class CallCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $callcenter = DB::table('callcenter')
        ->join('lead', 'lead.id', '=', 'callcenter.lead_id')
		->where([['calls', '<', 3], ['answered', '=', 0]])
        ->select('callcenter.*', 'lead.created_at as fecha')
        ->orderBy('calls', 'ASC')        
        ->orderBy('last_call', 'ASC')        
        ->get();

        return view('callcenter.index',compact('callcenter'));
    }

    public function list_calls(Request $request)
    {
        $callcenter = DB::table('callcenter')
        ->where('calls', '>=', 3)
        ->orWhere('answered', '=', 1)
        ->select('callcenter.*')
        ->orderBy('calls', 'ASC')        
        ->orderBy('last_call', 'DESC')        
        ->get();

        return view('callcenter.list_calls',compact('callcenter'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function namelastname($id)
    {
        $callcenter = CallCenter::find($id);

        return view('callcenter.call.namelastname', compact('callcenter'));
    }

    public function end_positive_answer(Request $request, $id)
    {
        $call = CallCenter::find($id);
        $call->namelastname = $request->namelastname;
        $call->save();

        return redirect()->route('callcenter.index');
    }




    public function call($id)
    {
        $callcenter = CallCenter::find($id);
        $lr = LeadResponse::where('lead_id', '=', $callcenter->lead_id)->get();

        return view('callcenter.call.call', compact('callcenter', 'lr'));
    }

    public function called($id)
    {
        $callcenter = CallCenter::find($id);
        $lr = LeadResponse::where('lead_id', '=', $callcenter->lead_id)->get();

        return view('callcenter.show', compact('callcenter', 'lr'));
    }


    public function dont_answer($id)
    {
        $end = Carbon::now();

        $call = CallCenter::find($id);
        $call->calls = $call->calls + 1;
        $call->answered = 0;
		$start    = new Carbon($call->last_call);

        if($call->last_call){

			$tiempo = $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S');

	        $call->last_call = $end;        	
	        $call->time_call = $tiempo;        	
	        $call->save();
        }
        else{
        	$call->last_call = $end;
        }
        $call->save();



        if($call->calls === 3){
            $lead = Lead::find($call->lead_id);
            $lead->invalid_master_client = 1;
            $lead->save();
        }

        return redirect()->route('callcenter.index');

    }

    public function negative($id)
    {
        return view('callcenter.call.negative', compact('id'));
    }
    public function positive($id)
    {
        $contracts = DB::table('client_contract')
        ->join('client', 'client.code', '=', 'client_contract.client_code')
        ->whereRaw('due >= CURRENT_DATE')
        ->select('client_contract.*', 'client.name', 'client.id as id_cliente')
        ->get();

        $all_contracts = DB::table('client_contract')
        ->join('client', 'client.code', '=', 'client_contract.client_code')
        ->select('client_contract.*', 'client.name', 'client.id as id_cliente')
        ->get();

        return view('callcenter.call.positive', compact('id', 'contracts', 'all_contracts'));
    }

    public function negative_answer($id, $option)
    {
        $call = CallCenter::find($id);
        $call->calls = $call->calls + 1;
        $call->last_call = Carbon::now();
        $call->answered = 1;
        $call->save();

        if($option == 1 || $option == 2){
            $lead = Lead::find($call->lead_id);
            $lead->invalid_master_client = 1;
            $lead->save();            
        }
        elseif ($option == 3 || $option == 4) {
	        BlackListMobile::addbl($call->phone);

            LeadInvalid::createOrUpdateValidated($call->lead_id);
        }

        return redirect()->route('callcenter.index');
    }


    public function positive_answer(Request $request, $id)
    {
        $call = CallCenter::find($id);
        $call->calls = $call->calls + 1;
        $call->last_call = Carbon::now();
        $call->answered = 1;
		if($request->ids_c){
	        $call->ids_companies = implode(',', $request->ids_c);
	        $call->save();
		}

        $call->save();

        $contracts = DB::table('client_contract')
        ->join('client', 'client.code', '=', 'client_contract.client_code')
        ->where('transference_type', '=', 2)
        ->whereRaw('due >= CURRENT_DATE')
        ->select('client_contract.*', 'client.name')
        ->get();

        if(sizeof($contracts) != 0){
                $prices = LeadCost::assocList();
                $totContracts = count($contracts);
                $price = $prices[$totContracts];

                        foreach($contracts as $c){

                                try{
                                    $cl = new ContractLead;
                                    $cl->lead_id = $call->lead_id;
                                    $cl->client_contract_id = $c->id;
                                    $cl->transference_type = $c->transference_type;
                                    $cl->cost = $c->transference_type == 2 ? $price->i : $price->v;
                                    $cl->discount = $c->discount;
                                    $cl->active = $c->transference_type == 2 ? 1 : 2;
                                    $cl->save();

                                }
                                catch(Exception $e){}
                        }        	
        }

        return redirect()->route('callcenter.namelastname', $call->id);
    }


    public function create()
    {
        return view('callcenter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'content' => 'required',
        ]);

        $glosary = new Glosary();
        $glosary->name = $request->input('name');
        $glosary->content = $request->input('content');
        $glosary->save();

        return redirect()->route('callcenter.index')
                        ->with('success','callcenter creada exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return view('callcenter.show',compact('role','rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('callcenter.edit',compact('callcenter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'content' => 'required',
        ]);


        $glosary = Glosary::find($id);
        $glosary->name = $request->input('name');
        $glosary->content = $request->input('content');
        $glosary->update();

        return redirect()->route('callcenter.index')
                        ->with('success','callcenter Actualizada con Exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("callcenter")->where('id',$id)->delete();
        return redirect()->route('callcenter.index')
                        ->with('success','callcenter Eliminada con Éxito');
    }

    public function export() 
    {
        return Excel::download(new CallCenterExport, 'callcenter.xlsx');
    }

}