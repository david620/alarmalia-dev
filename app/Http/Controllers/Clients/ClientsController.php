<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Breadcrumb;
use App\Client;
use App\ClientConfig;
use App\Models\Clients\Client as ClientFS;
use App\Models\Clients\Contract;
use App\Models\Clients\Company;
use App\Models\Clients\Province;
use App\Models\Question;
use App\User;
use Illuminate\Http\Request;
use Lang;
use App\Libraries\Pipe;
use Redirect;
use URL;
use Auth;
use Session;
use Validator;
use View;
use DB;

define('M_PREFIX','Clients_');

class ClientsController extends Controller{
	
	function index(Request $request){
		$clients = ClientFS::toList();
//		return $this->setView('Clients::clients.index',$data);
        return view('Clients.Views.clients.index',compact('clients'));
	}

	function contracts(Request $request){

		$contracts = Contract::toList();


        return view('Clients.Views.contracts.index',compact('contracts'));
	}

	function companies(Request $request){

		$companies = Company::toList();
        return view('Clients.Views.companies.index', compact('companies'));
	}

	function companiesEdit(Request $request,$id = -1){

		$entity = Company::find($id);

		if(!$entity) $entity = new Company;

		$new = !$entity->id;
		if($request->isMethod('post')){
			return $this->validateRequest([
		    	'name' =>'required|min:3',
		    ],
		    function() use($entity,$new,$request){

		    	$entity->name = $request->get('name');

		    	$entity->save();

		    	if(!$new){
		    		$clients = $request->get('clients',array());
		    		if(is_array($clients)){
		    			ClientFS::cleanCompany($entity->id);
		    			foreach($clients as $c){
		    				$client = ClientFS::find($c);
		    				if($client){
		    					$client->company = $entity->id;
		    					$client->save();
		    					unset($client);
		    				}
		    			}
		    		}
		    	}

		    	$entity->setRecentInList();
		    	if($new){
					$companies = Company::toList();
			        return view('Clients.Views.companies.index',compact('companies'));		    		
		    	} 
		    	else{
					$companies = Company::toList();
			        return view('Clients.Views.companies.index',compact('companies'));		    		
		    	}

		    	return false;
		    });

			$new = !$entity->id;
			$clients = ClientFS::toCompany();
			$companies = Company::toList();
	        return view('Clients.Views.companies.index',compact('companies'));
		}
		else{
			$new = !$entity->id;
			$clients = ClientFS::toCompany();
	        return view('Clients.Views.companies.edit',compact('entity', 'new', 'clients'));			
		}

	}

	function contractsEdit(Request $request,$id = -1){

        $respuestas_form = DB::table('forms')
            ->join('questions', 'questions.form_id', '=', 'forms.id')
            ->join('answers', 'answers.question_id', '=', 'questions.id')
            ->where('forms.status', '=', 1)       
            ->where('forms.long_short', '=', 1)       
            ->where('answers.content', '!=', null)       
            ->select(DB::raw("CONCAT(questions.title,' :   ', answers.content) AS name"), 'answers.*')
            ->pluck('name', 'answers.id');


        $respuestas_form_ids = DB::table('forms')
            ->join('questions', 'questions.form_id', '=', 'forms.id')
            ->join('answers', 'answers.question_id', '=', 'questions.id')
            ->where('forms.status', '=', 1)       
            ->where('forms.long_short', '=', 1)       
            ->where('answers.content', '!=', null)       
            ->select('answers.*')
            ->pluck('answers.id');

         $question_m2 = DB::table('questions')
            ->join('forms', 'forms.id', '=', 'questions.form_id')
            ->where('forms.status', '=', 1)       
            ->where('forms.long_short', '=', 1)       
            ->where('questions.title', '=', '¿Cuántos metros cuadrados mide tu vivienda aproximadamente?')       
            ->select('questions.id')
            ->first();

		if(strpos($id, 'c') !== false){
			Session::flash('cloneContractId',explode('c',$id)[1]);
		}

		$entity = Contract::find($id);

		if(!$entity){
			$entity = new Contract;
			$cloneContractId = Session::get('cloneContractId',0);
			if($cloneContractId > 0){
				$toClone = Contract::find($cloneContractId);
				if($toClone) $entity = $toClone->replicate();

				unset($toClone);
			}
		}

		if($request->isMethod('post')){

			$postAction = $request->get('postAction','');
			if($postAction == 'printLeads'){

				$html = View::make('Clients.Views.contracts.edit_leads_table',array('entity' => $entity, 'dateTimeFormat' => $this->dateTimeFormat))->render();
				return $this->printExcel($html);
				exit;
			}

			$new = !$entity->id;

			$fields = $request->all();
			
			if(!$new){
				foreach($fields as $k => $f){
					if(in_array($k,array('client_code','due','budget_type'))) unset($fields[$k]);
				}
			}
			else if($fields['due']){
				$fields['due'] = \DateTime::createFromFormat($this->dateFormat,$fields['due']);
			}

			//print_r($fields);exit;

			$entity->fill($fields);


			if(!$request->m2_check){
		    	$entity->m2_size = null;
		    	$entity->save();
			}

			if(!$request->type_lead){
		    	$entity->type_lead = 0;
		    	$entity->save();
			}


			Validator::extend('validate_due_date', function($attribute, $value, $parameters) use($entity)
			{	
				$value = $value ? \DateTime::createFromFormat($this->dateFormat,$value)->format('Y-m-d') : null;

				if(!$value) return false;

				$maxDue = $entity->maxDueFromClient();
				if(!$maxDue) return true;

				return strtotime($value) >= strtotime($maxDue);
				});

			Validator::extend('email_multiple', function($attribute, $value, $parameters) use($entity)
			{	
				$emails = explode(';',$value);

				foreach($emails as $e){
					if (!filter_var($e, FILTER_VALIDATE_EMAIL)) return false;
				}

				return true;
			});
			
			

			return $this->validateRequest([
		    	'client_code' => $new ? 'required|exists:client,code' : '',
		    	'due' => $new ? 'required|date_format:'.$this->dateFormat.'|validate_due_date' : '',
		    	'transference_type' => 'required|in:0,1,2',
		    	'budget_type' => $new ? 'required|in:cash,leads' : '',
		    	'budget_cash' => 'required_if:budget_type,cash|numeric',
		    	'budget_leads' => 'required_if:budget_type,leads|numeric',
		    	'email' => 'required|email_multiple'
		    ],
		    function() use($entity,$new,$request){

		    	$postalsC = $request->get('postalsC',array());
		    	if(!is_array($postalsC)) $postalsC = array();

		    	$entity->postals = implode(',',$postalsC);

		    	$respuestas = $request->get('respuestas',array());
		    	if(!is_array($respuestas)) $respuestas = array();

		    	$entity->questions_ids = implode(',',$respuestas);

		    	$entity->save();
		    	$entity->setRecentInList();

		    	if($new) $this->redirect = Redirect::route('Clients_contracts_edit',array($entity->id));

		    	return false;
		    });
		}

/*

		$tmp = ClientConfig::getValue('lead_cost_instantly');
		$data['costI'] = $tmp ? floatval($tmp->value) : 0;

		$tmp = ClientConfig::getValue('lead_cost_deferred');
		$data['costD'] = $tmp ? floatval($tmp->value) : 0;
*/

		$new = !$entity->id;

		$clients = ClientFS::toList(false);
		$provinces = Province::toList();


        return view('Clients.Views.contracts.edit',compact('new', 'clients', 'entity', 'provinces', 'respuestas_form', 'question_m2', 'respuestas_form_ids'));
	}

    public function contractsDelete(Request $request,$id)
    {
        DB::table("client_contract")->where('id',$id)->delete();

        return Redirect::back()
			->with('success','Contrato Eliminado con Éxito');
    }


    public function clientsDelete($id)
    {

        $cliente = ClientFS::where('id', '=', $id)->first();
        $cliente->delete();

        return Redirect::back()
			->with('success','Cliente Eliminado con Éxito');
    }

}