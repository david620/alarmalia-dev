<?php
namespace App\Http\Controllers;

use HTTP_Request2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Routing\Controller as BaseController;
use App\Libraries\FacturaScripts;
use App\Libraries\SMSUP;
use App\Models\Cliente as ClientFS;
use App\Models\Leads\Lead;
use App\Models\CallCenter;
use App\Models\LeadInvalid;
use App\Models\LeadCost;
use App\Models\LeadResponse;
use App\Models\Clients\Contract;
use App\Models\Clients\ContractLead;
use Illuminate\Http\Request;
use View;
use Mail;
use App;

class ClientsCronController extends BaseController{



	function synchro(Request $request){ //Listo
		$created = $updated = 0;

			$fs = new FacturaScripts;
			$clients = $fs->clientList();

			foreach($clients as $cli){
				$client = ClientFS::find($cli['codcliente']);
				if(!$client){
					$client = new ClientFS;
					$client->code = $cli['codcliente'];
					$created++;
				}
				else{
					$updated++;
				}
				$client->name = $cli['nombre'];
				$client->email = $cli['email'];
				$client->cifnif = $cli['cifnif'];
				$client->save();
			}

			unset($clients);

		echo $created.' created<br/>';
		echo $updated.' updated<br/>';
	}

	function synchroContracts(Request $request){ //Listo 
			$fs = new FacturaScripts;
			$contractLeads = ContractLead::toErp();
			foreach($contractLeads as $cl){
				$contract = $cl->contract;
				$client = $contract->client;
				$leads = ContractLead::fromContract($cl->client_contract_id);
				if($fs->synchroAlbaranCabecera($client,$contract,$leads)){

				}
			}
	}

	function distribute(Request $request){##listo
		//MUTEX

		$f = fopen(storage_path().'/framework/cache/lock_'.__FUNCTION__.($request->get('deferred',0) == 1 ? 1 : 0), 'w') or die ('Cannot create lock file');
		if (!flock($f, LOCK_EX | LOCK_NB)){
			echo 'Locked cron..';
			return;
		}
		$done = $ignored = 0;
			$instantly = $request->get('deferred',0) == 1 ? false : true;
			if(!$instantly){
				$contracts = ContractLead::pendingDeferred();
				foreach($contracts as $c){
					$c->active = 1;
					$c->save();
					$done++;
				}
			}
			else{

				$leads = Lead::toSell(true);
				$prices = LeadCost::assocList();
				foreach($leads as $l){
					if ($l->lead_type == 1 ) {

						$postalPrefix = substr($l->postal_code, 0, 2);
						$lead_response = LeadResponse::where('lead_id', '=', $l->id)->get();
						$contracts = null;


							if($lead_response){
								foreach ($lead_response as $ans) {
									$contracts = Contract::toSell($l->id, $postalPrefix, $ans->answer_id);

									if($contracts){
										break;			
									}
								}							
							}	
							else{
								continue;
							}

						$totContracts = count($contracts);

						if(!$totContracts){
							$ignored++;
							continue;
						}

						$price = $prices[$totContracts];
						foreach($contracts as $c){
							if($c->type_lead == 1 && $l->lead_type == 1 && $c->transference_type != 2 ){

								try{
									$cl = new ContractLead;
									$cl->lead_id = $l->id;
									$cl->client_contract_id = $c->id;
									$cl->transference_type = $c->transference_type;
									$cl->cost = $c->transference_type == 0 ? $price->i : $price->d;
									$cl->discount = $c->discount;
									$cl->active = $c->transference_type == 0 ? 1 : 0;
									$cl->save();

									$done++;
								}
								catch(Exception $e){}
							}
						}
					}
					elseif ($l->lead_type == 0) {	
						$lead_response = LeadResponse::where('lead_id', '=', $l->id)->get();
						$contracts = null;


							if($lead_response){
								foreach ($lead_response as $ans) {
									$contracts = Contract::toSellShort($l->id);

									if($contracts){
										break;			
									}
								}							
							}
							else{
								continue;
							}

						$totContracts = count($contracts);

						if(!$totContracts){
							$ignored++;
							continue;
						}


						$price = $prices[$totContracts];
						foreach($contracts as $c){

							if(($c->type_lead == 1 || $c->type_lead == 0) && $l->lead_type == 0 && $c->transference_type != 2){

								try{
									$cl = new ContractLead;
									$cl->lead_id = $l->id;
									$cl->client_contract_id = $c->id;
									$cl->transference_type = $c->transference_type;
									$cl->cost = $c->transference_type == 0 ? $price->i : $price->d;
									$cl->discount = $c->discount;
									$cl->active = $c->transference_type == 0 ? 1 : 0;
									$cl->save();

									$done++;
								}
								catch(Exception $e){}
							}


						}


					}

					if($l->lead_type != 0){
						//Envio lead a tabla callcenter
						$lead = CallCenter::where('lead_id', '=',$l->id)->first();
						if(!$lead){
							$lr = LeadResponse::where([['lead_id', '=', $l->id], ['question', '=', 'Indícanos por favor tu teléfono para mostrarte la comparativa']])->first();

							$cc = new CallCenter;
							$cc->lead_id = $l->id;
							$cc->phone = $lr->response;
							$cc->calls = 0;
							$cc->answered = 0;
							$cc->save();
						}						
					}
					elseif ($l->lead_type == 0) {

						$lead = CallCenter::where('lead_id', '=',$l->id)->first();

						if(!$lead){
							$cc = new CallCenter;
							$cc->lead_id = $l->id;
							$cc->phone = $l->phone;
							$cc->calls = 0;
							$cc->answered = 0;
							$cc->save();
						}

					}
				}
			}

			//listado de todos los leads con CP y pais ES, que se haya enviado menos de 4 veces. y que sea inmediato (qué consideramos inmediato??)

				//consultar loop en cada vuelta!
				//para cada lead buscamos contratos activos (primero inmediatos) por orden de prioridad, con budget disponible y que admita el CP y que pueda asumir el lead (que no sobrepase el límite). Asignar lead


			//listado de todos los contratos por orden de prioridad y con budget disponible

		echo $done.' done<br/>';
		echo $ignored.' not assigned<br/>';

		fclose($f);
	}

	function distributeEmail(Request $request){

		$f = fopen(storage_path().'/framework/cache/lock_'.__FUNCTION__, 'w') or die ('Cannot create lock file');
		if (!flock($f, LOCK_EX | LOCK_NB)){
			echo 'Locked cron..';
			return;
		}

		$done = $ignored = 0;

			$toSend = ContractLead::toSend();
			foreach($toSend as $s){
				$lead = $s->lead;
                if ( isset($s->client_code) && $s->client_code === "000002" ){
                    $isSent = $this->sendLeadBySectorAlarmAPI($lead);
                }else{

                    $data['clientCode'] = $s->client_code;
                    $data['lead'] = $lead;

                    //$html = View::make('Clients::emails.lead_client',$data)->render(); echo $html;exit;
                    $target = $s->email;


                    if($s->transference_type == 2){
						$name = CallCenter::where('lead_id', '=',$lead->id)->first()->namelastname;
    	                $data['nombres'] = $name;


				        try{
		                    Mail::send('Clients.Views.emails.lead_verified_client',$data,function($message) use($target){
		                        $message->to(explode(';',$target))
		                            ->from('no-reply@alarmator.es','Alarmalia')
		                            ->subject('Alarmalia - Nuevo Lead Verificado');
		                    });

				        }catch (HttpException $ex){
				            $success = false;
				        }





                    }
                    else{
	                    Mail::send('Clients.Views.emails.lead_client',$data,function($message) use($target){
	                        $message->to(explode(';',$target))
	                            ->from('no-reply@alarmator.es','Alarmalia')
	                            ->subject('Alarmalia - Nuevo Lead');
	                    });                    	
                    }




                    $isSent = count(Mail::failures()) == 0 ? true : false;
                }

                if( $isSent ) {
                    $s->sent = 1;
                    $s->save();
                    $done++;
                }else{
                    $ignored++;
                }
			}

		echo $done.' done<br/>';
		echo $ignored.' not assigned<br/>';

		fclose($f);
	}

	function invalidLead($leadId,$clientCode){ //LISTO
			LeadInvalid::createOrUpdate($leadId,$clientCode);
			$clientFS = ClientFS::find($clientCode);
			if($clientFS->master_invalid){
				Lead::invalidateByMaster($leadId);
				unset($clientFS);
			}
			echo 'La acción se ha registrado correctamente.<br/>';
	}


	private function sendLeadBySectorAlarmAPI($lead){
        $APIurl = env('SECTORALARM_API_URL');
        $APIkey = env('SECTORALARM_API_KEY');

        $request = new Http_Request2($APIurl);
        $url = $request->getUrl();
        $headers = array(
            'Content-Type' => 'application/json',
            'Ocp-Apim-Subscription-Key' => $APIkey,
        );

        $request->setHeader($headers);
        $request->setMethod(HTTP_Request2::METHOD_POST);

        $json = $this->getSectorAlarmAPIJson($lead);
        $request->setBody($json);

        try{
            $response = $request->send();
            $success = $response->getStatus() === 201 ? true : false;
        }catch (HttpException $ex){
            $success = false;
        }

        return $success;
    }

    private function getSectorAlarmAPIJson($lead){
	    $leadZipCode = isset($lead['postal_code']) ? $lead['postal_code'] : '';
	    $leadPhone = isset($lead['phone']) ? $lead['phone'] : '';

	    $response = new \stdClass();
	    $response->Country = 6;
	    $response->CustomerName = null;
	    $response->DateOfBirth = null;
	    $response->SSN = null;
	    $response->InvoiceAddress = array(
            "Street1"=> null,
            "Street2"=> null,
            "Street3"=> null,
            "Street4"=> null,
            "ZipCode"=> $leadZipCode,
            "City" => null
        );
        $response->InstallationAddress = array(
            "Street1"=> null,
            "Street2"=> null,
            "Street3"=> null,
            "Street4"=> null,
            "ZipCode"=> $leadZipCode,
            "City" => null
        );
        $response->Email = null;
        $response->Mobilephone = $leadPhone;
        $response->Homephone = null;
        $response->LeadSourceId = "101";
        $response->LeadChannelLogicalName = "Partner Lead";

        return json_encode($response);
    }
}
