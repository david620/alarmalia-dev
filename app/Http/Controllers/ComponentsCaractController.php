<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Advantage;
use App\Models\ComponentCaract;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;

class ComponentsCaractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $id_component = $id;
        $components_caract = ComponentCaract::where('component_id','=', $id)->get();

        return view('companies.components.caracteristicas.index',compact('components_caract', 'id_component'));
    }


    public function create(Request $request, $id)
    {
        $id_component = $id;
        return view('companies.components.caracteristicas.create', compact('id_component'));
    }
 

    public function store(Request $request)
    {


        $this->validate($request, [
            'content' => 'required',
            'component_id' => 'required',
        ]);

        DB::beginTransaction();

        $components_caract = new ComponentCaract;
        $components_caract->content = $request->content;
        $components_caract->component_id = $request->component_id;
        $components_caract->save();

        if (!$components_caract) {
            DB::rollBack();
            return redirect()->route('components_caract.create', $request->component_id)->with('alert-danger', 'Error Creando caracteristicas');
        }

        DB::commit();

        return redirect()->route('components_caract.index', $request->component_id)->with('success', 'Caracteristicas Creada Exitosamente.');
    }

 
    public function edit($id)
    {
        $components_caract = ComponentCaract::find($id);
        return view('companies.components.caracteristicas.edit',compact('components_caract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required',
            'component_id' => 'required',
        ]);



        $components_caract = ComponentCaract::find($id);
        $components_caract->content = $request->content;
        $components_caract->component_id = $request->component_id;

        $components_caract->update();


        return redirect()->route('components_caract.index', $request->component_id)->with('success', 'Caracteristica Modificada Exitosamente.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table("components_caract")->where('id',$id)->delete();
        return redirect()->route('components_caract.index', $request->component_id)
                        ->with('success','Caracteristica Eliminada con Éxito');
    }
}