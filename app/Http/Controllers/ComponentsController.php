<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Cliente;
use App\Models\Advantage;
use App\Models\Component;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;

class ComponentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $id_company = $id;
        $components = Component::where('company_id','=', $id)->get();
        return view('companies.components.index',compact('components', 'id_company'));
    }


    public function create(Request $request, $id)
    {
        $id_company = $id;
        return view('companies.components.create', compact('id_company'));
    }
 

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'component_icon' => 'required',
            'component_accessory' => 'required',
        ]);

        $component = new Component();
        $component->name = $request->input('name');
        $component->company_id = $request->input('company_id');
        $component->icon = "";
        $component->component_accessory = $request->input('component_accessory');
        $component->save();

        $component1 = Component::find($component->id);

        $filename = 'iconos/'.$component1->id.'_'.$request->component_icon->getClientOriginalName();

        Storage::disk('imagenes')->put($filename, File::get($request->component_icon));

        $route = 'imagenes/'.$filename;

        $component1->icon = $route;

        $component1->save();

        return redirect()->route('components.index', $request->company_id)->with('alert-danger', 'Error Creando Componente/Accesorio');
    }

 
    public function edit($id)
    {
        $components = Component::find($id);
        return view('companies.components.edit',compact('components'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        $component = Component::find($id);

        if (Input::hasFile('component_icon')) {
            if (Input::file('component_icon')->isValid()) {

                Storage::disk('imagenes')->delete(substr($component->icon, strpos($component->icon, '/'), strlen($component->icon)));

                DB::beginTransaction();

                $component->name = $request->input('name');
                $component->component_accessory = $request->input('component_accessory');
                $component->icon = $request->input('component_icon');

                $filename = 'iconos/'.$component->id.'_'.$request->component_icon->getClientOriginalName();

                Storage::disk('imagenes')->put($filename, File::get($request->component_icon));

                $route = 'imagenes/'.$filename;

                $component->icon = $route;

                $component->save();

                if (!$component) {
                    DB::rollBack();
                    return redirect()->route('components.edit', ['id' => $id])->with('mensage', 'Error Modificando Componente/Accesorio');
                }

                DB::commit();
            }
        } else {
                $component->name = $request->input('name');
                $component->component_accessory = $request->input('component_accessory');
                $component->update();
        }

        return redirect()->route('components.index', $component->company_id)->with('success','Componente/Accesorio Actualizado con Exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table("components")->where('id',$id)->delete();
        return redirect()->route('components.index', $request->company_id)
                        ->with('success','Componente/Accesorio Eliminado con Éxito');
    }
}