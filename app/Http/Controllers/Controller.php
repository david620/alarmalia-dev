<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Validator;
use View;
use Auth;
use App;
use Session;
use Illuminate\Support\Facades\Input;
use Redirect;
use Lang;
use Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	protected $dateFormat = 'Y-m-d';
	protected $dateTimeFormat = 'Y-m-d H:i';
	protected $redirect = null;

	protected function validateRequest($rules,callable $success){
		$validator = Validator::make(
		    Input::all(),
		    $rules
		);
		if ($validator->fails()):
			return Redirect::back()->withInput(Input::all())->withErrors($validator->messages());
		endif;
		
		$res = call_user_func($success);

		if($res === false){
			$newRedirect = null === $this->redirect ? Redirect::back() : $this->redirect;
			return $newRedirect->with('successForm',Lang::trans('app.dataUpdated'));
		}

		return $res;
	}

	protected function printExcel($html,$name = 'document.xls'){
		$response =  '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
			<head>
			    <!--[if gte mso 9]>
			    <xml>
			        <x:ExcelWorkbook>
			            <x:ExcelWorksheets>
			                <x:ExcelWorksheet>
			                    <x:Name>Sheet 1</x:Name>
			                    <x:WorksheetOptions>
			                        <x:Print>
			                            <x:ValidPrinterInfo/>
			                        </x:Print>
			                    </x:WorksheetOptions>
			                </x:ExcelWorksheet>
			            </x:ExcelWorksheets>
			        </x:ExcelWorkbook>
			    </xml>
			    <![endif]-->
			</head>

			<body>
			   '.$html.'
			</body></html>';
			return response($response)
			->header('Content-Disposition','attachment; filename='.$name)
			->header('Content-type', 'application/excel; charset=utf-8');
	}


}
