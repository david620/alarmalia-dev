<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Lead;
use App\Models\Permission;
use DB;
use App\Exports\LeadExport;
use Maatwebsite\Excel\Facades\Excel;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function index(Request $request)
    {
        $leads = Lead::all();
        return view('Leads.index',compact('leads'));
    }


    public function destroy($id)
    {
        DB::table("lead")->where('id',$id)->delete();
        return redirect()->route('Leads_index')
                        ->with('success','Lead Eliminado con Éxito');
    }
}