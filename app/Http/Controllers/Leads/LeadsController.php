<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Breadcrumb;
use App\Models\Client;
//use App\Models\Leads\Lead;
use App\Models\Lead;
use Illuminate\Http\Request;
use Lang;
use App\Libraries\Pipe;
use App\Libraries\Google;
use Redirect;
use URL;
use Auth;
use Session;
use DB;
use App\Exports\LeadExport;
use Maatwebsite\Excel\Facades\Excel;

class LeadsController extends Controller{
	
	function index(Request $request){


        $leads = DB::table('lead')
        ->whereRaw('date(created_real) > ?',array('2019-02-10'))
		->select(DB::raw("lead.*,(select count(distinct c.id) from client_contract_lead as c where c.lead_id = lead.id) as times_sold"))
		->orderBy('created_real','desc');


		if($request->name_filter){
			$leads->where('lead.postal_code','like' , '%' . $request->name_filter . '%');
		}

		if($request->dateIni){
			$leads->whereBetween('created_real', array($request->dateIni, $request->dateFin));
		}


		$leads = $leads->paginate(20);


        //$data['leads'] = Lead::toList($dateIni,$dateFin);

        return view('leads.Views.leads.index', compact('leads'));
		//return $this->setView('Leads::leads.index',$data);
	}	

	function detail(Request $request,$id){
		$leads = Lead::all();
	
		$lead = Lead::find($id);
		if(!$lead) return Redirect::back();

        return view('leads.Views.leads.detail', compact('lead', 'leads'));
	}

	function awake(Request $request,$id){

		$lead = Lead::find($id);
		if(!$lead) return Redirect::back();

		
		$newLead = $lead->replicate();
		$newLead->created_real = date('Y-m-d H:i:s');
		if($newLead->save()){
			foreach($lead->responses as $r){
				$newResp = $r->replicate();
				$newResp->lead_id = $newLead->id;
				$newResp->save();
			}
		}
		
		return Redirect::back()
			->with('successForm');
	}

    public function export() 
    {
        return Excel::download(new LeadExport, 'lead_list.xlsx');
    }

    public function destroy($id)
    {
        DB::table("lead")->where('id',$id)->delete();
        return redirect()->route('Leads_index')
                        ->with('success','Lead Eliminado con Éxito');
    }

}