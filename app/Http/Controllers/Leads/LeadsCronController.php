<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\Cliente;
use App\ClientConfig;
use App\Models\SourceLead;
use App\Models\SourceLeadDetail;
use App\Models\Lead;
use App\Models\LeadResponse;
use App\Models\BlackListMobile;

use Illuminate\Http\Request;


class LeadsCronController extends BaseController{
	function ipToCountry($ip){
		$data['ts'] = time();
		$data['id'] = 'D91F51C2AD84E';
		$data['ip'] = $ip;
		$data['sign'] = hash('sha256',$ip.$data['ts'].$data['id'].':kwBZ(+d:Nah_:j0v+?hZqj1c-?d-X');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,'https://tictac.umust.io/ip.php?'.http_build_query($data));
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$res = curl_exec($ch);

		$httpcode = curl_getinfo($ch,CURLINFO_HTTP_CODE);

		curl_close ($ch);

		return $httpcode == 200 ? $res : false;
	}

	function synchro(Request $request){

		$tot = 0;

			//var_dump(BlackListMobile::feed('66606666'));exit;
			$lastId = Lead::lastExtId();
			$list = SourceLead::toList($lastId);
			dd($list);
			$percentInvalid = ClientConfig::getValue('lead_percent_invalid',$client->id);
			
			foreach($list as $l){
				$data = SourceLeadDetail::data($l->id);
				
				if(Lead::exitsExt($l->id)) continue;
				else if(!isset($data['tel']) || empty($data['tel'])) continue;

				$country = $this->ipToCountry($l->ip);

				if($country === false) continue;

				

				$lead = new Lead;
				$lead->postal_code = isset($data['cp']) ? $data['cp'] : '';
				if(BlackListMobile::isIn($data['tel'])) $lead->postal_code = '**'.$lead->postal_code;
				else if(BlackListMobile::feed($data['tel'])) $lead->postal_code = '***'.$lead->postal_code;

				$lead->phone = isset($data['tel']) ? $data['tel'] : '';
				$lead->created_real = $l->date_created;
				$lead->origin = $l->source_url;
				$lead->id_ext = $l->id;
				$lead->ip = $l->ip;
				$lead->country = $country;
				$lead->percent_invalid = $percentInvalid ? $percentInvalid->value : 50;
				$invalidate = Lead::invalidateFromIp($lead->ip,$lead->phone);
				if($invalidate){
					$lead->deleted_at = date('Y-m-d H:i:s');
				}

				if($lead->save()){
					$tot++;
					$quiz = isset($data['quiz']) ? $data['quiz'] : '';
					$questions = explode("\n",$quiz);
					for($i = 0; $i < count($questions)-1; $i += 2){
						if(!$questions[$i]) continue;

						$res = new LeadResponse;
						$res->lead_id = $lead->id;
						$res->question = $questions[$i];
						$res->response = $questions[$i+1];
						$res->save();
					}
				}
			}

		echo $tot;
	}
}