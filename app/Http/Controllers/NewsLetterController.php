<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use App\Models\Newsletter as Newsletterl;
use App\Exports\NewsletterExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class NewsLetterController extends Controller
{
    public function index()
    {
    	$newsletters = Newsletterl::all();
        return view('newsletters.index', compact('newsletters'));
    }

    public function store(Request $request)
    {
        if ( ! Newsletter::isSubscribed($request->email) ) 
        {
            Newsletter::subscribePending($request->email);

            DB::insert('insert into newsletters (mail, url_source, status) values (?, ?, ?)', [$request->email, $request->url(), 1]);

            return redirect('/')->with('success', 'Gracias por Susbcribirse');
        }
        return redirect('/')->with('failure', 'Lo siento, usted ya se encuentra subscrito.');
            
    }

    public function subscribe($mail)
    {

        Newsletter::subscribeOrUpdate($mail);
        DB::update('update newsletters set status = 1 where mail = :mail', ['mail' => $mail]);

        return redirect()->route('newsletter.index')->with('success', 'Usuario Suscrito Exitosamente.');
    }

    public function unsubscribe($mail)
    {
		Newsletter::unsubscribe($mail, 'subscribers');
        DB::update('update newsletters set status = 0 where mail = :mail', ['mail' => $mail]);

        return redirect()->route('newsletter.index')->with('success', 'Usuario Unsuscrito Exitosamente.');
    }

    public function destroy($id)
    {
        $newsletters = Newsletterl::find($id);

//        Newsletterl::delete($newsletters->mail);
        DB::table("newsletters")->where('id',$id)->delete();
        return redirect()->route('newsletter.index')
                        ->with('success','Usuario Eliminado con Éxito');
    }

    public function export() 
    {
        return Excel::download(new NewsletterExport, 'users_newsletter.xlsx');
    }


}
