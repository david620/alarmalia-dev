<?php

namespace App\Http\Controllers;
use App\Models\Seller;
use App\Models\Product;
use App\Models\Sale;
use App\Models\ProductList;
use App\Models\ProductListItem;
use Illuminate\Http\Request;

class RestController extends APIController{

	function __construct(){
		parent::__construct();
	}


	public function synchronizeSellers(Request $request){
		$sellers = $request->get('seller',array());
		if(!is_array($sellers)) $sellers = array();

		if(!count($sellers)) return $this->notFound();

		foreach($sellers as $s){
			Seller::synchro($s);
		}

	}

	public function synchronizeProducts(Request $request){

		$products = $request->get('product',array());
		if(!is_array($products)) $products = array();

		if(!count($products)) return $this->notFound();

		foreach($products as $p){
			Product::synchro($p);
		}

	}

	public function synchronizeSales(Request $request){

		$sales = $request->get('sale',array());
		if(!is_array($sales)) $sales = array();

		if(!count($sales)) return $this->notFound();

		foreach($sales as $s){
			Sale::synchro($s);
		}

	}

	public function synchronizeProductList(Request $request){

		$items = $request->get('product_list',array());
		if(!is_array($items)) $items = array();

		if(!count($items)) return $this->notFound();

		foreach($items as $i){
			ProductList::synchro($i);
		}

	}

	public function synchronizeProductListItem(Request $request){

		$items = $request->get('product_list_item',array());
		if(!is_array($items)) $items = array();

		if(!count($items)) return $this->notFound();

		foreach($items as $i){
			ProductListItem::synchro($i);
		}

	}

	public function coreQueries(){
		$res = array();

		$res[] = (object)array(
			'query' => 'select NOMBRE as name, IDVENDEDOR as code from Vendedor',
			'resource' => 'seller',
			'service' => 'synchronize',
		);

		$res[] = (object)array(
			'query' => 'select IdLista as id, descripcion as description, Fecha as moment, NumElem as num, XList_IdFiltro as filter_id, tipo as type from ListaArticu',
			'resource' => 'product_list',
			'service' => 'synchronize',
		);

		$lastIds = ProductListItem::lastIds();

		$where = $lastIds ? ' where XItem_IdLista >= '.$lastIds->list_id.' and XItem_IdArticu >= '.$lastIds->product_code.' ' : '';
		
		$res[] = (object)array(
			'query' => 'SELECT TOP 20000 XItem_IdLista as list_id, XItem_IdArticu as product_code from ItemListaArticu '.$where.' order by XItem_IdLista,XItem_IdArticu',
			'resource' => 'product_list_item',
			'service' => 'synchronize',
		);

		$lastTsProduct = Product::lastTs();

		$res[] = (object)array(
			'query' => "SELECT TOP 20000 cast(l.IdVenta as nvarchar(4000))+cast(l.IdNLinea as nvarchar(4000)) as ext_id, l.Codigo as code, l.Descripcion as name, Cantidad as sold, l.PVP as pvp, lvi.PIva as iva, a.pmc as pmc, v.XVend_IdVendedor as seller, l.DescuentoLinea as discount, v.FechaHora as moment, Laboratorio as provider, l.IdVenta as sale_id, l.IdNLinea as line_id, l.ImporteBruto as total, l.ImporteNeto as total_net, l.TipoAportacion as share, (case when ((cip.IdVenta is not null and l.TipoAportacion <> '' and l.TipoAportacion is not null) or (l.TipoAportacion <> '' and l.TipoAportacion is not null)) then 1 else 0 end) as recipe, l.TipoLinea as type, l.RecetaPendiente as rp, l.Pvp as apvp, reden.Redencion as reden, reden.TotalRedencion as reden_tot  FROM LineaVenta as l LEFT JOIN Venta as v ON v.IdVenta = l.IdVenta LEFT JOIN Articu as a ON a.IdArticu = l.Codigo LEFT JOIN LineaVentaIVa lvi on lvi.IdVenta = l.IdVenta and l.IdNLinea = lvi.IdNLinea 
				left join LineaVentaCip cip on cip.IdVenta = l.IdVenta AND cip.IdNLinea = l.IdNLinea 
				left join LineaVentaReden reden on reden.IdVenta = l.IdVenta AND reden.IdNLinea = l.IdNLinea ".($lastTsProduct ? " WHERE CAST(v.FechaHora AS DATE) <= '".substr($lastTsProduct,0,10)."'" : '')." order by v.FechaHora desc",
			'resource' => 'product',
			'service' => 'synchronize',
		);

		$lastTsSale = Sale::lastTs();


		$res[] = (object)array(
			'query' => "select TOP 20000 FechaHora as moment, IdVenta as sale_id, XVend_IdVendedor as seller_code, Ejercicio as excercise, TotalBase as total_base, TotalCuota as total_quota, TotalVentaBruta as total_raw, TotalVenta as total from Venta ".($lastTsSale ? " WHERE CAST(FechaHora AS DATE) <= '".substr($lastTsSale,0,10)."'" : '')." order by FechaHora desc",
			'resource' => 'sale',
			'service' => 'synchronize',
		);

		return $this->makeResponse($this->success,$res);
	}

}