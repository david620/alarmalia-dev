<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Banner;
use App\Models\Component;
use App\Models\Clients\Contract;
use App\Models\Company;
use App\Models\Cliente;
use App\Models\HomePage;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\Post;
use App\Models\Advertise;
use App\Models\Form;
use App\Models\Contact;
use App\Models\Glosary;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Leads\Lead;
use App\Models\Leads\LeadResponse;
use App\Models\BlackListMobile;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;
use Mail;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $companies = Cliente::orderBy('id','ASC')->where('show_web', '=', 1)->get();
        $homepages = HomePage::all()->first();
        $forms = Form::where([['status', '=', 1], ['long_short', '=', 0]])->get();
        $formu = null;

        foreach ($forms as $form) {
            $ids = array_map('intval', explode(',', $form->pageform));

                                                    for($i = 0; $i < count($ids); $i++){
                                                        if($ids[$i] == 1){
                                                            $formu = $form;
                                                        }
                                                    }

        }

        if(!is_null($formu)){
            $question = Question::where([['form_id', '=', $formu['id']]])->first();
            $answers = Answer::where('question_id', '=', $question['id'])->get();
            return view('welcome', compact('banners', 'companies', 'homepages', 'formu', 'question', 'answers'));
        }

            return view('welcome', compact('banners', 'companies', 'homepages', 'formu'));

    }


    public function robots(Request $request)
    {
        if(Storage::disk('publico')->has('robots.txt')){
            $content = Storage::disk('publico')->get('robots.txt');
            return view('pages.robots', compact('content'));
        }
        else{
            $content = "";
            return view('pages.robots', compact('content'));
        }
    }

    public function robots_create(Request $request)
    {
        if(Storage::disk('publico')->has('robots.txt')){
            Storage::disk('publico')->put('robots.txt', $request->robots);
                return redirect()->route('robots.index');
        }
    }


    public function homepage(Request $request)
    {
        $homepages = HomePage::all()->first();
        return view('pages.home', compact('homepages'));
    }

    public function companies_list(Request $request)
    {
        $homepages = HomePage::all()->first();
        $banners = Banner::orderBy('id','DESC')->first();
        //$companies = Cliente::orderBy('id','ASC')->paginate(6);
        $companies = Cliente::orderBy('id','ASC')->where('show_web', '=', 1)->get();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 2]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('company_list', compact('banners', 'companies', 'homepages', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('company_list', compact('banners', 'companies', 'homepages', 'form', 'question', 'answers'));
    }

    public function company(Request $request, $id)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $companies = Cliente::where('id','=', $id)->first();
        $homepages = HomePage::all()->first();
        $components = Component::where([['company_id','=', $companies->id], ['component_accessory', '=', 0]])->get();
        $components_acc = Component::where([['company_id','=', $companies->id], ['component_accessory', '=', 1]])->get();

        return view('company', compact('banners', 'companies', 'homepages', 'components', 'components_acc'));
    }

    public function homealarms(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $homepages = HomePage::all()->first();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 3]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('alarmas_casa', compact('banners', 'homepages', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();

        return view('alarmas_casa', compact('banners', 'homepages', 'form', 'question', 'answers'));
    }

    public function business_alarms(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $homepages = HomePage::all()->first();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 4]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('alarmas_negocio', compact('banners', 'homepages', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('alarmas_negocio', compact('banners', 'homepages', 'form', 'question', 'answers'));
    }

    public function advertise(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $homepages = HomePage::all()->first();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 8]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('anunciate', compact('banners', 'homepages', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('anunciate', compact('banners', 'homepages', 'form', 'question', 'answers'));
    }
    public function faqs(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $faqs = Faq::all();
        $categories = FaqCategory::where('status', '=', '1')->get();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 5]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('faqs', compact('faqs', 'banners', 'categories', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();

        return view('faqs', compact('faqs', 'banners', 'categories', 'form', 'question', 'answers'));
    }

    public function blog(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $posts = Post::orderBy('id','ASC')->where('status', '=', 1)->paginate(6);
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 6]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('blog', compact('posts', 'banners', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();

        return view('blog', compact('posts', 'banners', 'form', 'question', 'answers'));

    }

    function ipToCountry($ip){
        $data['ts'] = time();
        $data['id'] = 'D91F51C2AD84E';
        $data['ip'] = $ip;
        $data['sign'] = hash('sha256',$ip.$data['ts'].$data['id'].':kwBZ(+d:Nah_:j0v+?hZqj1c-?d-X');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'https://tictac.umust.io/ip.php?'.http_build_query($data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);


        $httpcode = curl_getinfo($ch,CURLINFO_HTTP_CODE);

        curl_close ($ch);

        return $httpcode == 200 ? $res : false;
    }



    public function comparator(Request $request)
    {
        $preguntas = 1;
        $form = Form::where([['status', '=', 1], ['long_short', '=', 1]])->first();
        $first_question = Question::where([['form_id', '=', $form->id], ['first_question', '=', 1]])->first();
        $answers = Answer::where('question_id', '=', $first_question->id)->get();

        $array = Session::push('contador', $first_question->id);

        $lead = new Lead();
        $lead->ip = $request->ip();
        $lead->origin = $request->url();
        $lead->lead_type = 1; // 1 Lead Largo

        $country =  $this->ipToCountry($request->ip());
        $lead->country = $country;

        $lead->save();

        $lead = Lead::find($lead->id);
        $lead->created_real = $lead->created_at;
        $lead->update();


        $cuenta = count($questions = Question::where('form_id', '=', $form->id)->get());

        $porcentaje = (integer)(($preguntas*100)/($cuenta));

        Session::put('comparador', [$preguntas, $porcentaje, $lead, $first_question]);

        $alfabeto = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

        return view('comparador', compact('form', 'first_question', 'answers', 'porcentaje', 'preguntas', 'alfabeto'));
    }
    public function comparator_camino(Request $request, $id)
    {
        $session = Session::get('comparador');
        $array = Session::get('contador');
        $form = Form::where([['status', '=', 1], ['long_short', '=', 1]])->first();
        $question = Question::where('id', '=', $id)->first();
//        $question_arr = Question::where('id', '=', end($array))->first();

        $answers = Answer::where('question_id', '=', $question->id)->get();
        $alfabeto = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

        $preguntas = $session[0] + 1;

                $cuenta = (integer)((count($questions = Question::where('form_id', '=', $form->id)->get()))/2) ;
                $porcentaje = (integer)(($preguntas*100)/($cuenta));

            if($question->penultimate_question == 1){
                    $cuenta = count($array);
                    $porcentaje2 = (integer)(((($preguntas) *100)/($cuenta)));
                    if($porcentaje >= $porcentaje2){
                        $porcentaje2 = $porcentaje + 2;
                    }
                    if($porcentaje2 >= 100){
                        $porcentaje2 = (integer)(99 - 5);
                    }

                $porcentaje = $porcentaje2;
            }



            if($question->last_question == 1){
                $porcentaje = (integer)(99);
            }

        $lead = Lead::find($session[2]->id);
        $lead->origin = $request->url();
        $lead->update();

        Session::put('comparador', [$preguntas, $porcentaje, $lead, $question]);

        return view('comparador_camino', compact('form', 'question', 'answers', 'preguntas', 'porcentaje', 'alfabeto'));
    }

    public function end_comparator(Request $request)
    {
        $session = Session::get('comparador');
//        $contracts = Contract::orderBy('due','DESC')->whereRaw('due >= CURRENT_DATE')->get();
        $contracts = DB::table('client_contract')
            ->join('client', 'client.code', '=', 'client_contract.client_code')
            ->whereRaw('due >= CURRENT_DATE and show_web = 1')
            ->orWhere('show_web', '=', 1)
            ->inRandomOrder()
            ->select('client.id as client_id', 'client.precio_desde', 'client.precio_hasta', 'client.precio_desde', 'client.estrellas', 'client.link_image', 'client.url_web' )
            ->distinct()
            ->paginate(6);

        $request->session()->forget('contador');

        return view('end_comparador', compact('contracts'));
    }


    public function form_short(Request $request)
    {
            $lead = new Lead();
            $lead->ip = $request->ip();
            $lead->origin = $request->url();
            $lead->country = $this->ipToCountry($request->ip());
            $lead->phone = $request->phone;
            $lead->lead_type = 0; // 0 Lead Corto

            $lead->save();

            $lead = Lead::find($lead->id);
            $lead->created_real = $lead->created_at;
            $lead->update();

            $question = Question::where('id', '=', $request->q_id)->first();

            $lead_response = new LeadResponse();
            $lead_response->lead_id = $lead->id;
            $lead_response->question = $question->title;
            $lead_response->response = $request->resp;
            $lead_response->save();

            return redirect()->route('site.home');

    }


    public function store_comparator(Request $request)
    {
            $session = Session::get('comparador');
            $array = Session::get('contador');
            $answer = Answer::where('id', '=', $request->q_answer)->get();
            $question = Question::where('id', '=', $answer[0]->question_id)->get();
            Session::push('contador', $question[0]->id);

            $lead = Lead::find($session[2]->id);
            $lead->origin = $request->url();
            $lead->update();

            array_push($array, $question[0]->id);


            $lead_response = new LeadResponse();
            $lead_response->lead_id = $session[2]->id;
            $lead_response->question = $question[0]->title;

            if($answer[0]->answer_type == 4 || $answer[0]->answer_type == 1 || $answer[0]->answer_type == 3 ){
                $lead_response->response = $request->content;
                $lead_response->update();


                if($answer[0]->answer_type == 1){
                    $lead->postal_code = $request->content;
                    $lead->update();
                }
                if($answer[0]->answer_type == 3){

                    if(BlackListMobile::isIn($request->content)) $lead->postal_code = '**'.$lead->postal_code;
//                    else if(BlackListMobile::feed($request->content)) $lead->postal_code = '***'.$lead->postal_code;

                    $lead->phone = $request->content;
                    $lead->update();
                }
            }
            else{
                $lead_response->response = $answer[0]->content;
                $lead_response->save();



                if($answer[0]->answer_type == 1){
                    $lead = Lead::find($session[2]->id);
                    $lead->postal_code = $answer[0]->content;
                    $lead->save();
                }
                if($answer[0]->answer_type == 3){
                    $lead = Lead::find($session[2]->id);
                    if(BlackListMobile::isIn($answer[0]->content)) $lead->postal_code = '**'.$lead->postal_code;
  //                  else if(BlackListMobile::feed($answer[0]->content)) $lead->postal_code = '***'.$lead->postal_code;
                    $lead->phone = $answer[0]->content;
                    $lead->save();
                }
            }
                $lead_response->save();
/*
                $invalidate = Lead::invalidateFromIp($lead->ip,$lead->phone);
                if($invalidate){
                    $lead->deleted_at = date('Y-m-d H:i:s');
                    $lead->save();
                }
*/



            if($question[0]->last_question == 1){
                return redirect()->route('companies.end_comparator');
            }

            return redirect()->route('companies.comparator_camino', $answer[0]->next_question);
    }

    public function about_us(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 7]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('about_us', compact('banners', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('about_us', compact('banners', 'form', 'question', 'answers'));
    }

    public function policies(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $posts = Post::orderBy('id','ASC')->get();
        return view('politica_privacidad', compact('banners'));
    }
    public function avisolegal(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $posts = Post::orderBy('id','ASC')->get();
        return view('avisolegal', compact('banners'));
    }

    public function contact(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 9]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('contacto', compact('banners','form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('contacto', compact('banners','form', 'question', 'answers'));

    }

    public function glosary(Request $request)
    {
        $companies = Company::orderBy('id','ASC')->get();
        $glosaries = Glosary::orderBy('name','ASC')->get();
        $banners = Banner::orderBy('id','DESC')->first();
        $abc = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,23,25,26,27];
        $form = Form::where([['status', '=', 1], ['long_short', '=', 0], ['pageform', '=', 10]])->first();
        if($form == null){
            $question = null;
            $answers = null;
        return view('glosario', compact('banners', 'glosaries', 'companies', 'abc', 'form', 'question', 'answers'));
        }
        $question = Question::where([['form_id', '=', $form->id]])->first();
        $answers = Answer::where('question_id', '=', $question->id)->get();
        return view('glosario', compact('banners', 'glosaries', 'companies', 'abc', 'form', 'question', 'answers'));

    }

    public function home_store(Request $request)
    {

        $this->validate($request, [
            'description_home' => 'required',
            'select_alarm' => 'required',
            'desc_one' => 'required',
            'desc_two' => 'required',
            'desc_three' => 'required',
            'description_middle' => 'required',
            'card_one' => 'required',
            'card_two' => 'required',
            'card_three' => 'required',
            'card_four' => 'required',
            'card_five' => 'required',
            'card_six' => 'required',

        ]);

        DB::beginTransaction();

        $homepage = HomePage::all()->first();
        $homepage->description_home = $request->description_home;
        $homepage->select_alarm = $request->select_alarm;
        $homepage->desc_one = $request->desc_one;
        $homepage->desc_two = $request->desc_two;
        $homepage->desc_three = $request->desc_three;
        $homepage->description_middle = $request->description_middle;
        $homepage->card_one = $request->card_one;
        $homepage->card_two = $request->card_two;
        $homepage->card_three = $request->card_three;
        $homepage->card_four = $request->card_four;
        $homepage->card_five = $request->card_five;
        $homepage->card_six = $request->card_six;
        $homepage->save();

        if (!$homepage) {
            DB::rollBack();
            return redirect()->route('homepage.index')->with('alert-danger', 'Error Editando Home Page');
        }

        DB::commit();

        return redirect()->route('homepage.index')->with('message', 'Home Page Editado Exitosamente.');
    }

    public function advertise_store(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();

        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'company_name' => 'required',
            'company_email' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'terms' => 'required',
        ]);

        DB::beginTransaction();

        $advertise = New Advertise();
        $advertise->name = $request->name;
        $advertise->lastname = $request->lastname;
        $advertise->company_name = $request->company_name;
        $advertise->company_email = $request->company_email;
        $advertise->phone = $request->phone;
        $advertise->message = $request->message;
        $advertise->terms = $request->terms;
        $advertise->save();

        if (!$advertise) {
            DB::rollBack();
            return redirect()->route('companies.advertise')->with('alert-danger', 'Error Editando Home Page');
        }

        DB::commit();

                    $data['advertise'] = $advertise;
                    $target = 'plarramendi@farmatool.com';
//                    $target = 'davidacosta309@gmail.com';

                    Mail::send('Clients.Views.emails.advertise_client',$data,function($message) use($target){
                        $message->to($target)
                            ->from('no-reply@alarmator.es','Alarmalia')
                            ->subject('Alarmalia - Nuevo Mensaje - Formulario Anunciate');
                    });


        return view('anunciate.done',compact('banners'));
    }


    public function contact_store(Request $request)
    {
        $banners = Banner::orderBy('id','DESC')->first();

        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'terms' => 'required',
        ]);

        DB::beginTransaction();

        $contact = New Contact();
        $contact->name = $request->name;
        $contact->lastname = $request->lastname;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $contact->terms = $request->terms;
        $contact->save();

        if (!$contact) {
            DB::rollBack();
            return redirect()->route('companies.contact')->with('alert-danger', 'Error Editando Contácto');
        }

        DB::commit();


                    $data['contact'] = $contact;
                    $target = 'plarramendi@farmatool.com';
//                    $target = 'davidacosta309@gmail.com';

                    Mail::send('Clients.Views.emails.contact_client',$data,function($message) use($target){
                        $message->to($target)
                            ->from('no-reply@alarmator.es','Alarmalia')
                            ->subject('Alarmalia - Nuevo Mensaje - Formulario Contacto');
                    });





        return view('contacto.done',compact('banners'));
    }


}
