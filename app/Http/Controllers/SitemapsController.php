<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Banner;
use App\Models\Clients\Contract;
use App\Models\Company;
use App\Models\Cliente;
use App\Models\HomePage;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\Post;
use App\Models\Advertise;
use App\Models\Form;
use App\Models\Contact;
use App\Models\Glosary;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Leads\Lead;
use App\Models\Leads\LeadResponse;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;
use Sitemap;
use Page;

class SitemapsController extends Controller
{


    public function index(Request $request)
    {
        SitemapGenerator::create('https://alarmalia.com')->writeToFile(public_path('sitemap.xml'));
        echo "SITEMAP GENERADO CON EXITO";

    }

    public function posts()
    {
        $posts = Post::all();

        foreach ($posts as $post) {
            Sitemap::addTag(route('post.blog', $post), $post->updated_at, 'daily', '0.8');
        }

        return Sitemap::render();
    }

    public function pages()
    {
        $pages = Page::all();

        foreach ($pages as $page) {
            $tag = Sitemap::addTag(route('pages.show', $page), $page->updated_at, 'daily', '0.8');

            foreach ($page->images as $image) {
                $tag->addImage($image->url, $image->caption);
            }
        }

        return Sitemap::render();
    }



}