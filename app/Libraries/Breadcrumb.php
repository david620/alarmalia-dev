<?php

namespace App\Libraries;


class Breadcrumb{
	public $routeName = 'home';
	public $routeParams = array();
	public $icon = '';
	public $title = '';

	function __construct($routeName,$icon,$title,$routeParams = array()){
		$this->routeName = $routeName;
		$this->routeParams = $routeParams;
		$this->icon = $icon;
		$this->title = $title;
	}
}