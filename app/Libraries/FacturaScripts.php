<?php

namespace App\Libraries;

class FacturaScripts{
	private static $privateKey = ":{@LOM7YL7{p0ZT)8=(?-44H9=Eeim##2018";
	private static $publicKey = '41342FAGFF5Edsf427';
	private static $username = 'admin';

	private static function endpoint($query){
		return 'http://clapp.online/e/TICTAC/index.php?page=api&resource='.$query;
	}

	private static function makeRequest($url,$data = array()){
		
		$timestamp = date('Ymd-His');
		$signature = sha1(self::$privateKey.$timestamp.self::$publicKey.self::$username);

		$headers = array(
		    'X-Fs-Signature: '.$signature, 
		    'X-Fs-Timestamp: '.$timestamp, 
		    'X-Fs-Publickey: '.self::$publicKey, 
		    'X-Fs-Username: '.self::$username, 
		    'X-Fs-Exercice: '.date('Y'), 
	
		);

		$curl = curl_init();

		if(count($data)){
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
      		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($data));
		}

		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_URL => $url,
		    CURLOPT_HTTPHEADER => $headers
		));

		$resp = curl_exec($curl);

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
		curl_close($curl);
		return array($httpcode,$resp);

	}

	function clientList(){
		$url = self::endpoint('cliente&action=list');

		list($code,$result) = self::makeRequest($url);

		$res = json_decode($result,1);
		
		return isset($res['success']) && $res['success'] ? $res['data'] : array();
	}

	function synchroAlbaranCabecera($client,$contract,$leads){

		$albaran = array();

		$validatedDate = date('Y-m-d H:i:s');

		$albaran['codcliente'] = $client->code;
		$albaran['codagente'] = 1;
		$albaran['codpago'] = 'TRANS';
		$albaran['coddivisa'] = 'EUR';
		$albaran['numero2'] = time().'.'.$contract->id;
		$albaran['nombrecliente'] = $client->name;
		$albaran['cifnif'] = $client->cifnif;


		$albaran['fecha'] = date('Y-m-d',strtotime($validatedDate));
		$albaran['hora'] = date('H:i',strtotime($validatedDate));
		
		$albaran['porcomision'] = 0;
		$albaran['codalmacen'] = 'ALG';
		$albaran['codserie'] = 'A';
		$albaran['observaciones'] = '';
		$albaran['ptefactura'] = 1;

		$totalNeto = 0;
		$total = 0;
		$totalIva = 0;
		$lineas = array();

		$iva = 21;
		$totLines = 0;
		foreach($leads as $k => $l){
			$noIvaPrice = ($l->cost/(1+($iva/100)));
			$subtotal = $l->is_valid ? ($noIvaPrice*1) : 0;
			$tmpIva = ($l->cost*1);

			$lineas[] = array(
				'referencia' => $l->phone.'.'.$k,
				'descripcion' => $l->postal_code.($l->is_valid ? '' : ' -INVÁLIDO-'),
				'cantidad' => 1,
				'dtopor' => ($l->is_valid ? $l->discount : 100),
				'codimpuesto' => '0IVA21',
				'iva' => $iva,
				'pvptotal' => $subtotal,
				'pvpsindto' => $subtotal,
				'pvpunitario' => $noIvaPrice,
				'irpf' => 0,
				'mostrar_cantidad' => 1,
				'mostrar_precio' => 1

			);

			$totalNeto += $subtotal;
			$total += $tmpIva;
			$totalIva += $l->is_valid ? ($tmpIva-$subtotal) : 0;

			$totLines++;

		}

		$albaran['neto'] = round($totalNeto,4);
		$albaran['total'] = round($total,4);
		$albaran['totaliva'] = round($totalIva,4);


		$data = array('cabecera' => $albaran, 'lineas' => $lineas);



		$url = self::endpoint('albaran_cliente&action=create');

		list($code,$result) = self::makeRequest($url,$data);

		echo $result;exit;

		unset($data);
		
		$result = json_decode($result,true);
		$lineasOk = isset($result['data']['lineas']) ? $result['data']['lineas'] : 0;
	
		return $totLines == $lineasOk;
	
	}
}

