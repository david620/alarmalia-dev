<?php

namespace App\Libraries;
use App;

class Google{
	const MAPS_KEY = '';
	const MAPS_KEY_DEV = '';

	static function addressToLatLng($address){


		$key = self::getKey();

		$address = str_replace(' ','+',$address);
		$endpoint = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$key;
		$geocode = file_get_contents($endpoint);

		$output = json_decode($geocode);

		return isset($output->results[0]->geometry->location->lat) ? array($output->results[0]->geometry->location->lat,$output->results[0]->geometry->location->lng) : array(0,0);
	}

	static function getKey(){
		return App::environment('local') ? self::MAPS_KEY_DEV : self::MAPS_KEY;;
	}
}