<?php

namespace App\Libraries;
use Config;

class MasterSQL{

	protected $mysqli;

	function __construct($dbName){
		$this->mysqli = new \mysqli("localhost", Config::get('master.db_username'), Config::get('master.db_pass'), $dbName);
	}

	function query($query){
		$this->mysqli->query($query);
	}

	function tableExists($viewName){
		return $this->mysqli->query('DESCRIBE '.$viewName);
	}

	function close(){
		$this->mysqli->close();
	}

}