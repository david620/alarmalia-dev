<?php

namespace App\Libraries;

use Route;
use App\Models\RoleData;
use Auth;

class Pipe{
	static function validateTime($time){
		$parts = explode(':',$time);
		if(count($parts) == 2) return $time;
		return false;
	}

	static function normalizeDateTime($datetime,$format){
		$tmp =  \DateTime::createFromFormat($format,$datetime);
		return $tmp ? $tmp->format('Y-m-d H:i:s') : NULL;
	}

	static function months(){
		return array(
			1 => 'app.january',
			2 => 'app.february',
			3 => 'app.march',
			4 => 'app.april',
			5 => 'app.may',
			6 => 'app.june',
			7 => 'app.july',
			8 => 'app.august',
			9 => 'app.september',
			10 => 'app.october',
			11 => 'app.november',
			12 => 'app.december',
		);
	}

	static function roleRoutes($modules = array()){

		$res = array();
		foreach(Route::getRoutes() as $r){
			if(!in_array('roles',$r->middleware())) continue; //route must have the "roles" middleware
			$action = $r->getAction();
            if(!isset($action['module'])) continue; //route must be well formed
            if(!in_array($action['module'],$modules)) continue; //we dont print routes belonging to unactivated modules
			
			$res[] = $r;
		}
		return $res;
	}

	static function validateRoute($route){

		if(is_array($route)){
			return count(array_intersect($route)) > 0;
		}

		return $route;
	}

	static function genOauthCredentials($clientId) {
		// Get a whole bunch of random characters from the OS
		$fp = fopen('/dev/urandom','rb');
		$entropy = fread($fp, 32);
		fclose($fp);


		// Takes our binary entropy, and concatenates a string which represents the current time to the microsecond
		$entropy .= uniqid(mt_rand(), true);

		// Hash the binary entropy
		$hash = hash('sha512', $entropy);

		$private = substr(base64_encode($hash),100);
		$public = strtoupper(md5(microtime().rand())).$clientId;
	
		return array(
			$private,
			$public
		);
	}

	public static function formatNum($num){
    	return number_format(round($num,2), 2, ',', '.');
    }

	public static function validateTimestamp($ts){
        
        $tmp = explode('_',$ts);
        if(count($tmp) !== 2) return false;
        else if(strlen($tmp[0]) !== 8) return false;
        else if(strlen($tmp[1]) !== 6 && strlen($tmp[1]) !== 4) return false; //with and without seconds
        $year = substr($tmp[0],0,4);
        $month  = substr($tmp[0],4,2);
        $day = substr($tmp[0],6,2);

        if(($year != date('Y')) || ($month != date('m')) || ($day != date('d'))) return false;

        $hour = substr($tmp[1],0,2);
        $minutes = substr($tmp[1],2,2);

        $now = strtotime(date('Y-m-d H:i'));
        $tsDate = strtotime(sprintf('%s-%s-%s %s:%s',$year,$month,$day,$hour,$minutes));
        $diff = ($tsDate - $now) / 60;

        if($diff > 10 || $diff < -10) return false; //not more than 10 minutes and not less than 10 minutes
        
        return true;
    }
	
}