<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Libraries\SMSUP;

class BlackListMobile extends Model{
	protected $table = 'blacklist_mobile';

	static function isIn($num){
		return self::where('value',$num)->count() > 0;
	}

	static function feed($num){
		list($ok,$data) = SMSUP::hlr($num);
		if($ok) return false;
		else{
			$bl = new BlackListMobile;
			$bl->value = $num;
			$bl->data = $data;
			$bl->save();

			return true;
		}
		
	}

	static function addbl($num){
		list($ok,$data) = SMSUP::hlr($num);
		if($ok){
			$bl = new BlackListMobile;
			$bl->value = $num;
			$bl->data = $data;
			$bl->save();
			return true;					
		}

	}

}
