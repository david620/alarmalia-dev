<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallCenter extends Model {

    protected $table = 'callcenter';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}