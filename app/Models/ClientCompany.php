<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class ClientCompany extends Model {

    protected $table = 'client_company';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}