<?php
namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\User;
use Auth;


class Client extends Model{

	protected $table = 'client';
	public $primaryKey = 'code';
	public $incrementing = false;
	
	static function toList($paginate = true){
		$request = app('request');
		$tmp = self::orderBy('name');
		
		$name = $request->get('name_filter','');
		if(!empty($name)) $tmp->whereRaw('name like ?',array('%'.$name.'%'));
		
		if($paginate == true){
			return $tmp->paginate(50);
		}
		else{
	        $user = User::find(Auth::id());
	        if($user->roles()->first()->name != "Administrador" && $user->roles()->first()->name != null){
		        $tmp->where('id', '=', $user->company_id);	        	
			 	return $tmp->get();
	        }
	        else{
			 	return $tmp->get();	        	
	        }
		}

	}

	static function findById($id){
		return self::where('id',$id)->first();
	}

	static function toCompany(){
		return self::orderBy('name')->get();
	}

	static function cleanCompany($company){
		return self::where('company',$company)->update(array('company' => null));
	}

	function __toString(){
		return $this->name;
	}

	function setRecentInList(){
		Session::put('RECENT_'.$this->table,$this->id);
	}

	function isRecentInList(){
		return Session::get('RECENT_'.$this->table,-1) == $this->id;
	}
}