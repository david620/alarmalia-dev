<?php
namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;

use Session;
use DB;

class Company extends Model{

	protected $table = 'client_company';


	static function toList(){
		return self::orderBy('name')
		->leftJoin('client as c','c.company','=','client_company.id')
		->select('client_company.*',DB::raw('group_concat(c.name SEPARATOR \', \') as clients'))
		->groupBy('client_company.id')
		->paginate(100);
	}

	function setRecentInList(){
		Session::put('RECENT_'.$this->table,$this->id);
	}

	function isRecentInList(){
		return Session::get('RECENT_'.$this->table,-1) == $this->id;
	}
}