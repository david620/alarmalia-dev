<?php
namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Clients\ContractLead;
use App\Models\LeadCost;
use App\Models\LeadResponse;
use App\User;
use Auth;
use Session;
use DB;

class Contract extends Model{
	use SoftDeletes;


	protected $table = 'client_contract';
	protected $fillable = array('client_code','due','budget_cash','budget_leads','budget_type','lead_cost','transference_type','payment_type','email','discount','postals','priority', 'm2_size', 'questions_ids', 'type_lead');

	protected $dates = ['deleted_at'];

	protected $appends = array('transference_type_trans');

	function __construct(){
		if(!$this->id) $this->priority = 1;
	}

	static function toList(){
		$request = app('request');

		$tmp = self::orderBy('due','desc');

		$clientCode = $request->input('client_code','');
		if($clientCode) $tmp->where('client_code',$clientCode);

			$tmp->join('client as c','c.code','=','client_code')->select('client_contract.id', 'client_code', 'due', 'budget_cash', 'budget_leads', 'budget_type', 'lead_cost', 'transference_type', 'payment_type', 'discount', 'postals', 'priority', 'c.id as id_company', 'client_contract.email', 'client_contract.created_at', 'client_contract.updated_at', 'client_contract.deleted_at');
		$name = $request->input('name_filter','');
		if(!empty($name)){
			$tmp->whereRaw('c.name like ?',array('%'.$name.'%'));
		}

		$tmp->whereRaw('deleted_at is null');

		$onlyActives = $request->input('onlyActives',0);
		if($onlyActives) $tmp->whereRaw('due >= CURRENT_DATE');

        $user = User::find(Auth::id());

        if($user->roles()->first()->name != "Administrador" && $user->roles()->first()->name != null){
			$tmp->where('c.id','=', $user->company_id);
        }

		return $tmp->paginate(20);
	}

	function client(){
		return $this->hasOne('App\Models\Clients\Client','code','client_code');
	}

	function maxDueFromClient(){
		return self::whereRaw('client_code = ? and deleted_at is null',array($this->client_code))->max('due');
	}

	function setRecentInList(){
		Session::put('RECENT_'.$this->table,$this->id);
	}

	function isRecentInList(){
		return Session::get('RECENT_'.$this->table,-1) == $this->id;
	}

	function getTransferenceTypeTransAttribute(){
		return isset(self::transferenceTypes()[$this->transference_type]) ? self::transferenceTypes()[$this->transference_type] : $this->transference_type;
	}

	function assignedLeads(){
		return ContractLead::fromContract($this->id);
	}

	function counters(){
		return ContractLead::counters($this->id);
	}

	static function transferenceTypes(){
		return array(
			0 => trans('contracts.instantly'),
			1 => trans('contracts.deferred'),
			2 => trans('contracts.verified')
		);
	}

	static function toSell($leadId, $cpPrefix, $ans){

		$cpPrefix = '%^'.$cpPrefix.'^%';
		$ans = '%^'.$ans.'^%';

		//order by priority and number of leads

		$MAX_SELLINGS = LeadCost::total(); //máximo número de contratos
		//tener en cuenta la empresa: si tiene empresa entonces vender una sóla vez. Obtener empresa: si no empresa, vender, si empresa y empresa ya está en el array, eliminar.

		//muy importante. Como todavía no sabemos el coste del lead, puede ser que se pase el presupuesto del cliente si el budget en euros no es múltiplo del coste

		$tmp =  DB::connection('mysql')
		->select("select c.*, count(distinct ccl.id) as total_leads, (case when sum(ccl.cost) is not null then sum(ccl.cost) else 0 end)  as total_cost, 
					(select count(*) from client_contract_lead as cc where cc.client_contract_id = c.id and cc.lead_id = ?) as already_sold,
					cli.company 
					from client_contract c
					inner join client as cli on cli.code = c.client_code
					left join client_contract_lead ccl on ccl.client_contract_id =  c.id
					where c.deleted_at is null and due >= CURRENT_DATE and (postals like ? or questions_ids like ?) 
					group by c.id 
					having already_sold  < 1 and ((c.budget_type = 'cash' and  total_cost < c.budget_cash) or (c.budget_type = 'leads' and total_leads < budget_leads))
					order by c.priority, total_leads 
					",array($leadId,$cpPrefix,$ans));

		$final = array();
		$companies = array();

/*
		$m2_lead = LeadResponse::where([['lead_id', '=', $leadId], ['question', '=', '¿Cuántos metros cuadrados mide tu vivienda aproximadamente?']])->first();
		if($m2_lead){
			$m2_lead = (int)($m2_lead->response);		

			for ($i=0; $i < sizeof($tmp); $i++) { 
				if($tmp[$i]->m2_size != null){
					if($tmp[$i]->m2_size < $m2_lead ){
						unset($tmp[$i]);				
					}				
				}
			}

		}
*/
		foreach($tmp as $k => $t){

			if($MAX_SELLINGS == count($final)){
				break;
			}
			if(!$t->company){
				$final[] = $t;
			}
			else{
				if(!in_array($t->company,$companies)) $final[] = $t;
				$companies[] = $t->company;
			}

		}
		unset($tmp);
		return $final;

	}


	static function toSellShort($leadId){


		$MAX_SELLINGS = LeadCost::total(); 

		$tmp =  DB::connection('mysql')
		->select("select c.*, count(distinct ccl.id) as total_leads, (case when sum(ccl.cost) is not null then sum(ccl.cost) else 0 end)  as total_cost, 
					(select count(*) from client_contract_lead as cc where cc.client_contract_id = c.id and cc.lead_id = ?) as already_sold,
					cli.company 
					from client_contract c
					inner join client as cli on cli.code = c.client_code
					left join client_contract_lead ccl on ccl.client_contract_id =  c.id
					where c.deleted_at is null and due >= CURRENT_DATE  
					group by c.id 
					having already_sold  < 1 and ((c.budget_type = 'cash' and  total_cost < c.budget_cash) or (c.budget_type = 'leads' and total_leads < budget_leads))
					order by c.priority, total_leads 
					",array($leadId));

		$final = array();
		$companies = array();


		foreach($tmp as $k => $t){

			if($MAX_SELLINGS == count($final)){
				break;
			}
			if(!$t->company){
				$final[] = $t;
			}
			else{
				if(!in_array($t->company,$companies)) $final[] = $t;
				$companies[] = $t->company;
			}

		}
		unset($tmp);
		return $final;

	}


}