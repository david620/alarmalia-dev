<?php
namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;
use App\Models\Clients\Contract;
use App\Models\Leads\Lead;
use Session;
use DB;

class ContractLead extends Model{

	protected $table = 'client_contract_lead';

	protected $appends = array('transference_type_trans','is_valid');


	static function toSend(){
		return self::join('client_contract as cc','cc.id','=','client_contract_id')
		->select('client_contract_lead.*','cc.email','cc.client_code')
		->where('sent',0)
		->where('active',1)
		->orderBy('created_at','desc')
		->get();
	}

	static function toErp(){
		return self::whereRaw('at_erp is null and sent = 1')
		->orderBy('id','desc')
		->groupBy('client_contract_id')
		->get();
	}

	static function pendingDeferred(){
		return self::whereRaw("active = 0 and TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, created_at)) >= ?",array(Lead::INSTANTLY_SECONDS))
		->get();
	}

	static function fromContract($id){
		return self::where('client_contract_id',$id)
		->join('lead as l','l.id','=','lead_id')
		->select('client_contract_lead.*','postal_code','phone','created_real','country','l.percent_invalid',
			DB::raw('(select count(distinct li.client_code) from lead_invalid li where li.lead_id = l.id) as times_invalid'),
			DB::raw('(select count(distinct ccl.client_contract_id) from client_contract_lead ccl where ccl.lead_id = l.id) as times_sold'),
			DB::raw('li.times as invalid_clicks_client, l.invalid_master_client')
		)
		->join('client_contract as c','c.id','=','client_contract_id')
		->leftJoin('lead_invalid as li',function($join){
			$join->on('li.lead_id','=','l.id')
			->on('li.client_code','=','c.client_code');
		})
		->groupBy('client_contract_lead.id')
		->orderBy('client_contract_lead.created_at','desc')
		->get();
	}

	static function counters($contractId){
		return self::where('client_contract_id',array($contractId))
		->select(DB::raw("count(*) as total, sum(cost) as cost"))
		->first();
	}

	function getIsValidAttribute(){
		if(!$this->times_sold || !$this->times_invalid) return true;
		return ($this->times_invalid/$this->times_sold)*100 <=  $this->percent_invalid;
	}

	function getTransferenceTypeTransAttribute(){
		return isset(Contract::transferenceTypes()[$this->transference_type]) ? Contract::transferenceTypes()[$this->transference_type] : $this->transference_type;
	}

	function lead(){
		return $this->hasOne('App\Models\Leads\Lead','id','lead_id');
	}

	function contract(){
		return $this->hasOne('App\Models\Clients\Contract','id','client_contract_id');
	}
}