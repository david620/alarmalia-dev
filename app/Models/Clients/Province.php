<?php
namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;

class Province extends Model{

	protected $table = 'provinces';
	public $primaryKey = 'code';
	public $incrementing = false;
	
	static function toList(){
		return self::orderBy('name')->get();
	}

}