<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class ComponentCaract extends Model {

    protected $table = 'components_caract';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}