<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Clients\Models\ContractLead;
use App\Models\LeadCost;
use Session;
use DB;

class Contract extends Model{
	use SoftDeletes;
	protected $table = 'client_contract';
	protected $fillable = array('client_code','due','budget_cash','budget_leads','budget_type','lead_cost','transference_type','payment_type','email','discount','postals','priority');

	protected $dates = ['deleted_at'];

	protected $appends = array('transference_type_trans');

	function __construct(){
		if(!$this->id) $this->priority = 1;
	}

	static function toList(){
		$request = app('request');

		$tmp = self::orderBy('due','desc');

		$clientCode = $request->input('client_code','');
		if($clientCode) $tmp->where('client_code',$clientCode);
		
		$tmp->join('client as c','c.code','=','client_code');

		$name = $request->input('name','');
		if(!empty($name)){
			$tmp->whereRaw('c.name like ?',array('%'.$name.'%'));
		}

		$tmp->whereRaw('deleted_at is null');

		$onlyActives = $request->input('onlyActives',0);
		if($onlyActives) $tmp->whereRaw('due >= CURRENT_DATE');

		return $tmp->paginate(20);
	}

	function client(){
		return $this->hasOne('App\Models\Cliente','code','client_code');
	}

	function maxDueFromClient(){
		return self::whereRaw('client_code = ? and deleted_at is null',array($this->client_code))->max('due');
	}

	function setRecentInList(){
		Session::set('RECENT_'.$this->table,$this->id);
	}

	function isRecentInList(){
		return Session::get('RECENT_'.$this->table,-1) == $this->id;
	}

	function getTransferenceTypeTransAttribute(){
		return isset(self::transferenceTypes()[$this->transference_type]) ? self::transferenceTypes()[$this->transference_type] : $this->transference_type;
	}

	function assignedLeads(){
		return ContractLead::fromContract($this->id);
	}

	function counters(){
		return ContractLead::counters($this->id);
	}

	static function transferenceTypes(){
		return array(
			0 => trans('contracts.instantly'),
			1 => trans('contracts.deferred')
		);
	}

	static function toSell($leadId, $cpPrefix){

		$cpPrefix = '%^'.$cpPrefix.'^%';

		//order by priority and number of leads

		$MAX_SELLINGS = LeadCost::total(); //máximo número de contratos

		//tener en cuenta la empresa: si tiene empresa entonces vender una sóla vez. Obtener empresa: si no empresa, vender, si empresa y empresa ya está en el array, eliminar.

		//muy importante. Como todavía no sabemos el coste del lead, puede ser que se pase el presupuesto del cliente si el budget en euros no es múltiplo del coste
		
		$tmp =  DB::connection('mysql')
		->select("select c.*, count(distinct ccl.id) as total_leads, (case when sum(ccl.cost) is not null then sum(ccl.cost) else 0 end)  as total_cost, 
(select count(*) from client_contract_lead as cc where cc.client_contract_id = c.id and cc.lead_id = ?) as already_sold,
cli.company 
from client_contract c
inner join client as cli on cli.code = c.client_code
left join client_contract_lead ccl on ccl.client_contract_id =  c.id
where c.deleted_at is null and due >= CURRENT_DATE and postals like ?
group by c.id 
having already_sold  < 1 and ((c.budget_type = 'cash' and  total_cost < c.budget_cash) or (c.budget_type = 'leads' and total_leads < budget_leads))
order by c.priority, total_leads 
",array($leadId,$cpPrefix));

		$final = array();

		$companies = array();
		foreach($tmp as $k => $t){
			if($MAX_SELLINGS == count($final)){
				break;
			}

			if(!$t->company){
				$final[] = $t;
			}
			else{
				if(!in_array($t->company,$companies)) $final[] = $t;
				$companies[] = $t->company;
			}

		}

		unset($tmp);
		return $final;

	}

}