<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use DB;

class Lead extends Model {

	use SoftDeletes;

    protected $table = 'lead';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];


	static function toList($dateIni = '',$dateFin = ''){

		$tmp = self::whereRaw('date(created_real) > ?',array('2019-02-10'))
		->select(DB::raw("lead.*,(select count(distinct c.id) from client_contract_lead as c where c.lead_id = lead.id) as times_sold"))
		->orderBy('created_real','desc');

		if(!empty($dateIni)) $tmp->whereRaw('date(created_real) >= ?',array($dateIni));
		if(!empty($dateFin)) $tmp->whereRaw('date(created_real) <= ?',array($dateFin));

		//return $tmp->paginate(20);
	}

	static function lastExtId(){
		return self::withTrashed()->orderBy('id_ext','desc')->value('id_ext');
	}

	static function exitsExt($id){
		return self::withTrashed()->where('id_ext',$id)->count() > 0;
	}

	function responses(){
		return $this->hasMany('\App\Models\Leads\LeadResponse','lead_id','id');
	}

	static function toSell($instant = true){

		$subHaving = $instant ? ' and power <= ?  and power <= ?' : ' and power > ?  and power <= ?';
		return DB::connection('mysql')->select("select l.*, count(c.id) as times_sold, TIMESTAMPDIFF(SECOND,created_real,CURRENT_TIMESTAMP)  as power 
		from lead l left join client_contract_lead c on c.lead_id = l.id
		where length(l.postal_code) = 5 and length(l.phone) > 5 and l.country = 'ES' and l.deleted_at is null group by l.id
		having times_sold < 1 $subHaving
		order by power",array(self::INSTANTLY_SECONDS,self::MAX_AGE_SECONDS));
	}


	static function invalidateFromIp($ip,$phone){
		return self::whereRaw('ip = ? and phone = ? and TIMESTAMPDIFF(HOUR,created_real,CURRENT_TIMESTAMP) <= 24',array($ip,$phone))->count() > 0;
	}

	static function invalidateByMaster($id){
		self::whereRaw('id = ? and invalid_master_client = 0',array($id))
		->update(array('invalid_master_client' => 1));
	}

}