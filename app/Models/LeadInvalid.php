<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class LeadInvalid extends Model{
	protected $table = 'lead_invalid';

	static function createOrUpdate($id,$clientCode){
		$tmp = self::whereRaw('lead_id = ? and client_code = ?',array($id,$clientCode))->first();
		if(!$tmp){
			$tmp = new LeadInvalid;
			$tmp->lead_id = $id;
			$tmp->client_code = $clientCode;
			$tmp->times = 0;
		}
		$tmp->times++;
		$tmp->save();
	}
	static function createOrUpdateValidated($id){
		$tmp = self::whereRaw('lead_id = ? ',array($id))->first();
		if(!$tmp){
			$tmp = new LeadInvalid;
			$tmp->lead_id = $id;
			$tmp->times = 0;
		}
		$tmp->times++;
		$tmp->save();
	}
}

