<?php
namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Lead extends Model{
	use SoftDeletes;

	protected $table = 'lead';
	protected $dates = ['deleted_at','created_real'];

	const INSTANTLY_SECONDS = 10800; //3 hours
	const MAX_AGE_SECONDS = 604800; //1 week

	/*const INSTANTLY_SECONDS = 8179774; //randomo value
	const MAX_AGE_SECONDS = 38362383; //random value*/
	

	static function toList($dateIni = '',$dateFin = ''){

		$tmp = self::whereRaw('date(created_real) > ?',array('2019-02-10'))
		->select(DB::raw("lead.*,(select count(distinct c.id) from client_contract_lead as c where c.lead_id = lead.id) as times_sold"))
		->orderBy('created_real','desc');

		if(!empty($dateIni)) $tmp->whereRaw('date(created_real) >= ?',array($dateIni));
		if(!empty($dateFin)) $tmp->whereRaw('date(created_real) <= ?',array($dateFin));

		//return $tmp->paginate(20);
	}

	static function lastExtId(){
		return self::withTrashed()->orderBy('id_ext','desc')->value('id_ext');
	}

	static function exitsExt($id){
		return self::withTrashed()->where('id_ext',$id)->count() > 0;
	}

	function responses(){
		return $this->hasMany('\App\Models\Leads\LeadResponse','lead_id','id');
	}

	static function toSell($instant = true){
		$subHaving = $instant ? ' and power <= ?  and power <= ?' : ' and power > ?  and power <= ?';

		$tmp =  DB::connection('mysql')->select("select l.*, count(c.id) as times_sold, TIMESTAMPDIFF(SECOND,created_real,CURRENT_TIMESTAMP)  as power 
		from lead l left join client_contract_lead c on c.lead_id = l.id
		where length(l.phone) > 5 and l.country = 'ES' and l.deleted_at is null group by l.id
		having times_sold < 1 $subHaving
		order by power",array(self::INSTANTLY_SECONDS,self::MAX_AGE_SECONDS));

		return $tmp;

	}

	static function invalidateFromIp($ip,$phone){
		return self::whereRaw('ip = ? and phone = ? and TIMESTAMPDIFF(HOUR,created_real,CURRENT_TIMESTAMP) <= 24',array($ip,$phone))->count() > 0;
	}

	static function invalidateByMaster($id){
		self::whereRaw('id = ? and invalid_master_client = 0',array($id))
		->update(array('invalid_master_client' => 1));
	}

}