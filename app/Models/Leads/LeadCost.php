<?php
namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;

class LeadCost extends Model{
	protected $table = 'lead_cost';


	static function total(){
		return self::count();
	}

	static function assocList(){
		$tmp = self::orderBy('number','asc')->get();
		$res = array();

		foreach($tmp as $t){
			$res[$t->number] = (object)array('i' => $t->instantly, 'd' => $t->deferred, 'v' => $t->verified);
		}

		unset($tmp);

		return $res;
	}
}

