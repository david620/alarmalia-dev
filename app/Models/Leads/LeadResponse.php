<?php
namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;

class LeadResponse extends Model{
	
	protected $connection = 'mysql';
	protected $table = 'lead_response';

}