<?php
namespace App\Modules\Leads\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ClientConfig;
use Config;

class SourceLead extends Model{
	protected $table = 'gf_entry';

	static function configureConnection(){

		$db = ClientConfig::getValue('wp_db',1);
		$username = ClientConfig::getValue('wp_username',1);
		$pass = ClientConfig::getValue('wp_password',1);

		Config::set('database.connections.source', array(
		    'driver'    => 'mysql',
		    'host'      => 'localhost',
		    'database'  => $db,
		    'username'  => $username,
		    'password'  => $pass,
		    'charset'   => 'utf8',
		    'collation' => 'utf8_general_ci',
		    'prefix'    => '0EFueN94_',
		    'options'   => [
		        \PDO::ATTR_EMULATE_PREPARES => true
		    ]
		));
	}

	static function toList($fromId = null){
		self::configureConnection();


		$tmp = self::orderBy('id')
		->whereRaw("status = 'Active' and source_url like ?",array('https://alarmalia.com/resultados/?dbl=%'))
		->select('id','form_id','date_created','source_url','ip');

		
		if($fromId > 0) $tmp->whereRaw('id > ?',array($fromId));

		return $tmp->get();

	}





}
	