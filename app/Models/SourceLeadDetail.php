<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SourceLeadDetail extends Model{
	protected $connection = 'source';
	protected $table = 'gf_entry_meta';

	const FIELD_CP = 3;
	const FIELD_TEL = 2;
	const FIELD_QUIZ = 1;


	static function data($id){
		$tmp = self::where('entry_id',$id)->get();

		$res = array();
		foreach($tmp as $t){
			switch($t->meta_key){
				case self::FIELD_CP:
					$res['cp'] = $t->meta_value;
					break;

				case self::FIELD_TEL:
					$res['tel'] = $t->meta_value;
					break;

				case self::FIELD_QUIZ:
					$res['quiz'] = $t->meta_value;
					break;
			}
		}

		unset($tmp);

		return $res;
	}
}