<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead', function (Blueprint $table) {
            $table->increments('id');
            $table->string('postal_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('origin')->nullable();
            $table->tinyInteger('validated')->nullable();
            $table->dateTime('validated_at')->nullable();
            $table->timestamps();
            $table->dateTime('agreed_at')->nullable();
            $table->string('agreed_with')->nullable();
            $table->softDeletes();
            $table->dateTime('created_real')->nullable();
            $table->unsignedInteger('id_ext')->nullable();
            $table->string('ip')->nullable();
            $table->string('country')->nullable();
            $table->unsignedInteger('percent_invalid')->nullable();
            $table->tinyInteger('invalid_master_client')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead');
    }
}
