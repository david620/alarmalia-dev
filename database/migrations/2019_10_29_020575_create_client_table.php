<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20)->nullable()->index();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
            $table->string('cifnif', 20)->nullable();
            $table->unsignedInteger('company')->nullable();
            $table->tinyInteger('master_invalid')->default(0);
//NUEVOS CAMPOS//
            $table->text('description')->nullable();
            $table->string('link_image')->nullable();
            $table->string('phone')->nullable();
            $table->string('url_web')->nullable();
            $table->text('nivel_seguridad')->nullable();
            $table->text('tecnologia')->nullable();
            $table->text('diferencia')->nullable();
            $table->text('precio')->nullable();
            $table->text('contratar')->nullable();
            $table->text('instalacion')->nullable();
            $table->text('direccion')->nullable();
            $table->text('permanencia')->nullable();
            $table->text('baja')->nullable();
            $table->text('facturacion')->nullable();
            $table->integer('estrellas')->default(1);
            $table->double('precio_desde')->nullable();
            $table->double('precio_hasta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
