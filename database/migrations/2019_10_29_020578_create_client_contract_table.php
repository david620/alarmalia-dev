<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_contract', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_code', 20)->nullable();
            $table->foreign('client_code')->references('code')->on('client')->onUpdate('cascade')->onDelete('cascade');  
            $table->date('due')->nullable();
            $table->float('budget_cash')->nullable()->unsigned();
            $table->unsignedInteger('budget_leads')->nullable();
            $table->string('budget_type')->default('cash');
            $table->float('lead_cost')->nullable()->unsigned();
            $table->tinyInteger('transference_type')->default(0);
            $table->tinyInteger('payment_type')->default(0);
            $table->string('email')->nullable();
            $table->integer('discount')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->string('postals', 1024)->nullable();
            $table->unsignedInteger('priority')->default(0);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_contract');
    }
}
