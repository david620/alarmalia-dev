<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientContractLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_contract_lead', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lead_id')->nullable();
            $table->foreign('lead_id')->references('id')->on('lead')->onUpdate('cascade')->onDelete('cascade');  
            $table->unsignedInteger('client_contract_id')->nullable();
            $table->foreign('client_contract_id')->references('id')->on('client_contract');  
            $table->tinyInteger('transference_type')->default(0);
            $table->float('cost')->nullable()->unsigned();
            $table->float('discount')->default(0);
            $table->timestamps();
            $table->tinyInteger('sent')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->date('at_erp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_contract_lead');
    }
}
