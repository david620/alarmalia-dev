<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGogleCampTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_camp', function (Blueprint $table) {
            $table->increments('Idmonth');
            $table->integer('Year')->nullable();
            $table->integer('Month')->nullable();
            $table->integer('Inversion')->nullable();
            $table->integer('clicks')->nullable();
            $table->bigInteger('impresiones')->nullable();
            $table->bigInteger("Funnel Lead")->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_camp');
    }
}
