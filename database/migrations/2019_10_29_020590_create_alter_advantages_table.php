<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlterAdvantagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advantages', function (Blueprint $table) {


            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('client')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * 

     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advantages', function (Blueprint $table) {
            //
            $table->dropForeign('advantages_company_id_foreign');
        });
    }
}
