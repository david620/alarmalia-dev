<?php

use Illuminate\Database\Seeder;
use App\Models\Alarma;

class AlarmaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Alarma::create([
            'nombre_alarma' => 'Cualquier cosa',
            'estado' => '1'
        ]); 
        Alarma::create([
            'nombre_alarma' => 'Cualquier cosa 2 ',
            'estado' => '1'
        ]); 
        Alarma::create([
            'nombre_alarma' => 'Cualquier cosa 3 ',
            'estado' => '1'
        ]); 
        Alarma::create([
            'nombre_alarma' => 'Cualquier cosa 4 ',
            'estado' => '1'
        ]); 
        Alarma::create([
            'nombre_alarma' => 'Cualquier cosa 5 ',
            'estado' => '1'
        ]); 
    }
}
