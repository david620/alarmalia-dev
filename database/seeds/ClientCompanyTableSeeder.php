<?php

use Illuminate\Database\Seeder;

class ClientCompanyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('client_company')->delete();
        
        \DB::table('client_company')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Tyco',
                'created_at' => '2019-03-25 14:16:02',
                'updated_at' => '2019-03-25 14:16:02',
            ),
        ));
        
        
    }
}