<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(HomePageSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(AdvantageSeeder::class);
        $this->call(FaqCategorySeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(FormTableSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(AnswerSeeder::class);
        $this->call(GlosarySeeder::class);
        $this->call(NewsletterSeeder::class);
        $this->call(LeadTableSeeder::class);
        $this->call(BlacklistMobileTableSeeder::class);
        $this->call(ClientCompanyTableSeeder::class);
        $this->call(ClientContractTableSeeder::class);
        $this->call(GoogleCampTableSeeder::class);
        $this->call(LeadCostTableSeeder::class);
        $this->call(LeadResponseTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(ClientContractLeadTableSeeder::class);
        $this->call(LeadInvalidTableSeeder::class);
    }
}
