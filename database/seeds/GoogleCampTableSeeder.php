<?php

use Illuminate\Database\Seeder;

class GoogleCampTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('google_camp')->delete();
        
        \DB::table('google_camp')->insert(array (
            0 => 
            array (
                'Idmonth' => 1,
                'Year' => 2019,
                'Month' => 2,
                'Inversion' => 649,
                'clicks' => 582,
                'impresiones' => 17591,
                'Funnel Lead' => 18,
            ),
            1 => 
            array (
                'Idmonth' => 2,
                'Year' => 2019,
                'Month' => 3,
                'Inversion' => 2180,
                'clicks' => 1720,
                'impresiones' => 60400,
                'Funnel Lead' => 136,
            ),
            2 => 
            array (
                'Idmonth' => 3,
                'Year' => 2019,
                'Month' => 4,
                'Inversion' => 4935,
                'clicks' => 3817,
                'impresiones' => 158462,
                'Funnel Lead' => 359,
            ),
            3 => 
            array (
                'Idmonth' => 4,
                'Year' => 2019,
                'Month' => 5,
                'Inversion' => 1826,
                'clicks' => 1388,
                'impresiones' => 45921,
                'Funnel Lead' => 194,
            ),
            4 => 
            array (
                'Idmonth' => 5,
                'Year' => 2019,
                'Month' => 6,
                'Inversion' => 6112,
                'clicks' => 3758,
                'impresiones' => 100568,
                'Funnel Lead' => 504,
            ),
            5 => 
            array (
                'Idmonth' => 6,
                'Year' => 2019,
                'Month' => 7,
                'Inversion' => 2360,
                'clicks' => 2963,
                'impresiones' => 102047,
                'Funnel Lead' => 570,
            ),
            6 => 
            array (
                'Idmonth' => 7,
                'Year' => 2019,
                'Month' => 8,
                'Inversion' => 2150,
                'clicks' => 3420,
                'impresiones' => 108000,
                'Funnel Lead' => 593,
            ),
            7 => 
            array (
                'Idmonth' => 8,
                'Year' => 2019,
                'Month' => 9,
                'Inversion' => 3050,
                'clicks' => 3650,
                'impresiones' => 107000,
                'Funnel Lead' => 650,
            ),
            8 => 
            array (
                'Idmonth' => 9,
                'Year' => 2019,
                'Month' => 10,
                'Inversion' => 5012,
                'clicks' => 5112,
                'impresiones' => 144000,
                'Funnel Lead' => 794,
            ),
            9 => 
            array (
                'Idmonth' => 10,
                'Year' => 2019,
                'Month' => 11,
                'Inversion' => 2353,
                'clicks' => 4195,
                'impresiones' => 104000,
                'Funnel Lead' => 872,
            ),
            10 => 
            array (
                'Idmonth' => 11,
                'Year' => 2019,
                'Month' => 12,
                'Inversion' => 2348,
                'clicks' => 3975,
                'impresiones' => 96200,
                'Funnel Lead' => 819,
            ),
        ));
        
        
    }
}