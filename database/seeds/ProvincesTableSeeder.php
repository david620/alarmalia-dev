<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'code' => 15,
                'created_at' => NULL,
                'id' => 1,
                'name' => 'A CORUÑA',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'code' => 1,
                'created_at' => NULL,
                'id' => 2,
                'name' => 'ÁLAVA',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'code' => 2,
                'created_at' => NULL,
                'id' => 3,
                'name' => 'ALBACETE',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'code' => 3,
                'created_at' => NULL,
                'id' => 4,
                'name' => 'ALICANTE',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'code' => 4,
                'created_at' => NULL,
                'id' => 5,
                'name' => 'ALMERÍA',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'code' => 33,
                'created_at' => NULL,
                'id' => 7,
                'name' => 'ASTURIAS',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'code' => 5,
                'created_at' => NULL,
                'id' => 8,
                'name' => 'ÁVILA',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'code' => 6,
                'created_at' => NULL,
                'id' => 9,
                'name' => 'BADAJOZ',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'code' => 8,
                'created_at' => NULL,
                'id' => 10,
                'name' => 'BARCELONA',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'code' => 9,
                'created_at' => NULL,
                'id' => 11,
                'name' => 'BURGOS',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'code' => 10,
                'created_at' => NULL,
                'id' => 12,
                'name' => 'CÁCERES',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'code' => 11,
                'created_at' => NULL,
                'id' => 13,
                'name' => 'CÁDIZ',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'code' => 39,
                'created_at' => NULL,
                'id' => 14,
                'name' => 'CANTABRIA',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'code' => 12,
                'created_at' => NULL,
                'id' => 15,
                'name' => 'CASTELLÓN',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'code' => 51,
                'created_at' => NULL,
                'id' => 16,
                'name' => 'CEUTA',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'code' => 13,
                'created_at' => NULL,
                'id' => 17,
                'name' => 'CIUDAD REAL',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'code' => 14,
                'created_at' => NULL,
                'id' => 18,
                'name' => 'CÓRDOBA',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'code' => 16,
                'created_at' => NULL,
                'id' => 19,
                'name' => 'CUENCA',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'code' => 17,
                'created_at' => NULL,
                'id' => 20,
                'name' => 'GIRONA',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'code' => 18,
                'created_at' => NULL,
                'id' => 21,
                'name' => 'GRANADA',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'code' => 19,
                'created_at' => NULL,
                'id' => 22,
                'name' => 'GUADALAJARA',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'code' => 20,
                'created_at' => NULL,
                'id' => 23,
                'name' => 'GUIPUZCOA',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'code' => 21,
                'created_at' => NULL,
                'id' => 24,
                'name' => 'HUELVA',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'code' => 22,
                'created_at' => NULL,
                'id' => 25,
                'name' => 'HUESCA',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'code' => 7,
                'created_at' => NULL,
                'id' => 26,
                'name' => 'BALEARES',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'code' => 23,
                'created_at' => NULL,
                'id' => 27,
                'name' => 'JAÉN',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'code' => 26,
                'created_at' => NULL,
                'id' => 28,
                'name' => 'LA RIOJA',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'code' => 35,
                'created_at' => NULL,
                'id' => 29,
                'name' => 'LAS PALMAS',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'code' => 24,
                'created_at' => NULL,
                'id' => 30,
                'name' => 'LEÓN',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'code' => 25,
                'created_at' => NULL,
                'id' => 31,
                'name' => 'LLEIDA',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'code' => 27,
                'created_at' => NULL,
                'id' => 32,
                'name' => 'LUGO',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'code' => 28,
                'created_at' => NULL,
                'id' => 33,
                'name' => 'MADRID',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'code' => 29,
                'created_at' => NULL,
                'id' => 34,
                'name' => 'MÁLAGA',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'code' => 52,
                'created_at' => NULL,
                'id' => 35,
                'name' => 'MELILLA',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'code' => 30,
                'created_at' => NULL,
                'id' => 36,
                'name' => 'MURCIA',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'code' => 31,
                'created_at' => NULL,
                'id' => 37,
                'name' => 'NAVARRA',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'code' => 32,
                'created_at' => NULL,
                'id' => 38,
                'name' => 'OURENSE',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'code' => 34,
                'created_at' => NULL,
                'id' => 39,
                'name' => 'PALENCIA',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'code' => 36,
                'created_at' => NULL,
                'id' => 40,
                'name' => 'PONTEVEDRA',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'code' => 37,
                'created_at' => NULL,
                'id' => 41,
                'name' => 'SALAMANCA',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'code' => 40,
                'created_at' => NULL,
                'id' => 42,
                'name' => 'SEGOVIA',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'code' => 41,
                'created_at' => NULL,
                'id' => 43,
                'name' => 'SEVILLA',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'code' => 42,
                'created_at' => NULL,
                'id' => 44,
                'name' => 'SORIA',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'code' => 38,
                'created_at' => NULL,
                'id' => 45,
                'name' => 'TENERIFE',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'code' => 43,
                'created_at' => NULL,
                'id' => 46,
                'name' => 'TARRAGONA',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'code' => 44,
                'created_at' => NULL,
                'id' => 47,
                'name' => 'TERUEL',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'code' => 45,
                'created_at' => NULL,
                'id' => 48,
                'name' => 'TOLEDO',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'code' => 46,
                'created_at' => NULL,
                'id' => 49,
                'name' => 'VALENCIA',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'code' => 47,
                'created_at' => NULL,
                'id' => 50,
                'name' => 'VALLADOLID',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'code' => 48,
                'created_at' => NULL,
                'id' => 51,
                'name' => 'VIZCAYA',
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'code' => 49,
                'created_at' => NULL,
                'id' => 52,
                'name' => 'ZAMORA',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'code' => 50,
                'created_at' => NULL,
                'id' => 53,
                'name' => 'ZARAGOZA',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}