<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'email' => 'administrador@alarmalia.com',
            'password'     => bcrypt('123456')
        ]);
        User::create([
            'name' => 'Editor empresa',
            'email' => 'editor_empresa@alarmalia.com',
            'password'     => bcrypt('123456')
        ]);
        User::create([
            'name' => 'Editor blog',
            'email' => 'editor_blog@alarmalia.com',
            'password'     => bcrypt('123456')
        ]);
        User::create([
            'name' => 'Administrador de leads',
            'email' => 'administrador_leads@alarmalia.com',
            'password'     => bcrypt('123456')
        ]);
    }
}
