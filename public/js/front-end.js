$(document).ready(function(){
    if ($(window).scrollTop() == 0) {
        $('.transparent').removeClass('scroll');
    }
    else{
        $('.transparent').addClass('scroll');
    }

    $('#button-menu').click(function(){
        if(!$('header').hasClass('opened')){
            $('header').addClass('opened')
        }
        else
            $('header').removeClass('opened')
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() == 0) {
            $('.transparent').removeClass('scroll');
        }
        else{
            $('.transparent').addClass('scroll');
        }
    });

    $(document).on("click",'.modallink',function(e){
        e.stopPropagation();
        $('#largeModalTitle').html('');
        $('#largeModalText').html('');
        $('#largeModalTitle.fade').removeClass('show');
        $('#largeModalText.fade').removeClass('show');
        $.getJSON( "/legal-text/" + $(this).attr("text"), function( data ) {
            $('#largeModalTitle').html(data.title);
            $('#largeModalText').html(data.text);
            $('#largeModalTitle.fade').addClass('show');
            $('#largeModalText.fade').addClass('show');
        });
    });
    $(document).on("click",'.error',function(e){
        $(this).removeClass('error')
    });
    $(document).on("change",'.error',function(e){
        $(this).removeClass('error')
    });
});
