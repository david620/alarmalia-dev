@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Clientes
@endsection
@section('contentheader_title') 

@stop


@section('main-content')

<div class="box">
    <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-6">
      			<h3 class="box-title">Lista de Clientes</h3>
      		</div>
      		<div class="col-xs-6 text-right">
      			<h3 class="box-title">
      				
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
    	<form id="filter_c" name="filter_c" class="img-rounded " method="GET" style="background-color: #efefef; padding: 20px">
          
        <div class="row">
          
          <div class="col-sm-3">
            <label>Nombre Clientes</label>
            <input class="form-control" name="name" type="text" value="{{ app('request')->input('name') }}">
              
          </div>
          
          <div class="col-sm-7">
            <label>&nbsp;</label><br/>
            <button class="btn btn-primary"><span class="fa fa-search"></span> Buscar</button>
          </div>
        </div></form>
        <br/>
      <table id="example1" class="table table-bordered table-striped">
          	<thead>
              <tr class="header">
	              <th>Código</th>
	              <th>Nombre</th>
	              <th>Correo</th>
	              <th>Sincronizado</th>	              
	              <th></th>
	          	</tr>
          	</thead>
          	<tbody>
          		@foreach($clients as $c)
          			<tr>
          				<td>{{ $c->code }}</td>
          				<td>
                    @if(Pipe::validateRoute('Clients_contracts'))
                      <a href="{{ URL::route('Clients_contracts') }}?client_code={{ $c->code }}">{{ $c->name }}</a>
                    @else
                    {{ $c->name }}
                    @endif
                  </td>
          				<td>{{ $c->email }}</td>
                  		<td>{{ $c->created_at }}</td>
          				<td>
                     @if(Pipe::validateRoute('Clients_delete'))
                                  {!!link_to_route('Clients_delete', $title =
                                    'Eliminar',
                                    $parameters = ['id' => $c->id],
                                    $attributes = ['class'=>'btn btn-danger','style'=>'width:100%', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Cliente?')"]);!!}
                    @endif

                  </td>
          			</tr>
          		@endforeach
          	</tbody>
        </table>
        <div class="text-center">
	    </div>
    </div>
    <div class="box-footer">
    </div>
</div>
@endsection

<form id="deleteForm" method="post">
  {{ csrf_field() }}
</form>
