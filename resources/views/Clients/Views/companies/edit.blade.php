@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Compañias
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

<div class="box">
   <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-10">
      			<h3 class="box-title">Editar</h3>
      		</div>
      		<div class="col-xs-2 text-right">
      			<h3 class="box-title">
      				
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
    	<form method="post">
    	<p>
	    	<div class="row">
	    		<div class="col-sm-12">
	    			<label>Nombre</label>
	    			<input class="form-control" type="text" name="name" value="{{ old('name',$entity->name) }}">
		       		@if ($errors->has('name'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('name') }}</strong>
	                  </span>
	              	@endif
	    		</div>

	    	</div>
	    </p>
	    @if($entity->id)
	    <p>
	    	<select class="form-control" size="{{ count($clients) }}" multiple name="clients[]">
	    		@foreach($clients as $c)
	    			<option value="{{ $c->code }}" @if($c->company == $entity->id) selected="selected" @endif>{{ $c->name }}</option>
	    		@endforeach
	    	</select>

	    </p>
	    @endif
	    <br/>
	    <p>
	       		<button class="btn btn-danger" type="submit"><span class="fa fa-save"></span> Guardar</button>
	    </p>
	    {{ csrf_field() }}
		</form>
	   </div>
	</div>
@endsection
