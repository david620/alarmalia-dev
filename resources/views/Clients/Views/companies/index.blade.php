@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Compañias
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')
<div class="box">
    <div class="box-header with-border">
   		<div class="row">
   			<div class="col-sm-9">
      			<h3 class="box-title">Empresas</h3>
      		</div>
      		<div class="col-sm-3 text-right">
      			<h3 class="box-title">
                @if(Pipe::validateRoute('Clients_companies_edit'))
      				  <a href="{{ URL::route('Clients_companies_edit',array(0)) }}"><button type="button" class="btn btn-warning"><span class="fa fa-plus"></span> Nuevo</button></a>
                @endif
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
    	
    	<table class="table">
          	<thead>
          		<tr>
                <th>Nombre</th>
  	            <th>Clientes</th>
  	            <th>Creado</th>
	          	</tr>
          	</thead>
          	<tbody>
          		@foreach($companies as $c)
          			<tr @if($c->isRecentInList()) class="success" @endif>
          				<td>
		                    @if(Pipe::validateRoute('Clients_companies_edit'))
		                    <a href="{{ URL::route('Clients_companies_edit',array($c->id)) }}">{{ $c->name }}</a>
		                    @else
		                    {{ $c->name }}
		                    @endif
		                </td>
          				  <td>{{ $c->clients }}</td>
                  
                  	<td>{{ $c->created_at }}</td>
          				
          			</tr>
          		@endforeach
          	</tbody>
        </table>
        <div class="text-center">
          {{ $companies->appends(app('request')->all())->links() }}
	    </div>
    </div>
    <div class="box-footer">
        <i>Mostrando: {{ $companies->firstItem()}} al {{$companies->lastItem() }} de {{ $companies->total()}} Elementos </i>
    </div>
</div>
</section>
@endsection

<form id="deleteForm" method="post">
  {{ csrf_field() }}
</form>
