@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Contratos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="box">
   <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-10">
      			<h3 class="box-title">Contratos</h3>
      		</div>
      		<div class="col-xs-2 text-right">
      			<h3 class="box-title">
      				
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
    	<form method="post">
	    {{ csrf_field() }}
    	<p>
	    	<div class="row">
	    		<div class="col-sm-4">
	    			<label>Cliente</label>
	    			<select name="client_code" class="form-control input-sm" @if(!$new) disabled @endif>
	    				<option value="">Seleccione una Opción</option>
	    				@foreach($clients as $c)
	    					<option value="{{ $c->code }}" @if(old('client_code',$entity->client_code) == $c->code) selected="selected" @endif>{{ $c->name }}</option>
	    				@endforeach
	    			</select>
		       		@if ($errors->has('client_code'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('client_code') }}</strong>
	                  </span>
	              	@endif
	    		</div>

		    	
		    	<div class="col-sm-3">
	    			<label>Caducidad</label>
					<input type="date" class="form-control datePicker" value="{{ old('due',($entity->due  ? $entity->due : '')) }}"  maxlength="10" name="due" @if(!$new) disabled @endif>
	    			@if ($errors->has('due'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('due') }}</strong>
	                  </span>
	              	@endif
	    		</div>

	    		

	    		<div class="col-sm-3">
	    			<label>Tipo de Transferencia</label>
	    			<select class="form-control input-sm" name="transference_type" id="transference_type" @if($new) onchange="setLeadCost(this)" @endif>
	    				<option value="0" data-cost=" ">Instantáneo</option>
	    				<option value="1" data-cost=" " @if(old('transference_type',$entity->transference_type) == 1) selected="selected" @endif>Diferido</option>
	    				<option value="2" data-cost=" " @if(old('transference_type',$entity->transference_type) == 2) selected="selected" @endif>Verificado</option>
	    			</select>
	    			@if ($errors->has('transference_type'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('transference_type') }}</strong>
	                  </span>
	              	@endif
	    		</div>


	    		<div class="col-sm-2">
	    			<label>Descuento</label>
	    			<select class="form-control input-sm" name="discount">
	    				@for($i = 0; $i <= 100; $i++)
	    					<option value="{{ $i }}" @if(old('discount',$entity->discount) == $i) selected="selected" @endif>{{ $i }}%</option>
	    				@endfor
	    			</select>
	    		</div>

	    		
	    	</div>
	    </p>
	    <p>
	    	<div class="row">
	    		<div class="col-sm-2">
	    			<label>Tipo de Presupuesto</label>
	    			<select class="form-control input-sm" name="budget_type" onchange="changeBudgetType(this)">
	    				<option value="cash" @if(old('budget_type',$entity->budget_type) == 'cash')) selected="selected" @endif>Euros</option>
	    				<option value="leads" @if(old('budget_type',$entity->budget_type) == 'leads')) selected="selected" @endif>Leads</option>
	    			</select>
	    		</div>

	    		<div class="col-sm-2">
	    			<label>Contratos <small>(Euros)</small></label>
	    			<input class="form-control input-sm" name="budget_cash" id="budget_cash" @if(old('budget_type',$entity->budget_type) == 'leads')) disabled @endif id type="text" value="{{ old('budget_cash',$entity->budget_cash) }}">
		       		@if ($errors->has('budget_cash'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('budget_cash') }}</strong>
	                  </span>
	              	@endif
	    		</div>

	    		<div class="col-sm-2">
	    			<label>Presupuesto <small>(Leads)</small></label>
	    			<input class="form-control input-sm" @if(old('budget_type',$entity->budget_type) != 'leads')) disabled @endif name="budget_leads" id="budget_leads" type="number" value="{{ old('budget_leads',$entity->budget_leads) }}" maxlength="100">
		       		@if ($errors->has('budget_leads'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('budget_leads') }}</strong>
	                  </span>
	              	@endif
	    		</div>

	    		<div class="col-sm-4">
	    			<label>Email</label>
	    			<input class="form-control input-sm" name="email" value="{{ old('email',$entity->email) }}">
	    			@if ($errors->has('email'))
	                  <span class="help-block">
	                      <strong>{{ $errors->first('email') }}</strong>
	                  </span>
	              	@endif

	    		</div>

	    		<div class="col-sm-2">
	    			<label>Prioridad</label>
	    			<select class="form-control input-sm" name="priority">
	    				@for($i = 0; $i <= 100; $i++)
	    					<option value="{{ $i }}" @if(old('priority',$entity->priority) == $i) selected="selected" @endif>{{ $i }}</option>
	    				@endfor
	    			</select>
	    		</div>
	    		

	    	</div>
	    </p>

<!--
	    <p>
	    	<div class="row">

	    		<div class="col-sm-12">
	    			<?php $resp = old('respuestas',explode(',',$entity->questions_ids)); if(!is_array($resp)) $resp = array(); ?>
	    			<label>Preguntas y Respuestas</label>

	    			@if(!$new)
	    			<select class="form-control multiselect" name="respuestas[]" multiple>

			            @foreach ($respuestas_form as $key => $value) 
				    					<option value="^{{ $key }}^" @if(in_array('^'.$key.'^',$resp)) selected="selected" @endif>{{ $value }}</option>
			            @endforeach

	    			</select>
	    			@endif
	    			@if($new)
	    			<select class="form-control multiselect" name="respuestas[]" multiple>

			            @foreach ($respuestas_form as $key => $value) 
				    					<option value="^{{ $key }}^" selected="selected">{{ $value }}</option>
			            @endforeach

	    			</select>
	    			@endif
	    		</div>
			</div>
	    </p>
-->
	    <p>
	    	<div class="row">
	    		<div class="col-sm-6">
	    			<?php $postals = old('postalsC',explode(',',$entity->postals)); if(!is_array($postals)) $postals = array(); ?>
	    			<label>Provincias</label>
	    			@if(!$new)
	    			<select class="form-control multiselect" name="postalsC[]" multiple>
	    				@foreach($provinces as $prov)
	    					<option value="^{{ $prov->code }}^" @if(in_array('^'.$prov->code.'^',$postals)) selected="selected" @endif>{{ $prov->name }}</option>
	    				@endforeach
	    			</select>
	    			@endif
	    			@if($new)
	    				@if($postals)
			    			<select class="form-control multiselect" name="postalsC[]" multiple>
			    				@foreach($provinces as $prov)
			    					<option value="^{{ $prov->code }}^" @if(in_array('^'.$prov->code.'^',$postals)) selected="selected" @endif>{{ $prov->name }}</option>
			    				@endforeach
			    			</select>
						@else		    			
			    			<select class="form-control multiselect" name="postalsC[]" multiple>
			    				@foreach($provinces as $prov)
			    					<option value="^{{ $prov->code }}^" selected="selected" >{{ $prov->name }}</option>
			    				@endforeach
			    			</select>
			    		@endif
	    			@endif
	    		</div>


	    		<div class="col-sm-6">
		    		@if($entity->type_lead == null || $entity->type_lead == 0)
			    		<label>Asociar Leads Cortos: (Si/No) &nbsp;&nbsp;</label>{{ Form::checkbox('type_lead', 1, 0) }}
		    		@else
			    		<label>Asociar Leads Cortos: (Si/No) &nbsp;&nbsp;</label>{{ Form::checkbox('type_lead', 1, 1) }}
		    		@endif
				    <p align="justify"></p>
<!--
	    		@if($question_m2)
		    		@if($entity->m2_size == null)
		    			<label>Metros Cuadrados (Viviendas de más de: )&nbsp;&nbsp;</label>{{ Form::checkbox('m2_check', TRUE, false, ['id' => 'm2_check', 'onchange' => 'changeM2(this)']) }}
		    		@else
	    			<label>Metros Cuadrados (Viviendas de más de: )&nbsp;&nbsp;</label>{{ Form::checkbox('m2_check', TRUE, TRUE, ['id' => 'm2_check', 'onchange' => 'changeM2(this)']) }}
		    		@endif
	    			<input type="number" name="m2_size" class="form-control input-sm" id="m2_box" placeholder="Metros Cuadrados"  id type="text" value="{{ old('m2_size',$entity->m2_size) }}" >
	    			<br>
	    		@endif
-->
	    		</div>
	    	</div>
	    </p>
	    











	    
	    <br/>
	    <p>
	       		<button class="btn btn-danger" type="submit"><span class="fa fa-save"></span> Guardar</button>
	    </p>
	    {{ csrf_field() }}
		</form>

		@if($entity->id)
			<?php $counters = $entity->counters(); ?>
			<div class="text-center">
				<h1>{{ intval($counters->total) }} Leads,  {{ floatval($counters->cost) }} €</h1>
			</div>
		@endif
    </div>
    @if(!$new)
    <div class="box-footer">
         <div class="row"><div class="col-sm-6"> <i>Creado {{ $entity->created_at }}</i></div><div class="col-sm-6 text-right"><i> Última Actualizacíón {{ $entity->updated_at }}</i></div></div>
    </div>
    @endif
</div>

@if($entity->id)
<div class="box box-success">
   <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-9">
      			<h3 class="box-title">Leads Asignados a Contrato</h3>
      		</div>
      		<div class="col-xs-3 text-right">
      			<span class="fa fa-file-excel-o fa-2x" style="cursor: pointer" onclick="document.printLeadsForm.submit();"></span>
      		</div>
      	</div>
    </div>
    <div class="box-body">
    	@include('Clients.Views.contracts.edit_leads_table')
    </div>
</div>
@endif


@endsection

<form name="printLeadsForm" method="post" target="_blank">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="postAction" value="printLeads">
</form>

<script type="text/javascript">
function changeBudgetType(el){
	if($(el).val() == 'cash'){
		$('#budget_cash').prop('disabled',false);
		$('#budget_leads').prop('disabled',true);
	}
	else{
		$('#budget_cash').prop('disabled',true);
		$('#budget_leads').prop('disabled',false);
	}
}










function setLeadCost(el){
	$('#lead_cost').val($(el).find('option:selected').data('cost'));
}


function changeM2(el){

	if($(el).is(':checked')){
        $( "#m2_box" ).removeClass( "hidden" );
	}
	else{
        $( "#m2_box" ).addClass( "hidden" );
	}
}


</script>
