<table class="table">
	<thead>
		<tr>
			<th>Activo</th>
			<th>CP</th>
			<th>Teléfono</th>
			<th>País</th>
			<th>Creado Real</th>
			<th>Tipo de Transferencia</th>
			<th>Coste Lead</th>
			<th>Email Enviado</th>
			<th>Creado</th>
			<th class="text-center">Veces Vendido</th>
			<th class="text-center">Veces Inválido</th>
			<th class="text-center">Válido</th>
			<th class="text-center">Clicks Inválido Cliente</th>
			<th class="text-center">Inválido por Cliente Master</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($entity->assignedLeads() as $l)
			<tr>
				<td>{{ $l->active ? 'SI' : 'NO' }}</td>
				<td>{{ $l->postal_code }}</td>
				<td>{{ $l->phone }}</td>
				<td>{{ $l->country }}</td>
				<td>{{ $l->created_real }}</td>
				<td>{{ $l->transference_type }}</td>
				<td>{{ $l->cost }}</td>
				<td>{{ $l->sent ? 'SI' : 'NO' }}</td>
				<td>{{ $l->created_at }}</td>
				<td class="text-center">{{ $l->times_sold }}</td>
				<td class="text-center">{{ intval($l->times_invalid) }}</td>
				<td class="text-center">{{ $l->is_valid ? 'SI' : 'NO' }}</td>
				<td class="text-center">{{ intval($l->invalid_clicks_client) }}</td>
				<td class="text-center">{{ intval($l->invalid_master_client) }}</td>				

			</tr>
		@endforeach
	</tbody>
</table>