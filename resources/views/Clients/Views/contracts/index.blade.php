@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Contratos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop








@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
                    <form id="filter_c" name="filter_c" class="img-rounded " method="GET" style="background-color: #efefef; padding: 20px">                        
                      <div class="row">                        
                        <div class="col-sm-6">
                          <label>Nombre: </label>
                          <input class="form-control" name="name_filter" type="text" value="{{ app('request')->input('name_filter') }}">
                        </div>
                        
                        <div class="col-sm-4">
                          <label>Solo Activos: </label><br/>
                          <input type="checkbox" name="onlyActives" value="1" @if(app('request')->input('onlyActives') == 1) checked="checked" @endif>
                        </div>
                        <div class="col-sm-12">
                          <label>&nbsp;</label><br/>
                          <button class="btn btn-primary btn-block"><span class="fa fa-search"></span> Buscar</button>
                          @if(Pipe::validateRoute('Clients_contracts_edit'))
                            <a href="{{ URL::route('Clients_contracts_edit',array(0)) }}"><button type="button" class="btn btn-warning btn-block"><span class="fa fa-plus"></span> Nuevo Contrato</button></a>
                          @endif                          
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>





<div class="box">
      @if ($message = Session::get('success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
      @endif
    <div class="box-header with-border">
      <div class="row">
        <div class="col-xs-6">
            <h3 class="box-title">Listado de Leads</h3>
          </div>
          <div class="col-xs-6 text-right">
            <h3 class="box-title">
              
            </h3>
          </div>
        </div>
    </div>
    <div class="box-body table-responsive-dt">
        <br/>
      <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr class="header">
                <th>Cliente</th>
                <th>Caducidad</th>
                <th>Presupuesto</th>
                <th>Tipo de Transferencia</th>
                <th>Tipo de Presupuesto</th>
                <th>Presupuesto</th>
                <th>Prioridad</th>
                <th>Leads Asignados</th>
                <th>Euros Asignados</th>
                <th>Creado</th>               
                <th>Acciones</th>
              </tr>
            </thead>
                    <tbody>
                      @foreach($contracts as $c)
                        <tr @if($c->isRecentInList()) class="success" @endif>
                            <td>
                              @if(Pipe::validateRoute('Clients_contracts_edit'))
                                <a href="{{ URL::route('Clients_contracts_edit',array($c->id)) }}">{{ $c->client }}</a>
                              @else
                                {{ $c->client }}
                              @endif
                            </td>
                            <td>{{ $c->due }}</td>
                            <td>{{ $c->email }}</td>
                            <td>
                              @if($c->transference_type == 0)
                                Instantáneo
                              @endif
                              @if($c->transference_type == 1)
                                Diferido
                              @endif
                              @if($c->transference_type == 2)
                                Verificado
                              @endif
                            </td>
                            <td>{{ $c->budget_type }}</td>
                            <td>{{ $c->budget_type == 'cash' ? $c->budget_cash : $c->budget_leads }}</td>
                            <td>{{ $c->priority }}</td>
                            <?php $counters = $c->counters(); ?>
                            <td class="text-center">{{ intval($counters->total) }}</td>
                            <td>{{ floatval($counters->cost) }}</td>
                            <td>{{ $c->created_at }}</td>
                            <td>
                              @if(Pipe::validateRoute('Clients_contracts_edit'))
                              {!!link_to_route('Clients_contracts_edit', $title = '&nbsp; &nbsp; Clone',
                              $parameters = ['id' => ('0c'.$c->id)],
                              $attributes = ['class'=>'btn btn bg-primary fa fa-clone', 'onclick'=>"return confirm('¿Seguro que desea Clonar este Contrato?')"]);!!}  


                              @endif

                              @if(Pipe::validateRoute('Clients_contracts_delete'))
                                            {!!link_to_route('Clients_contracts_delete', $title =
                                              'Eliminar',
                                              $parameters = ['id' => $c->id],
                                              $attributes = ['class'=>'btn btn-danger','style'=>'width:100%', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Contrato?')"]);!!}
                              @endif

                            </td>


                        </tr>                      
                      @endforeach
                    </tbody>
        </table>
        <div class="text-center">
      </div>
    </div>
    <div class="box-footer">

    </div>
</div>

</div>
@endsection

<form id="awakeForm" method="post">
  {{ csrf_field() }}
</form>

<form id="deleteForm" method="post">
  {{ csrf_field() }}
</form>

<form id="cloneForm" method="get">

</form>