<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
</head>
<body style="font-family: arial;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">

			<table width="600"><tr><td>
			<img src="{{ asset('img/logo.png') }}" width="200">
			<table class="table" style="width: 100%; border: 1px solid #ccc">
				<thead>
					<tr>
						<th style="color: #999">Nombre y Apellido</th>
						<th style="color: #999">Teléfono</th>
					</tr>
				</thead>
				<tbody>
					<tr>   
						<td align="center"><h3>{{ $contact->name }} {{ $contact->lastname }}</h3></td>
						<td>{{ $contact->phone }}</td>   
						<td>{{ $contact->company_email }}</td>   
					</tr>
				</tbody>
			</table>

				<div style="width: 100%; height: 1px; background-color: #eee"></div>
			<table class="table" style="width: 100%; border: 1px solid #ccc">
					{!! $contact->message !!}
			</table>
		</td>
	</tr>
</table>



</body>
</html>