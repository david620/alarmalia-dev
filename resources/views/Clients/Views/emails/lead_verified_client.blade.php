<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
</head>
<body style="font-family: arial;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">

			<table width="600"><tr><td>
			<img src="{{ asset('img/logo.png') }}" width="200">
			<table class="table" style="width: 100%; border: 1px solid #ccc">
				<thead>
					<tr>
						<th style="color: #999">Código Postal</th>
						<th style="color: #999">Nombres y Pellidos</th>
						<th style="color: #999">Teléfono</th>
						<th style="color: #999">País</th>
						<th style="color: #999">Fecha Creación</th>
					</tr>
				</thead>
				<tbody>
					<tr>   
						@if($lead->postal_code)
				        	<td align="center">
				        		<h3>{{ $lead->postal_code }}</h3>
				        	</td>
			        	@else
				        	<td align="center"><h3></h3>No Aplica</td>
			        	@endif
			        	<td align="center"><h3>{{ $nombres }}</h3></td>
			        	<td align="center"><h3>{{ $lead->phone }}</h3></td>
						<td align="center"><h3>{{ $lead->country }}</h3></td>
						<td align="center"><h3>{{ $lead->created_real}}</h3></td>
					</tr>
				</tbody>
			</table>

			<h3>Respuestas Lead</h3>

			@foreach($lead->responses as $response)
			<p>
			  <span class="list-group-item-heading" style="color: #888">{{ $response->question }}</span>
			  @if($response->response)
			    <br/>{{ $response->response ? $response->response  : trans('app.noResponse')}}
			  @endif
			</p>
			@endforeach

			<div style="width: 100%; height: 1px; background-color: #eee"></div>

			</td></tr></table>
		</td>
	</tr>
</table>
<br/><br/>
<table align="center">
	<tr>
		<td align="center"><a style="background-color: #008CBA; padding: 15px 32px; border: none; color: white; text-align: center; text-decoration: none; display: inline-block; font-size: 16px;" target="_blank" href="{{ URL::route('Client_public_lead_invalid',array($lead->id,$clientCode)) }}" style="color: #f40">Marcar como Lead no válido</a></td>
	</tr>
</table>
</body>
</html>