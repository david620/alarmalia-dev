<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
</head>
<body style="font-family: arial;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">

			<table width="600"><tr><td>
			<img src="{{ asset('img/logo.png') }}" width="200">
			<table class="table" style="width: 100%; border: 1px solid #ccc">
				<thead>
					<tr>
						<th style="color: #999">Título Post</th>
						<th style="color: #999">Usuario Post</th>
						<th style="color: #999">Compañia Post</th>
					</tr>
				</thead>
				<tbody>
					<tr>   
						<td align="center"><h3>{{ $post->title }}</h3></td>
						@if((\App\User::where('id', '=',$post->user_id)->first()->name))
			        	<td align="center"><h3>{{(\App\User::where('id', '=',$post->user_id)->first()->name) }}</h3></td>
			        	@else
						<td></td>   
			        	@endif
						@if((\App\Models\Cliente::where('id', '=', (\App\User::where('id', '=',$post->user_id)->first()->company_id) )->first()))
						<td align="center"><h3>{{(\App\Models\Cliente::where('id', '=', (\App\User::where('id', '=',$post->user_id)->first()->company_id) )->first()) }}</h3></td>
						@else
						<td></td> 
						@endif
					</tr>
				</tbody>
			</table>

				<div style="width: 100%; height: 1px; background-color: #eee"></div>
			<table class="table" style="width: 100%; border: 1px solid #ccc">
					{!! $post->content !!}
			</table>
		</td>
	</tr>
</table>



</body>
</html>