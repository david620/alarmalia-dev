<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 3) )
        @include('layouts.modal_flotante')
    @endif
@endif

    @include('layouts.header')

    <section class="bg-home-1" id="home" >
        <div class="home-bg-casa"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">

                         <div class="col-lg-6">
                    <br>
                            <div class="home-desc-center margin-home2">
                                <h1 class="tittle-img alingtittle2" >Alarmas para casa</h1>                                
                                                
                                <div class="mt-9">
                                    <p class="justify alingbtn2">
                                        <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <br><br>
                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="client-logo pt-1 pb-1 margenes">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h1-title">Alarmas de seguridad para la casa</h2>
                </div>
            </div>
        </div>
    </section>


    <section class="section counter pt-1 pb-1">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <p class="parrafo">Cuando tu seguridad y la de tu familia es lo primero, la instalación de una alarma para casa es una solución que te ayudará a estar más protegido, a evitar posibles robos en tu vivienda y a preservar tu patrimonio.
                    <br><br>
                    Las alarmas para casa se deben adaptar a las necesidades de cada cliente y, además de ofrecerle seguridad, deben poseer otros valores añadidos que respondan a las peticiones del cliente y también a las propias características de la casa.</p>                                     	
                </div>                          
            </div>          
        </div>
        <br><br><br>

        <div id="mobileInfo" class="container mx-0 px-0">
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" id="mobile1" src="{{URL::asset('img/1.png') }}" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1" alt="">
                <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/2.png') }}" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2" alt="">
                <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/3.png') }}" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3" alt="">
                <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/4.png') }}" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4" alt="">
                <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/5.png') }}" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5" alt="">
                <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/6.png') }}" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6" alt="">
                <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" id="mobile1" src="{{URL::asset('img/7.png') }}" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7" alt="">
                <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="desktopInfo" class="container">
            <div class="row">
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/1grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casa con jardín</h1>
                        <p class="parrafo mt-2">Es importante que el equipo de alarma incluya entre sus componentes detectores exteriores, que permitan delatar un intento de intrusión antes de que el ladrón acceda al interior de la vivienda. Además, si la casa tiene fácil acceso desde el exterior, a través de ventanas o puertas secundarias, es recomendable la instalación de detectores de apertura de ventanas y puertas, que alertarán de cualquier acceso no deseado.</p>
                    </div>
                </div>
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/2grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Sótanos, garajes o piscinas</h1>
                       <p class="parrafo mt-2">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua</p>
                    </div>
                </div>
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/3grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Segunda vivienda (residencia esporádica) </h1>
                        <p class="parrafo mt-2">La instalación de una alarma conectada a Central Receptora de Alarmas (CRA) con servicio de vigilante propio, es lo más aconsejable. Ya que en caso de salto de alarma, se activará el protocolo de aviso a la Policía, que será quien, junto al vigilante que se desplace, realice la verificación presencial del salto de alarma. De esta forma no es necesario que el cliente se desplace a la vivienda para realizar la verificación.</p>
                    </div>
                </div>
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/4grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Zona de urbanización con seguridad privada</h1>
                        <p class="parrafo mt-2">El cliente podrá dejar como contacto principal el de los responsables de seguridad de la misma, que acudirán de inmediato en caso de recibir la llamada de la Central Receptora de Alarmas.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/5grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casas pequeñas</h1>
                        <p class="parrafo mt-2">Una alarma inalámbrica conectada a Central Receptora de Alarmas (CRA) y con sistema de videoverificación puede ser la solución más indicada. Si se escoge esta opción, el equipo de expertos de la CRA se encargará de llevar a cabo la verificación en caso de que se produzca un salto de alarma, y de avisar a la Policía en caso de que se trate de un salto de alarma real. Otra alternativa que existe en el mercado y que podría ajustarse a las necesidades de una pequeña casa, son las alarmas sin conexión a Central Receptora de Alarmas. Sus costes son inferiores al no estar conectadas continuamente a la Central Receptora de Alarmas de la empresa de seguridad correspondiente. Esto implica que el propio usuario será el que reciba la alerta y el que deba desplazarse personalmente a la vivienda para verificar el salto de alarma. <br>En ambos casos, no será necesaria la realización de obras.</p>
                    </div>
                </div>
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/6grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casas grandes (con gran valor económico) </h1>
                        <p class="parrafo mt-2">La solución indicada es un sistema de alarma cableado. La principal ventaja de los sistemas de alarmas cableados para casas es que, al estar conectados físicamente a la central, son totalmente resistente a posibles inhibiciones y posibilitan la detección inmediata de cualquier fallo en los elementos del sistema.Este tipo de alarmas para casa sí requiere obras en su instalación y suelen incluir elementos menos estéticos, pero el nivel de seguridad es mucho mayor que el que se puede conseguir con una alarma inalámbrica.</p>
                    </div>
                </div>
                <div class="col-lg-3 mx-0 px-0">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/7grande.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casa sin suministro eléctrico</h1>
                        <p class="parrafo mt-2">Gracias a baterías externas de larga duración, normalmente de zinc, la alarma para casa podría funcionar durante un año sin necesidad de conexión de luz.</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <center>
                <div class="mt-5">
                    <a href="{{ route('companies.comparator')}}" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                </div>
            </center>
            <br><br><br>
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <img src="{{URL::asset('img/appsmoviles.png') }}" class="img-fluid" alt="">
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4"> APPS MOVILES</p>
                    <p class="parrafo mt-2">Existen aplicaciones móviles desde las que controlar lo que pasa en la casa, así como recibir imágenes de las diferentes cámaras del sistema de seguridad. Este tipo de soluciones te permiten activar y desactivar la alarma de forma remota u obtener imágenes de tu casa en cualquier momento, estés donde estés.</p>
                </div>
            </div>
        </div>
    </section>

    <section style="background-color: #eff5f8;">
        <div class="container">
                <br><br>            
            <div class="row mt-4" style="padding:0px 15px">
                <p class="parrafo mt-2">El técnico especializado que visite la casa deberá tener en cuenta todos estos aspectos para ofrecer la alarma que se adapte mejor en cada caso. Él será el encargado de recomendar el tipo de elementos necesarios y el número de los mismos. Por ejemplo, el número de detectores volumétricos será mayor si dispones de una casa con varias alturas y con un número alto de habitaciones.
                <br><br>
                Cada casa tiene diferentes necesidades de seguridad. Por eso, en Alarmalia te ayudamos a encontrar la mejor solución en base a tus requerimientos específicos y características del inmueble que deseas proteger. 
                <br><br>
                Compara ahora y consigue, además, nuestra guía de seguridad gratuita, con la que conocer detalladamente los aspectos más importantes que debes tener en cuenta antes de contratar una alarma para tu casa.</p>
            </div>
        </div>
                <br><br>
    </section>

@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 3) )
        @include('layouts.modal_fijo')
    @endif
@endif
   
    @include('layouts.encuentra_tu_alarma') 
    @include('layouts.footer')

    <script type="text/javascript">

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });

        $(document).ready(function() {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#desktopInfo').show();
                    $('#mobileInfo').hide();
                } else {
                    $('#desktopInfo').hide();
                    $('#mobileInfo').show();
                }
            };
            if (window.innerWidth > 575) {
                $('#desktopInfo').show();
                $('#mobileInfo').hide();
            } else {
                $('#desktopInfo').hide();
                $('#mobileInfo').show();
            }
        });
    </script>

</body>

</html>