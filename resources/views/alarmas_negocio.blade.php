<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 4) )
        @include('layouts.modal_flotante')
    @endif
@endif
  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->
    <section class="bg-home-1" id="home" >
        <div class="home-bg-negocio"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">

                         <div class="col-lg-6">
                   
                            <div class="home-desc-center margin-home">
                                <h1 class="tittle-img alingtittle"  >Alarmas para negocio</h1>                                
                                                
                                <div class="mt-9 alingbtn">
                                    <p class="justify">
                                        <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <br><br>
                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="client-logo pt-1 pb-1 margenes2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h1-title">Alarmas de seguridad para empresas</h2>
                </div>
            </div>
        </div>
    </section>
		
    <section class="section counter pt-1 pb-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="parrafo mt-2" >La protección de un negocio es un aspecto primordial y que se debe tener en cuenta, independientemente de su tamaño o actividad. Para muchos casos, como es el de autónomos o pequeños comercios, puede ser además la base de la economía familiar, por lo que su tranquilidad pasa por proteger el negocio de posibles robos o adversidades que ponga en peligro los bienes que se encuentren en el establecimiento.
                    <br><br>
                    Existen diversos tipos de sistemas de alarma para negocios, ya que cada empresa o local cuenta con características muy distintas y, por tanto, con necesidades diferentes. El método habitual es que, previamente a la instalación de un sistema de seguridad de alarmas en un negocio, la empresa de seguridad analice el caso concreto y estudie las características particulares del negocio para proponer el sistema de alarma óptimo.</p>
                    <br>
                    <h1 class="title-heading text-center" style="color: #614FA2 !important;">¿ El negocio requiere de un sistema de alarma de negocio cableado o inalámbrico?</h1> 
                    <p class="parrafo mt-4">Esta cuestión depende de muchos factores, uno de los de mayor peso es el tamaño del establecimiento. Si estamos ante un local más bien pequeño seguramente con un sistema de alarmas de negocio inalámbrico en el que los elementos de la misma (sensores, detectores…) se conecten con la central por medio de ondas de radio será suficiente. Sin embargo, si lo que estamos protegiendo es una nave de dimensiones considerables o un local grande con zonas amplias, probablemente sea necesario un sistema mixto o híbrido,que combina elementos cableados (en el que los elementos de la alarma de negocio se conectan con la central por medio de cable) e inalámbricos.</p>
                 
                    
                </div>                          
            </div>          
        </div>
    
        <div class="col-lg-12" style="padding-right: 0px;padding-left: 0px;">  
        <img src="{{URL::asset('img/banercam.png') }}"  class="img-fluid" alt="" style="height: 12rem;">
        </div>
        <br>
           

        <div class="container" id="Desktop" style="display: none;">
            <p class="parrafo mt-2">Existen varios aspectos importantes que se deben tener en cuenta sobre la importancia de contar con una alarma para negocio:</p>
            <div class="row">
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/1.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Alarma como elemento disuasorio</h1>
                        <p class="title-desc text-center text-white-50">El hecho de contar con una instalación de alarma e informar de ello en la fachada por medio de una placa que lo acredite, es ya un elemento disuasorio en sí mismo.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/2.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Protección continua</h1>
                        <br><p class="title-desc text-center text-white-50 mt-2">Contar con un sistema de alarma conectado con una Central Receptora de Alarmas (CRA) garantiza la seguridad de que el negocio está protegido las 24 horas del día, los siete días de la semana.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/3.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Conexión CRA </h1>
                        <br><p class="title-desc text-center text-white-50 mt-2">Contar con una alarma conectada con una Central Receptora de Alarmas es un valor añadido que ofrece mayor tranquilidad. Las alarmas para negocio conectadas lanzan un aviso de salto de alarma a la Central Receptora de Alarmas y es ésta la que verifica de inmediato si se trata de un salto real para,en caso afirmativo, dar el aviso a la policía para que se persone en el local.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/4.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Botones SOS</h1>
                        <br><br><p class="title-desc text-center text-white-50 mt-3">Algunos proveedores incluyen en sus servicios de alarmas para negocio la posibilidad de instalar un botón que sea rápidamente accesible para dar un aviso de atraco, y que con un movimiento sutil el personal del negocio pueda alertar de un incidente.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/5.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Detectores con cámara</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Normalmente, los sistemas de alarma para negocio se instalan concámaras que recojan imágenes ante un salto de alarma, para poder monitorizar lo sucedido.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/6.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Servicios adicionales </h1>
                        <p class="title-desc text-center text-white-50 mt-4">Dependiendo de la actividad del negocio, pueden ser muy importantes la detección y proteccion ante incendios o ante inundaciones. Los proveedores de alarmas suelen contar con la tecnología necesaria para instalar detectores que avisen ante estos acontecimientos.</p>
                    </div>
                </div>
                 <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/7.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Empresas de seguridad</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Gran parte de las empresas de alarmas, ofrecen servicios complementarios que hacen más efectiva la protección con alarma del negocio, como contar con vigilantes propios que acudan al local ante un salto de alarma para verificar y controlar la situación.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/numbers/8.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center" style="color: #614FA2 !important;">Ley de privacidad</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Esta ley distingue entre varios grados de seguridad, siendo los de grado 2 de riesgo bajo a medio. Se refiere a pequeños establecimientos o industrias que podrán optar por un sistema de alarma conectado a una Central Receptora de Alarmas (CRA), y los de grado 3, de riesgo medio/alto. Estos son establecimientos que están obligados a contar con un sistema de alarmas con conexión con una CRA, tales como gasolineras, joyerías, negocios de compra/venta de oro, estancos, farmacias, locales de cambio de divisas o bancos, entre otros. Estos grados habrá que tenerlos en cuenta en el caso de alarmas para negocio.</p>
                    </div>
                </div>
            </div>
            <center>
        </div>


<!-- mobile-->

        <div class="container" id="Mobile1" style="display: none;">
            <p class="parrafo mt-2">Existen varios aspectos importantes que se deben tener en cuenta sobre la importancia de contar con una alarma para negocio:</p>
            <div class="row">
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/1xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1" alt="">
                <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">El hecho de contar con una instalación de alarma e informar de ello en la fachada por medio de una placa que lo acredite, es ya un elemento disuasorio en sí mismo.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/2xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2" alt="">
                <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Contar con un sistema de alarma conectado con una Central Receptora de Alarmas (CRA) garantiza la seguridad de que el negocio está protegido las 24 horas del día, los siete días de la semana.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/3xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3" alt="">
                <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Contar con una alarma conectada con una Central Receptora de Alarmas es un valor añadido que ofrece mayor tranquilidad. Las alarmas para negocio conectadas lanzan un aviso de salto de alarma a la Central Receptora de Alarmas y es ésta la que verifica de inmediato si se trata de un salto real para,en caso afirmativo, dar el aviso a la policía para que se persone en el local.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/4xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4" alt="">
                <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Algunos proveedores incluyen en sus servicios de alarmas para negocio la posibilidad de instalar un botón que sea rápidamente accesible para dar un aviso de atraco, y que con un movimiento sutil el personal del negocio pueda alertar de un incidente.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/5xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5" alt="">
                <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Normalmente, los sistemas de alarma para negocio se instalan concámaras que recojan imágenes ante un salto de alarma, para poder monitorizar lo sucedido.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/6xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6" alt="">
                <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Dependiendo de la actividad del negocio, pueden ser muy importantes la detección y proteccion ante incendios o ante inundaciones. Los proveedores de alarmas suelen contar con la tecnología necesaria para instalar detectores que avisen ante estos acontecimientos.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/7xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7" alt="">
                <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Gran parte de las empresas de alarmas, ofrecen servicios complementarios que hacen más efectiva la protección con alarma del negocio, como contar con vigilantes propios que acudan al local ante un salto de alarma para verificar y controlar la situación.</p>
                    </div>
                </div>
            </div>
            <div class="accordion mx-0 px-0" id="accordionExample">
                <img class="mx-0 px-0" style="width: 100%;" src="{{URL::asset('img/numbers/8xalarmanegocio.png') }}" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8" alt="">
                <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p class="parrafo mt-2">Esta ley distingue entre varios grados de seguridad, siendo los de grado 2 de riesgo bajo a medio. Se refiere a pequeños establecimientos o industrias que podrán optar por un sistema de alarma conectado a una Central Receptora de Alarmas (CRA), y los de grado 3, de riesgo medio/alto. Estos son establecimientos que están obligados a contar con un sistema de alarmas con conexión con una CRA, tales como gasolineras, joyerías, negocios de compra/venta de oro, estancos, farmacias, locales de cambio de divisas o bancos, entre otros. Estos grados habrá que tenerlos en cuenta en el caso de alarmas para negocio.</p>
                    </div>
                </div>
            </div>
            </div>
            <center>
        </div>


    </section>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 4) )
        @include('layouts.modal_fijo')
    @endif
@endif
@include('layouts.encuentra_tu_alarma') 
@include('layouts.footer')

</body>
<script type="text/javascript">

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });

    $(document).ready(function() {
        if (window.innerWidth > 700) {
            $('#Mobile1').hide();
            $('#Desktop').show();
        } else {
            $('#Desktop').hide();
            $('#Mobile1').show();
          
        }
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            $(this).siblings('active').addClass("active");
            $(this).addClass("active");
        });
        window.onresize = function() {
            if (window.innerWidth > 700) {
            $('#Mobile1').hide();
            $('#Desktop').show();
        } else {
            $('#Desktop').hide();
            $('#Mobile1').show();
          
        }
        };
    });
</script>

</html>