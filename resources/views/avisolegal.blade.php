<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

@include('layouts.encabezado_fijo')

   <!-- START COUNTER -->
    <section class="section counter pt-1 pb-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-headin text-center" style="color: #222222;"> AVISO LEGAL</h1>
                    <p class="label_anun mt-4" style="font-family: IBM Plex Sans;	font-weight: bold;color: #565656;">
                    Condiciones generales de acceso y uso del portal “alarmalia.com” </p>
                
                    <br>
                    <p class="title-desc text-justify text-white-50">1. CONDICIONES LEGALES Y SU ACEPTACIÓN</p>
                    <p class="title-desc text-justify text-white-50 mt-4">
                    1.1. Estas condiciones generales regulan el uso del sitio de Internet WWW.ALARMALIA.COM (en adelante el “Portal”), y de los servicios que a través de ella presta MARKETPLACE SEGURIDAD, S.L., con C.I.F. B-67311290, domiciliada en Barcelona, calle Muntaner número 452, 1º 2ª, 08006 Barcelona (en adelante, “Alarmalia.com”) e inscrita en el Registro Mercantil de Barcelona, al Tomo 46679, Folio 144, Hoja B-527552, inscripción 1ª.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50 mt-4"> 1.2. La utilización del Portal atribuye la condición de usuario del Portal (en adelante, el “Usuario” o conjuntamente, los “Usuarios”) y expresa la aceptación plena y sin reservas del Usuario de las presentes condiciones generales (en adelante, las “Condiciones Generales”) en la versión publicada por www.alarmalia.com en el momento en que el Usuario acceda al Portal.</p>
                    <br>
                    <br>
                    <p class="title-desc text-justify text-white-50">2. OBJETO </p>
                    <p class="title-desc text-justify text-white-50 mt-4"> 2.1. A través del Portal, www.alarmalia.com facilita a los Usuarios el acceso y la utilización de servicios de publicidad comparativa e información sobre las distintas alarmas que existentes en el mercado.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">2.2. Servicio de publicidad e información </p>
                    <p class="title-desc text-justify text-white-50 mt-4">2.1.1. Alarmalia.com facilita a los Usuarios información sobre alarmas que existen en el mercado de acuerdo a las respuestas al cuestionario comparativo que le ofrecemos y a través de la guía que se facilita a través del Portal.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">2.1.2. Alarmalia.com no ofrece ni comercializa los productos y servicios disponibles en la información suministrada en el portal, ni asume responsabilidad alguna por tales productos o servicios.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">2.1.3. En el marco de este servicio el Usuario podrá facilitar voluntariamente a Alarmalia.com su teléfono, quedando estos datos de carácter personal sujetos al consentimiento de la actual política de protección de datos española.</p>
                   
                 <br>
                    <p class="title-desc text-justify text-white-50">3. CONDICIONES DE ACCESO Y UTILIZACIÓN DEL PORTAL POR PARTE DEL USUARIORTE</p>
                    <p class="title-desc text-justify text-white-50 mt-4">3.1. La prestación de los servicios del Portal por parte de Alarmalia.com tiene en principio, y salvo lo expuesto en el párrafo siguiente, carácter totalmente gratuito para el Usuario.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">3.2. No obstante lo anterior Alarmalia.com podrá, en cualquier momento, modificar las condiciones de acceso al servicio. Dichas modificaciones sólo serán aplicables a partir de su entrada en vigor.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">3.3. El Usuario se compromete a utilizar el Portal y los servicios de conformidad con la ley, estas Condiciones Generales, las buenas prácticas generalmente aceptadas, así como de forma correcta y diligente, absteniéndose de utilizarlos con fines o efectos ilícitos, prohibidos por las presentes Condiciones Generales o lesivos de los derechos e intereses de terceros.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">3.4. Alarmalia.com se reserva el derecho de modificar unilateralmente, en cualquier momento y sin previo aviso, la presentación y configuración del Portal y de los servicios prestados a través de él, así como sus condiciones de utilización.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">3.5. El Usuario es consciente, y acepta voluntariamente, que el uso del Portal, de los Contenidos y de los servicios tiene lugar, en todo caso, bajo su única y exclusiva responsabilidad.</p>

                 <br>
                    <p class="title-desc text-justify text-white-50">4. USO DE LAS “COOKIES”</p>
                    <p class="title-desc text-justify text-white-50 mt-4">4.1. Alarmalia.com utiliza cookies. El uso de cookies se regulará por la Política de Privacidad y Cookies, la cual, explica qué son, como se utilizan por Alarmalia.com y cómo gestionarlas o deshabilitarlas por el Usuario.</p>

                 <br>
                    <p class="title-desc text-justify text-white-50">5. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.1. Los dispositivos de enlace (links) que el Portal pone a disposición de los Usuarios tienen por único objeto facilitar a los Usuarios la búsqueda de la información disponible en Internet.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.2. Alarmalia.com no ofrece ni comercializa los productos y servicios disponibles en los sitios enlazados ni asume responsabilidad alguna por tales productos o servicios.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.3. Alarmalia.com únicamente pone a disposición información pública de las entidades, y, en consecuencia no podrá ser considerado responsable por la idoneidad de la información facilitada. El Usuario reconoce y acepta que es de su exclusiva responsabilidad el verificar los términos y condiciones de los productos y servicios de las empresas instaladoras de alarmas antes de proceder a su contratación.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.4. El Usuario deja expresamente indemne a Alarmalia.com de toda responsabilidad frente a la falta de idoneidad o adecuación de la información suministrada por Alarmalia.com y los términos y condiciones de los productos o servicios en el momento de la contratación.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.5. Por tanto Alarmalia.com excluye frente al Usuario cualquier responsabilidad u obligación derivada de la oferta y/o contratación de los servicios incluidos en el párrafo anterior, por la calidad y ausencia de defectos de los citados servicios, y por el cumplimiento de las obligaciones contraídas por las empresas instaladoras de alarmas, mediante la oferta y los contratos realizados.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.6. Alarmalia.com no será responsable del uso que los Usuarios o cualquier tercero hagan del Portal, ni garantiza que lo hagan conforme a las presentes Condiciones Generales.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.7. Alarmalia.com no será responsable de los fallos que pudieran producirse en las comunicaciones, incluido el borrado, transmisión incompleta o retrasos en la remisión, no comprometiéndose tampoco a que la red de transmisión esté operativa en todo momento. Tampoco responderá en caso de que un tercero, quebrantando las medidas de seguridad establecidas por Alarmalia.com y sus servidores, acceda a los mensajes o los utilice para la remisión de virus informáticos.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.8. Alarmalia.com no puede garantizar la invulnerabilidad absoluta de sus sistemas de seguridad, ni puede garantizar la seguridad o inviolabilidad de dichos datos en su transmisión a través de la red.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.9. Alarmalia.com no garantiza la licitud de los contenidos de terceros a los que se acceda a través de su Portal, ni la veracidad, exactitud, fiabilidad y utilidad de los contenidos que contiene el Portal, pues estos datos son recogidos de portales externos de las empresas de seguridad instaladoras de alarmas..</p>
                    <p class="title-desc text-justify text-white-50 mt-4">5.10. Alarmalia.com no controla ni garantiza la ausencia de virus informáticos en los servicios prestados por terceros a través del Portal que puedan producir alteraciones en su sistema informático (software y hardware) o en los documentos y ficheros electrónicos almacenados en su sistema informático.</p>

                 <br>
                    <p class="title-desc text-justify text-white-50">6. PROPIEDAD INTELECTUAL E INDUSTRIAL</p>
                    <p class="title-desc text-justify text-white-50 mt-4">6.1. Este Portal, el nombre comercial, las marcas, los logos y el resto de servicios y contenidos del Portal están protegidos por la legislación de propiedad intelectual e industrial, y, o bien son titularidad exclusiva de Alarmalia.com, o bien su uso ha sido debidamente autorizado por sus correspondientes titulares.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">6.2. Alarmalia.com no concede ninguna licencia o autorización de uso de ninguna clase sobre sus derechos de propiedad industrial e intelectual o sobre cualquier otra propiedad o derecho relacionado con el Portal, los servicios o los contenidos.</p>             

                 <br>
                    <p class="title-desc text-justify text-white-50">7. DENEGACIÓN Y RETIRADA DEL ACCESO AL PORTAL Y/O A LOS SERVICIOS</p>
                    <p class="title-desc text-justify text-white-50 mt-4">7.1. Alarmalia.com se reserva el derecho a denegar o retirar el acceso al Portal y/o a los servicios, en cualquier momento y sin necesidad de aviso previo a los Usuarios que incumplan las presentes Condiciones Generales.</p>
                  
                 <br>
                    <p class="title-desc text-justify text-white-50">8. LEY APLICABLE Y JURISDICCIÓN</p>
                    <p class="title-desc text-justify text-white-50 mt-4">8.1. El acceso al Portal y su utilización, así como la interpretación, cumplimiento y ejecución de los presentes Condiciones Generales se sujetará a lo establecido por las leyes españolas.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">8.2. Alarmalia.com y el Usuario, con renuncia expresa a cualquier otro fuero, se someten al de los Juzgados y Tribunales de la ciudad de Barcelona para cualquier controversia que pudiera derivarse de la prestación de los servicios objeto de estas Condiciones Generales.</p>
                  
                  
                </div>                          
            </div>          
        </div>
        <br><br><br>
        

    </section>
    <!-- END COUNTER -->
		
@include('layouts.encuentra_tu_alarma') 
@include('layouts.footer')

</body>

</html>