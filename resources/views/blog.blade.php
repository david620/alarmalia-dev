<!DOCTYPE html>
<html lang="es">

@include('layouts.head')
<style type="text/css">
.pagination > li > a,
.pagination > li > span {
    color: black; // use your own color here
}

.pagination > .active > a,
.pagination > .active > a:focus,
.pagination > .active > a:hover,
.pagination > .active > span,
.pagination > .active > span:focus,
.pagination > .active > span:hover {
    background-color: black;
    border-color: black;
}    

</style>
<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 6) )
        @include('layouts.modal_flotante')
    @endif
@endif
 
@include('layouts.header')


@include('layouts.encabezado_fijo')

    <section class="section counter mt-6 client-logo" style="background-color: white;">
        <div class="container">
            <h1 class="title-heading text-center">Sistemas de alarmas para casa y negocio</h1>            
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-6">
                                <div class="servicios-box bg-white btn-rounde mt-4" style="background: transparent;">
                                    <div class="service-icon text-center">
                                        <img src="{{URL::asset($post->url_img) }}"  height="max-content" alt="">
                                    </div><br>
                                    <h1 class="title-headin text-center">{{$post->title}}</h1>
                                    <div class="parrafo mt-2">
                                        {!!trim(substr($post->content, 0, 200))!!}...
                                    </div>
                                    <br>
                                    <a href="{{route('post.blog', $post->id)}}" class="btn btn-secondary  btn-round">Leer nota completa</a>
                                </div>
                            </div> 
                        @endforeach               
                    </div>                      
            <div class="container">        
             <a >{{ $posts->links() }}</a> 
            </div>
        </div>
     </section>        
    
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 6) )
        @include('layouts.modal_fijo')
    @endif
@endif

@include('layouts.encuentra_tu_alarma')     
@include('layouts.footer')
    <script type="text/javascript">

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });
    </script>
</body>

</html>