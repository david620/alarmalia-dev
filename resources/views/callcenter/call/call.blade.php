@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Call
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
                	<br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Posibles Respuestas</h3>
                </div>
                <div class="box-body">
                    <a class="btn btn-success btn-block" href="{{route('callcenter.positive', $callcenter->id)}}">SI</a>
                    <a class="btn btn-danger btn-block" href="{{route('callcenter.negative', $callcenter->id)}}">NO</a>
                    <a class="btn btn-warning btn-block" href="{{route('callcenter.dont_answer', $callcenter->id)}}">NO CONTESTA</a>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
            <div class="box box-primary">


			         <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">NÚMERO DE TELÉFONO A MARCAR:  <b>{{$callcenter->phone}}</b></h3>
			            </div>
						<div class="box-body">
							<p align="justify" style="font-size:25px">
								Buenos días, le llamo de ALARMALIA.COM, hemos recibido su teléfono a través de nuestro cuestionario web en Alarmalia.com. Está usted interesado en conocer o contratar algún servicio de seguridad?
							</p>	
						</div> 
			            <!-- form start -->
			          </div>

		    </div>
	    </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example4" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Pregunta</th>
							<th>Respuesta</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($lr as $r)
						<tr>
							<td>{{$r->question}}</td>
							<td>{{$r->response}}</td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>


    </div>
@endsection
