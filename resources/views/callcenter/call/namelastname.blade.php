@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Nombres y Apellidos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">

	{!! Form::open(array('route' => ['callcenter.end_positive_answer', $callcenter->id],'method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
	{{ csrf_field() }}
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-body">
                        <div class="form-group">
                            <label for="ejemplo">Nombres y Apellidos</label>
                            {!! Form::text('namelastname', '', array('placeholder'=>'Escribe aquí los nombres y apellidos', 'class' => 'form-control')) !!}
                        </div>
					{!! Form::submit('FINALIZAR LLAMADA', ['class' => 'btn btn-warning btn-block']) !!}
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
            <div class="box box-primary">


			         <div class="box box-primary">
						<div class="box-body">
							<p align="justify" style="font-size:25px">
								Le agradeceriamos si nos proporciona sus datos como Nombres y Apellidos para realizar la experiencia más personalizada. Muchas Gracias.
							</p>	
						</div> 
			            <!-- form start -->
			          </div>

		    </div>
	    </div>
    </div>
	{!! Form::close() !!}

</div>
@endsection
