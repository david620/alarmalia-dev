@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Respuesta Negativa
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Opciones según tipo de llamada</h3>
                </div>
                <div class="box-body">
                    <a class="btn btn-success btn-block" href="{{route('callcenter.negative_answer', [$id, 1])}}">No me interesa</a>
                    <a class="btn btn-danger btn-block"  href="{{route('callcenter.negative_answer', [$id, 2])}}">No he rellenado ningún cuestionario</a>
                    <a class="btn btn-warning btn-block" href="{{route('callcenter.negative_answer', [$id, 3])}}">No, era sólo una prueba</a>
                    <a class="btn btn-default btn-block" href="{{route('callcenter.negative_answer', [$id, 4])}}">Quiero que eliminen mi número</a>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
            <div class="box box-primary">

			         <div class="box box-primary">
			            <div class="box-header with-border">
			            </div>
						<div class="box-body">
							<p align="justify" style="font-size:25px">
								Le damos las gracias desde Alarmalia.com por su tiempo y colaboración, muy buenos días.
							</p>	
						</div> 
			            <!-- form start -->
			          </div>

		    </div>
	    </div>
    </div>


@endsection
