@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Positiva
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">

	{!! Form::open(array('route' => ['callcenter.positive_answer', $id],'method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
	{{ csrf_field() }}
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
                	<br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acción</h3>
                </div>
                <div class="box-body">
					{!! Form::submit('SIGUIENTE', ['class' => 'btn btn-warning btn-block']) !!}
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
            <div class="box box-primary">


			         <div class="box box-primary">
						<div class="box-body">
							<p align="justify" style="font-size:25px">
								Le damos las gracias desde Alarmalia.com por su tiempo y colaboración, se pondrán en contacto con usted los diferentes instaladores para ampliarle la información que necesite. Muy buenos días.
							</p>	
						</div> 
			            <!-- form start -->
			          </div>

		    </div>
	    </div>
    </div>

    <div class="row">
        <div class="col-md-6 ">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
		        <div class="box-header">
		            <h3 class="box-title">Está usted interesado en alguna alarma en particular: (Alarmas con Contrato Activo)</h3>
		        </div>
	            <table id="example" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Empresa</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
                    	@foreach($contracts as $c)
						<tr>
							<td>{{$c->name}}</td>
							<td>
								{!! Form::checkbox('ids_c[]', $c->id_cliente, 0); !!}
							</td>
						</tr>
						@endforeach
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>

        <div class="col-md-6 ">

          <div class="box">
			<div class="box-body">
		        <div class="box-header">
		            <h3 class="box-title">Está usted interesado en alguna alarma en particular: (Alarmas sin Contrato)</h3>
		        </div>
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Empresa</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
                    	@foreach($all_contracts as $c)
						<tr>
							<td>{{$c->name}}</td>
							<td>
								{!! Form::checkbox('ids_c[]', $c->id_cliente, 0); !!}
							</td>
						</tr>
						@endforeach
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>

    </div>
</div>
{!! Form::close() !!}

@endsection
