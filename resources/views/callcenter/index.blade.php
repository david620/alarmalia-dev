@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Call Center
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Fecha</th>
							<th>Hora</th>
							<th>Teléfono</th>
							<th>Call-Center</th>
							<th>Última Llamada</th>
							<th>Duración desde última llamada</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($callcenter as $cc)
							@php 
								$end = \Carbon\Carbon::now();
								$algo = new DateTime($cc->fecha);
								$fecha = ($algo)->format('d-m-Y');
								$hora = ($algo)->format('H:i:s');
								$start    = new \Carbon\Carbon($cc->last_call);
								$tiempo = $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S');
							@endphp
						<tr>
							<td>{{$fecha}}</td>
							<td>{{$hora}}</td>
							<td>{{$cc->phone}}</td>
							<td><center>
								@if($cc->calls == 0)
									<i class="fa fa-phone" aria-hidden="true"> Sin Llamadas</i> 
								@endif
								@if($cc->calls == 1)
									<i class="fa fa-phone" aria-hidden="true" style="color:#d9534f; font-size: 18px;"><strong> 1<sup>ra</sup> Llamada</strong></i>            
								@endif								
								@if($cc->calls == 2)
									<i class="fa fa-phone" aria-hidden="true" style="color:#008f39; font-size: 18px;"><strong> 2<sup>nda</sup> Llamada</strong></i>   
								@endif								


							</center></td>
							<td>
								@if(!$cc->last_call)
									<center> - </center>
								@else
									{{$cc->last_call}}
								@endif

							</td>
							<td>
								{{$tiempo}}									
							</td>
							<td>
                                    {!!link_to_route('callcenter.call', $title = 'Llamar',
                                    $parameters = ['id' => $cc->id],
                                    $attributes = ['class'=>'btn btn-success','style'=>'width:100%']);!!}
								
							</td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
