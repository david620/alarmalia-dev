@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Call Center
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{ route('callcenter.export')}}">Exportar a Excel</a>		
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Teléfono</th>
							<th>Call-Center</th>
							<th>Última Llamada</th>
							<th>Contestado</th>

						</tr>
                    </thead>
                    <tbody>
						@foreach ($callcenter as $cc)
						<td>
							<a href="{{ route('callcenter.called', $cc->id)}}">
                            	{{$cc->phone}}
							</a>
                        </td>
							<td><center>
								@if($cc->calls == 0)
									<i class="fa fa-phone" aria-hidden="true"> Sin Llamadas</i> 
								@endif
								@if($cc->calls == 1)
									<i class="fa fa-phone" aria-hidden="true" style="color:#d9534f; font-size: 18px;"><strong> 1<sup>ra</sup> Llamada</strong></i>            
								@endif								
								@if($cc->calls == 2)
									<i class="fa fa-phone" aria-hidden="true" style="color:#008f39; font-size: 18px;"><strong> 2<sup>nda</sup> Llamada</strong></i>   
								@endif								
								@if($cc->calls == 3)
									<i class="fa fa-phone" aria-hidden="true" style="color:#008f39; font-size: 18px;"><strong> 3<sup>ra</sup> Llamada</strong></i>   
								@endif								


							</center></td>
							<td>
								@if(!$cc->last_call)
									<center> - </center>
								@else
									{{$cc->last_call}}
								@endif
							</td>
							<td>
								@if($cc->answered == 0)
									No Contestó
								@endif								
								@if($cc->answered == 1)
									Contestó
								@endif								
							</td>

						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
