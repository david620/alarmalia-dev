@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Called
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
                	<br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Nombres y Apellidos</h3>
                </div>
                <div class="box-body">
                    <h3 class="box-title">
                    	@if(isset($callcenter->namelastname))
                    		<center>{{$callcenter->namelastname}}</center>
                    	@endif
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
          <div class="box">
			<div class="box-body">
	            <table id="example4" class="table table-bordered table-hover">
	                	@php
                    		$ab = explode(',', $callcenter->ids_companies);
                    	@endphp
                    <thead>
                    	<tr class="header">
							<th>Compañias Seleccionadas: </th>
						</tr>
                    </thead>
                    <tbody>
                    	@if($ab)
							@foreach ($ab as $b)
							<tr>
								<td>

										@if(isset((\App\Models\Clients\Client::where('id', '=',$b)->first()->name)))
{{											(\App\Models\Clients\Client::where('id', '=',$b)->first()->name) }}
										@endif

								</td>
							</tr>
							@endforeach 
                       @endif
                    </tbody>
                </table>
            </div>
		  </div>
	    </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example4" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Pregunta</th>
							<th>Respuesta</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($lr as $r)
	                	@php
                    		$ab = explode(',', $r->ids_companies);
                    	@endphp

						<tr>
							<td>{{$r->question}}</td>
							<td>{{$r->response}}</td>	
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>


    </div>
@endsection
