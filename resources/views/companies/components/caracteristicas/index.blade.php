@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Caracteristicas de Componentes de la Compañia
@endsection
@section('contentheader_title')

@stop


@section('main-content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
		        	<div class="table-responsive">
		        		
		        		<table id="" class="table no-border">
		                    <tbody  id="">
		                    	<tr class="">

				                          {!!link_to_route('components_caract.create', $title = 'Crear Caracteristica Componente o Accesorio',
				                          $parameters = ['id' => $id_component],
				                          $attributes = ['class'=>'btn btn-lg bg-blue']);!!}	
								</tr>
		                    </tbody>
		                </table>	
					</div>
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>No</th>
							<th>Nombre</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($components_caract as $key => $component)
						<tr>
							<td>{{$component->id }}</td>
							<td>{{ $component->content }}</td>
							<td><center>
								<a class="btn btn-primary" href="{{ route('components_caract.edit',$component->id) }}">Editar</a>
								{!! Form::open(['method' => 'DELETE','route' => ['components_caract.destroy', $component->id],'style'=>'display:inline']) !!}
			                    <input type="hidden" name="component_id" value="{{$id_component}}">
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
