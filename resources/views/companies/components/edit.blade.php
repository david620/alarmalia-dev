@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Crear Componente/Accesorio
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

              <div class="table-responsive">
                
                <table id="" class="table no-border">
                        <tbody  id="">
                          <tr class="">       
                                  {!!link_to_route('components.index', $title = 'Regresar',
                                  $parameters = ['id' => $components->company_id],
                                  $attributes = ['class'=>'btn btn-lg bg-blue']);!!}  
                          </tr>
                &nbsp;
                          <tr class="">
                            {!!link_to_route('components_caract.index', $title = 'Caracteristicas Componentes/Accesorios',
                            $parameters = ['id' => $components->id],
                            $attributes = ['class'=>'btn btn-lg bg-blue']);!!}  
                          </tr>

                        </tbody>
                    </table>  
          </div>


  {!! Form::model($components, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['components.update', $components->id]]) !!}
    {{ csrf_field() }}



         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Componente/Accesorio</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Nombre:</label>
                {!! Form::text('name', $components->name, array('placeholder' => 'Nombre','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="component_accessory">Componente/Accesorio:</label>
                {!! Form::select('component_accessory',['0' => 'Componente', '1' => 'Accesorio',], $components->component_accessory, array('placeholder' => 'Seleccionar', 'class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="component_icon">Icono Componente:</label>
                  {!!Form::file('component_icon', array('class' => 'form-control'))!!}
                </div>
                    <input type="hidden" name="company_id" value="{{$components->company_id}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Editar Componente/Accesorio</button>
              </div>
            </form>
          </div>

  {!! Form::close() !!}
@endsection
