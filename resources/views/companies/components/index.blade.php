@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Componentes de la Compañia
@endsection
@section('contentheader_title')

@stop


@section('main-content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
		        	<div class="table-responsive">
		        		
		        		<table id="" class="table no-border">
		                    <tbody  id="">
		                    	<tr class="">

				                          {!!link_to_route('components.create', $title = 'Crear Componente o Accesorio',
				                          $parameters = ['id' => $id_company],
				                          $attributes = ['class'=>'btn btn-lg bg-blue']);!!}	
								</tr>
								&nbsp;
								&nbsp;
		                    	<tr class="">
									<a class="btn btn-lg bg-blue" href="{{ route('companies.edit', $id_company)}}">Regresar</a>				
								</tr>
		                    </tbody>
		                </table>	
					</div>
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>No</th>
							<th>Nombre</th>
							<th>Componente/Accesorio</th>
							<th>Icono</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($components as $key => $component)
						<tr>
							<td>{{ $id_company }}</td>
							<td>{{ $component->name }}</td>
							<td>
								@if($component->component_accessory == 0)
								<p align="justify">	
									Componente
								</p>
								@endif
								@if($component->component_accessory == 1)
								<p align="justify">	
									Accesorio
								</p>
								@endif
							</td>
							<td><p align="justify"><font color=blue><a style="color:#0000ff" href="{{URL::to($component->icon)}}" target="_blank">{{$component->name}}</a></font></p></td>
							<td><center>
								<a class="btn btn-primary" href="{{ route('components.edit',$component->id) }}">Editar</a>
								{!! Form::open(['method' => 'DELETE','route' => ['components.destroy', $component->id],'style'=>'display:inline']) !!}
			                    <input type="hidden" name="company_id" value="{{$id_company}}">
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
