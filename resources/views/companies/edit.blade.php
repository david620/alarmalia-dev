@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Editar Compañia
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

		        	<div class="table-responsive">
		        		
		        		<table id="" class="table no-border">
		                    <tbody  id="">
		                    	<tr class="">
										        <a class="btn btn-lg bg-blue" href="{{ route('companies.index')}}">Regresar</a>				
								          </tr>
								&nbsp;
		                    	<tr class="">
                            {!!link_to_route('advantages.index', $title = 'Ventajas y Desventajas',
                            $parameters = ['id' => $company->id],
                            $attributes = ['class'=>'btn btn-lg bg-blue']);!!}	
								          </tr>
                &nbsp;
                          <tr class="">
                            {!!link_to_route('components.index', $title = 'Componentes y Accesorios',
                            $parameters = ['id' => $company->id],
                            $attributes = ['class'=>'btn btn-lg bg-blue']);!!}  
                          </tr>
		                    </tbody>
		                </table>	
					</div>


	{!! Form::model($company, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['companies.update', $company->id]]) !!}
    {{ csrf_field() }}


         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Compañia</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Nombre de la Compañia:</label>
		            {!! Form::text('name', $company->name, array('placeholder' => 'Nombre de la Compañia ','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                  <label for="description">Descripción de la Compañía:</label>
		            {!! Form::textarea('description', $company->description, array('placeholder' => 'Descripción de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor2')) !!}
                </div>
                <div class="form-group">
                  <label for="link_image">Logo de la Compañia:</label>
					{!!Form::file('link_image', array('class' => 'form-control'))!!}
                </div>
                <div class="form-group">
                  <label for="phone">Teléfono de la Compañía:</label>
		            {!! Form::text('phone', $company->phone, array('placeholder' => 'Teléfono de la Compañia ','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                  <label for="url_web">Página de la Compañía:</label>
		            {!! Form::url('url_web', $company->url_web, array('placeholder' => 'Página de la Compañia ','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                  <label for="nivel_seguridad">Nivel de Seguridad de la Compañía:</label>
		            {!! Form::textarea('nivel_seguridad', $company->nivel_seguridad, array('placeholder' => 'Nivel de Seguridad de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor3')) !!}
                </div>
                <div class="form-group">
                  <label for="tecnologia">Tecnología de la Compañía:</label>
		            {!! Form::textarea('tecnologia', $company->tecnologia, array('placeholder' => 'Tecnología de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor4')) !!}
                </div>
                <div class="form-group">
                  <label for="diferencia">Diferencias de la Compañía:</label>
		            {!! Form::textarea('diferencia', $company->diferencia, array('placeholder' => 'Diferencias de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor5')) !!}
                </div>
                <div class="form-group">
                  <label for="precio">Descripción Precios de la Compañía:</label>
		            {!! Form::textarea('precio', $company->precio, array('placeholder' => 'Precios de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor6')) !!}
                </div>

                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="precio_desde">Precios Desde:</label>
                      {!! Form::text('precio_desde', $company->precio_desde, array('placeholder' => 'Precios Desde ','class' => 'form-control')) !!}
                      </div>
                    </div>
                    <div class="col-md-6 ">
                      <div class="form-group">
                        <label for="precio_hasta">Precios Hasta:</label>
                      {!! Form::text('precio_hasta', $company->precio_hasta, array('placeholder' => 'Precios Desde ','class' => 'form-control')) !!}
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="contratar">Contratación de la Compañía:</label>
		            {!! Form::textarea('contratar', $company->contratar, array('placeholder' => 'Contratación de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor7')) !!}
                </div>
                <div class="form-group">
                  <label for="instalacion">Instalación de la Compañía:</label>
		            {!! Form::textarea('instalacion', $company->instalacion, array('placeholder' => 'Instalación de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor8')) !!}
                </div>
                <div class="form-group">
                  <label for="direccion">Dirección de la Compañía:</label>
		            {!! Form::textarea('direccion', $company->direccion, array('placeholder' => 'Dirección de la Compañia ','class' => 'form-control', 'id' => 'summary-ckeditor')) !!}
                </div>
                <div class="form-group">
                  <label for="permanencia">Permanencia de la Compañía:</label>
		            {!! Form::textarea('permanencia', $company->permanencia, array('placeholder' => 'Permanencia de la Compañia','class' => 'form-control', 'id' => 'summary-ckeditor9')) !!}
                </div>
                <div class="form-group">
                  <label for="baja">Darse de Baja de la Compañía:</label>
		            {!! Form::textarea('baja', $company->baja, array('placeholder' => 'Darse de Baja de la Compañía','class' => 'form-control', 'id' => 'summary-ckeditor10')) !!}
                </div>
                <div class="form-group">
                  <label for="facturacion">Facturación de la Compañía:</label>
		            {!! Form::textarea('facturacion', $company->facturacion, array('placeholder' => 'Facturación de la Compañía','class' => 'form-control', 'id' => 'summary-ckeditor11')) !!}
                </div>
                <div class="form-group">
                  <label for="facturacion">Valoración Estrellas:</label>
                    {!! Form::select('estrellas',['1' => '1 Estrella', '2' => '2 Estrellas', '3' => '3 Estrellas', '4' => '4 Estrellas', '5' => '5 Estrellas',], $company->estrellas, array('class' => 'form-control', 'id' => 'tipo', 'placeholder' => 'Seleccionar', 'required' => 'required')) !!}
                </div>
 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Editar Compañia</button>
              </div>
            </form>
          </div>

	{!! Form::close() !!}
@endsection
@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'summary-ckeditor' );
        CKEDITOR.replace( 'summary-ckeditor2' );
        CKEDITOR.replace( 'summary-ckeditor3' );
        CKEDITOR.replace( 'summary-ckeditor4' );
        CKEDITOR.replace( 'summary-ckeditor5' );
        CKEDITOR.replace( 'summary-ckeditor6' );
        CKEDITOR.replace( 'summary-ckeditor7' );
        CKEDITOR.replace( 'summary-ckeditor8' );
        CKEDITOR.replace( 'summary-ckeditor9' );
        CKEDITOR.replace( 'summary-ckeditor10' );
        CKEDITOR.replace( 'summary-ckeditor11' );
    </script>
@stop