<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
    <meta name="keywords" content="alarma, tyco alarmas, prosegur alarmas, vigilante" />
    <meta content="RkPeople" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{URL::asset('img/favicon.png')}}">

    <!-- css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/star-rating.css') }}" rel="stylesheet">
    <script src="{{ asset('js/star-rating.min.js') }}"></script>

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleFrank.css') }}" />

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
    <link href="{{ asset('fonts/css/all.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129577938-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-129577938-1');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M77GWGH');
    </script>
    <!-- End Google Tag Manager -->
    <style type="text/css">
        .carousel-caption {
            top: 60%;
            transform: translateY(-50%);
            bottom: initial;
            left: 60%;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        /*  bhoechie tab */
        div.bhoechie-tab-container {
            z-index: 10;
            background-color: #ffffff;
            padding: 0 !important;
            border-radius: 4px;
            -moz-border-radius: 4px;
            border: 1px solid #ddd;
            margin-top: 20px;
            margin-left: 50px;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            -moz-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            background-clip: padding-box;
            opacity: 0.97;
            filter: alpha(opacity=97);
        }

        div.bhoechie-tab-menu {
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
            color: #EFF5F8;
        }

        div.bhoechie-tab-menu div.list-group>a:first-child {
            border-top-right-radius: 0;
            -moz-border-top-right-radius: 0;
        }

        div.bhoechie-tab-menu div.list-group>a:last-child {
            border-bottom-right-radius: 0;
            -moz-border-bottom-right-radius: 0;
        }

        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa {
            background-color: #EFF5F8;
            background-image: #EFF5F8;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group>a.active:after {
            content: '';
            position: absolute;
            left: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-left: 10px solid #EFF5F8;
        }

        div.bhoechie-tab-content {
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active) {
            display: none;
        }
    </style>


</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M77GWGH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!--Navbar Start-->
    @include('layouts.header')
    <!-- Navbar End -->

    <!-- START HOME de frank
    <section>
        <div id="carouselExampleControls" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block " width="100%" src="{{URL::asset($banners->banner_url)}}" alt="First slide">
                    <div class="carousel-caption">
                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                        <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->
@include('layouts.encabezado_fijo')
    <!-- START COUNTER DESKTOP-->
    <section id="desktopCompanyDetail" class="section counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <br><br>
                    <h1 class="title-heading text-center">
                        <center>
                            <img class="d-block " width="400rem!" src="{{URL::asset($companies->link_image)}}">
                        </center>
                    </h1>
                    <div class="description-company-detail text-center mt-4">{!!$companies->description!!}</div>

                    <br><br><br>

                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="p-2 text-center mt-5">
                                <br><br>
                                <center>
                                    <form class="form-1">
                                        <div class="form-field">
                                            <select id="glsr-ltr" class="star-rating">
                                            @for($i = 1; $i <= $companies->estrellas; $i++)
                                                <option selected value="{{$i}}">{{$i}}</option>
                                            @endfor
                                            </select>
                                        </div>
                                    </form>
                                    <h1 class="description-company text-center">Opiniones de usuarios</h1>
                                </center>
                            </div>
                        </div>
                        @if($companies->nivel_seguridad)
                        <div class="col-lg-6">
                            <div class="p-2 text-center mt-5">
                                <center>
                                    <img src="{{URL::asset('img/alarmalia/somosindependientes.png') }}" /><br /><br />
                                    <h1 class="title-item1 text-center">Nivel de Seguridad</h1>
                                <div class="description-company text-center text-white-50 mt-4">{!!$companies->nivel_seguridad!!}</div>
                                </center>

                            </div>
                        </div>
                        @endif
                    </div>

                </div>
                @if(!(\App\Models\Component::where('company_id', '=',$companies->id)->get()->isEmpty()))                    
                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 bhoechie-tab-menu">
                    <div class="list-group">
                        <a href="#" class="list-group-item text-center">
                            <br />
                            <center>
                                <img src="{{URL::asset('img/icon/group.png') }}" /><br /><br />
                                <h1 class="title-heading text-center">Componentes Básicos</h1>
                            </center>
                        </a>
                        <a href="#" class="list-group-item text-center">
                            <br />
                            <center>
                                <img src="{{URL::asset('img/icon/group-3.png') }}" /><br /><br />
                                <h1 class="title-heading text-center">Accesorios</h1>
                            </center>
                        </a>
                    </div>
                </div>
                @endif
                <div class="col-lg-9 col-md-9 col-sm-7 col-xs-7 bhoechie-tab">
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active" style="margin-top: 0;">
                        <div class="container">
                            @foreach($components as $component)
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="p-3 text-center mt-5">
                                        <center>
                                            <span>
                                                <img  height="90rem!" src="{{URL::asset($component->icon) }}" />
                                                <h1 class="title-headin text-center">{{$component->name}}</h1>
                                            </span>
                                        </center>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="p-2 text-letf mt-1">
                                        <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px;">
                                            @foreach( (\App\Models\ComponentCaract::where('component_id', '=',$component->id)->get()) as $component_car)
                                            <li class="description-company">
                                                <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$component_car->content}}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <hr class="hrCompany">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="bhoechie-tab-content " style="margin-top: 0;">
                        <div class="container">
                            @foreach($components_acc as $component)
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="p-2 text-center mt-5">
                                        <span>
                                                <img  height="90rem!" src="{{URL::asset($component->icon) }}" />
                                            <h1 class="title-headin text-center">{{$component->name}}</h1>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="p-2 text-letf mt-1">
                                        <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px;">
                                            @foreach( (\App\Models\ComponentCaract::where('component_id', '=',$component->id)->get()) as $component_car)
                                            <li class="description-company">
                                                <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$component_car->content}}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <hr class="hrCompany">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- END COUNTER -->

    <!-- START COUNTER MOBILE-->
    <section id="mobileCompanyDetail" class="section counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <center>
                        <img class="d-block " width="50%" height="50%" src="{{URL::asset($companies->link_image)}}">
                    </center>
                    <p class="description-company text-center">{{$companies->name}}</p>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <center>
                                    <form class="form-1">
                                        <div class="form-field">
                                            <select id="glsr-ltr" class="star-rating">
                                            @for($i = 1; $i <= $companies->estrellas; $i++)
                                                <option selected value="{{$i}}">{{$i}}</option>
                                            @endfor
                                            </select>
                                        </div>
                                    </form>
                                    <h1 class="description-company text-center">Opiniones de usuarios</h1>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="description-company-detail text-center mt-4">{!!$companies->description!!}</div>
                </div>
                @if($companies->nivel_seguridad)
                <div class="container">
                    <div class="row">
                        <div class="container">
                            <center>
                                <img src="{{URL::asset('img/alarmalia/somosindependientes.png') }}" />
                                <h1 class="title-item text-center">Nivel de Seguridad</h1>
                            </center>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->nivel_seguridad!!}</div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="accordion mx-auto" aria-multiselectable="true" style="width: 100%;" id="accordionExample">
                  @if(!(\App\Models\Component::where('company_id', '=',$companies->id)->get()->isEmpty()))                    
                    <div class="card mx-auto" style="width: 100%;">
                        <div class="card-header" id="headingOne" style="width: 100%;">
                            <div class="container" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                <center>
                                    <img src="{{URL::asset('img/icon/group.png') }}" /><br /><br />
                                    <h1 class="title-item text-center">Componentes Básicos</h1>
                                </center>
                            </div>
                        </div>

                        <div id="collapse1" class="collapse multi-collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="accordion mx-auto" aria-multiselectable="true" style="width: 100%;" id="sub-acordion">
                                    <div class="card mx-auto" style="width: 100%;">
                                        @foreach($components as $component)
                                            <div class="card-header" id="heading3" style="width: 100%;">
                                                <div class="container" data-toggle="collapse" data-target="#collapse{{$component->id + 16}}" aria-expanded="true" aria-controls="collapse{{$component->id + 16}}">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <span>
                                                <img  height="90rem!" src="{{URL::asset($component->icon) }}" />
                                                            </span>
                                                        </div>
                                                        <div class="col-9">
                                                            <span>
                                                                <h1 class="title-item2 text-letf mt-3">{{$component->name}}</h1>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapse{{$component->id + 16}}" class="collapse multi-collapse" aria-labelledby="heading3" data-parent="#sub-acordion">
                                                <div class="card-body">
                                                    <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px;">
			                                            @foreach( (\App\Models\ComponentCaract::where('component_id', '=',$component->id)->get()) as $component_car)
			                                            <li class="description-company">
			                                                <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$component_car->content}}
			                                            </li>
			                                            @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endforeach



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endif
                  @if(!(\App\Models\Component::where('company_id', '=',$companies->id)->get()->isEmpty()))                    
                    <div class="card mx-auto" style="width: 100%;">
                        <div class="card-header" id="headingOne" style="width: 100%;">
                            <div class="container" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                <center>
                                    <img src="{{URL::asset('img/icon/group-3.png') }}" /><br /><br />
                                    <h1 class="title-item text-center">Accesorios</h1>
                                </center>
                            </div>
                        </div>

                        <div id="collapse2" class="collapse multi-collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="accordion mx-auto" aria-multiselectable="true" style="width: 100%;" id="sub-acordion2">
                                        @foreach($components_acc as $component)
                                        <div class="card mx-auto" style="width: 100%;">
                                            <!--_______________________ Sub Acordion 1 __________________________-->
                                            <div class="card-header" id="heading7" style="width: 100%;">
                                                <div class="container" data-toggle="collapse" data-target="#collapse{{$component->id + 12}}" aria-expanded="true" aria-controls="collapse{{$component->id + 12}}">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <span>
				                                                <img  height="90rem!" src="{{URL::asset($component->icon) }}" />
                                                            </span>
                                                        </div>
                                                        <div class="col-9">
                                                            <span>
                                                                <h1 class="title-item2 text-letf mt-3">{{$component->name}}</h1>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapse{{$component->id + 12}}" class="collapse multi-collapse" aria-labelledby="heading7" data-parent="#sub-acordion2">
                                                <div class="card-body">
                                                    <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px;">
			                                            @foreach( (\App\Models\ComponentCaract::where('component_id', '=',$component->id)->get()) as $component_car)
			                                            <li class="description-company">
			                                                <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$component_car->content}}
			                                            </li>
			                                            @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                  @endif
                </div>
            </div>
        </div>

    </section>
    <!-- END COUNTER -->

    <section class="section counter mt-0" >
        <div class="container">
            <div class="row vertical-content text-center">
                @if($companies->tecnologia)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/tecnologia.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Tecnología</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->tecnologia!!}</div>
                    </div>
                </div>
                @endif
                @if($companies->diferencia)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/diferencia.png') }}" class="img-fluid" alt="">
                        </div><br><br>
                        <h1 class="title-heading text-center">Diferencia</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->diferencia!!}</div>
                    </div>
                </div>
                @endif
                @if($companies->precio)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/precio.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Precio</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->precio!!}</div>
                        <div class="row text-center">
                            <div class="col-6">
                                <p class="description-company pPrice text-center">Desde</p>
                                <p class="numbrePrice pPrice text-center">{{$companies->precio_desde}}€</p>
                                <p class="description-company pPrice text-center">al mes</p>
                            </div>
                            <div class="col-6">
                                <p class="description-company pPrice text-center">Hasta</p>
                                <p class="numbrePrice pPrice text-center">{{$companies->precio_hasta}}€</p>
                                <p class="description-company pPrice text-center">al mes</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($companies->contratar)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/comocontratar.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Como Contratar</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->contratar!!}</div>
                    </div>
                </div>
                @endif
                @if($companies->instalacion)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/instalacion.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Instalación</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->instalacion!!}</div>
                    </div>
                </div>
                @endif
                @if($companies->direccion)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/location.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Oficinas</h1>
                        <div class="description-company text-center text-white-50 mt-4">{!!$companies->direccion!!}</div>
                    </div>
                </div>
                @endif
                @if($companies->permanencia)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/permanencia.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Permanencia</h1>
                        <p class="description-company text-center text-white-50 mt-4">{!!$companies->permanencia!!}</p>
                    </div>
                </div>
                @endif
                @if($companies->baja)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/baja.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Baja</h1>
                        <p class="description-company text-center text-white-50 mt-4">{!!$companies->baja!!}</p>
                    </div>
                </div>
                @endif
                @if($companies->facturacion)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <div class="mt-4">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/facturacion.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-heading text-center">Facturación</h1>
                        <p class="description-company text-center text-white-50 mt-4">{!!$companies->facturacion!!}</p>
                    </div>
                </div>
                @endif
            </div>
        </div>


        <div class="container">
                @if(!(\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 0)->get()->isEmpty()) || !(\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 1)->get()->isEmpty()))
                    <div class="col-lg-12">
                        <h1 class="title-heading text-center">
                            <i style="color: #FC8401;" class="fas fa-check"></i>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i style="color: #614FA2" class="fas fa-times"></i>
                        </h1>
                        <br>
                        <center>
                            <h1 class="title-heading text-center">Ventajas y Desventajas</h1>
                        </center>
                    </div>
                @endif
            <div class="row">
                @if(!(\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 0)->get()->isEmpty()))
                <div class="col-lg-6">
                    <div class="p-2 text-letf mt-5">
                        <center>
                            <h1 class="title-heading text-center">PROS</h1>
                        </center>
                        <br>
                        <ul id="services-list" style="list-style-type: none;">
                            @foreach( (\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 0)->get()) as $advantage)
                            <li>
                                <i style="color: #FC8401;" class="fas fa-check"></i> {{$advantage->content}}
                            </li>
                            <br>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                @if(!(\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 1)->get()->isEmpty()))
                <div class="col-lg-6">
                    <div class="p-2 text-letf mt-5">
                        <center>
                            <h1 class="title-heading text-center">CONTRAS</h1>
                        </center>
                        <br>
                        <ul id="services-list" style="list-style-type: none;">
                            @foreach( (\App\Models\Advantage::where('company_id', '=',$companies->id)->where('advatage_disadvantage', '=', 1)->get()) as $advantage)
                            <li>
                                <i style="color: #614FA2" class="fas fa-times"></i> {{$advantage->content}}
                            </li>
                            <br>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </div>
            <br><br><br>
        </div>



    </section>

    <!-- START COUNTER -->
    @include('layouts.encuentra_tu_alarma') 
    @include('layouts.footer')

    <script type="text/javascript">
        $(document).ready(function() {
            if (window.innerWidth > 575) {
                $('#desktopCompanyDetail').show();
                $('#mobileCompanyDetail').hide();
            } else {
                $('#desktopCompanyDetail').hide();
                $('#mobileCompanyDetail').show();
            }
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#desktopCompanyDetail').show();
                    $('#mobileCompanyDetail').hide();
                } else {
                    $('#desktopCompanyDetail').hide();
                    $('#mobileCompanyDetail').show();
                }
            };
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });

        var starratings = new StarRating('.star-rating', {
            clearable: false,
            initialText: "Select a Rating",
            maxStars: 5,
            showText: false,
            onClick: function(el) {
                console.log('Selected: ' + el[el.selectedIndex].text);
            },
        });
    </script>
</body>

</html>