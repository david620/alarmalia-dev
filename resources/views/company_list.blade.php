<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title>Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
  <meta name="keywords" content="alarma, tyco alarmas, prosegur alarmas, vigilante" />
  <meta content="RkPeople" name="author" />
  <!-- favicon -->
  <link rel="shortcut icon" href="{{URL::asset('img/favicon.png')}}">

  <!-- css -->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('fonts/css/all.css') }}" rel="stylesheet">

  <!--Slider-->
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('css/styleFrank.css') }}" />

  <!-- Swiper CSS -->
  <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129577938-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-129577938-1');
  </script>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-M77GWGH');
  </script>
  <!-- End Google Tag Manager -->
  <style type="text/css">
    .carousel-caption {
      top: 60%;
      transform: translateY(-50%);
      bottom: initial;
      left: 60%;
      -webkit-transform-style: preserve-3d;
      -moz-transform-style: preserve-3d;
      transform-style: preserve-3d;
    }

    /*  bhoechie tab */
    div.bhoechie-tab-container {
      z-index: 10;
      background-color: #ffffff;
      padding: 0 !important;
      border-radius: 4px;
      -moz-border-radius: 4px;
      border: 1px solid #ddd;
      margin-top: 20px;
      margin-left: 50px;
      -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
      box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
      -moz-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
      background-clip: padding-box;
      opacity: 0.97;
      filter: alpha(opacity=97);
    }

    div.bhoechie-tab-menu {
      padding-right: 0;
      padding-left: 0;
      padding-bottom: 0;
    }

    div.bhoechie-tab-menu div.list-group {
      margin-bottom: 0;
    }

    div.bhoechie-tab-menu div.list-group>a {
      margin-bottom: 0;
    }

    div.bhoechie-tab-menu div.list-group>a .glyphicon,
    div.bhoechie-tab-menu div.list-group>a .fa {
      color: #EFF5F8;
    }

    div.bhoechie-tab-menu div.list-group>a:first-child {
      border-top-right-radius: 0;
      -moz-border-top-right-radius: 0;
    }

    div.bhoechie-tab-menu div.list-group>a:last-child {
      border-bottom-right-radius: 0;
      -moz-border-bottom-right-radius: 0;
    }

    div.bhoechie-tab-menu div.list-group>a.active,
    div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
    div.bhoechie-tab-menu div.list-group>a.active .fa {
      background-color: #EFF5F8;
      background-image: #EFF5F8;
      color: #ffffff;
    }

    div.bhoechie-tab-menu div.list-group>a.active:after {
      content: '';
      position: absolute;
      left: 100%;
      top: 50%;
      margin-top: -13px;
      border-left: 0;
      border-bottom: 13px solid transparent;
      border-top: 13px solid transparent;
      border-left: 10px solid #EFF5F8;
    }

    div.bhoechie-tab-content {
      background-color: #ffffff;
      padding-left: 20px;
      padding-top: 10px;
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active) {
      display: none;
    }
    .imgdimension{
    display: flex;
    justify-content: center;
    align-content: center;
}
  </style>


</head>

<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 2) )
        @include('layouts.modal_flotante')
    @endif
@endif
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M77GWGH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!--Navbar Start-->
  @include('layouts.header')
  <!-- Navbar End -->
    <!-- HOME de frank 
  <section>
    <div id="carouselExampleControls" class="carousel slide">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}" alt="First slide">
          <div class="carousel-caption">
            <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
            <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  END HOME -->
 
@include('layouts.encabezado_fijo')


  <!-- START COUNTER -->
  <section id="desktopCompany" class="section counter my-0 py-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <br><br>
          <h2 class="title-heading text-center">Mejores empresas de alarma</h2>
          <p class="title-desc text-center text-white-50 mt-4">Encuentra las principales empresas de alarmas y conoce todas sus características, ventajas y desventajas.</p>
          <br><br><br>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bhoechie-tab-menu">
          <div class="list-group">
            @foreach($companies as $company)
            <a href="#" class="list-group-item text-left imgdimension">
              <br />
              <center><img class="d-block " style="max-height: 70px; width: 100%;  min-width: 240px; max-width: 240px;" src="{{URL::asset($company->link_image)}}"></center>
            </a>
            @endforeach
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bhoechie-tab">
          <!-- flight section -->
          @foreach($companies as $company)
          <div class="bhoechie-tab-content" style="margin-top: 0;">
            <center>
              <h1 style="padding-inline-start: 40px;" class="title-company text-left">{{$company->name}}</h1>
            </center>
            <div style="padding-inline-start: 40px;" class="description-company text-left text-white-50 mt-4">{!!$company->description!!}</div>
            <br>
    	          @if(!(\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 0)->get()->isEmpty()))
			            <h1 style="padding-inline-start: 40px;" class="title-company text-left">PROS</h1>
	              @endif

            <ul id="services-list" style="list-style-type: none;">
              @foreach( (\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 0)->get()) as $advantage)
              <li class="description-company">
                <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$advantage->content}}
              </li>
              <br>
              @endforeach
            </ul>

    	          @if(!(\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 1)->get()->isEmpty()))
		             <h1 style="padding-inline-start: 40px;" class="title-company text-left">CONTRAS</h1>
	              @endif
            <ul id="services-list" style="list-style-type: none;">
              @foreach( (\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 1)->get()) as $advantage)
              <li class="description-company">
                <i style="color: #614FA2" class="fas fa-times"></i> {{$advantage->content}}
              </li>
              <br>
              @endforeach
            </ul>
            <div class="mt-5" style="padding-inline-start: 40px;" align="left">
              <a href="{{ route('companies.profile', $company->id) }}" class="btn btn-round btn-morado">Más Información</a>
            </div>
          </div>
          @endforeach

        </div>

      </div>
    </div>
    <br><br><br>

  </section>

  <section id="mobileCompany" class="my-0 py-0">
    <div class="container" style="background-color: #EFF5F8;">
      <div class="row">
        <div class="col-lg-12">
          <br>
          <h1 class="title-heading text-center" style="color: #2E3A52;">Mejores empresas de alarma</h1>
          <p class="title-desc text-center text-white-50 mt-4">Encuentra las principales empresas de alarmas y conoce todas sus características, ventajas y desventajas.</p>
          <br>
        </div>
      </div>
    </div>
    @foreach($companies as $company)
    <div class="accordion mx-auto" style="width: 100%;" id="accordionExample">
      <div class="card mx-auto" style="width: 100%; border:none">
        <div class="card-header" id="headingOne" style="width: 100%;">
          <div class="container" data-toggle="collapse" data-target="#collapse{{ $company->id }}" aria-expanded="true" aria-controls="collapse{{ $company->id }}">
            <center>
              <img style="max-height: 80px;width: 70%; min-width: 70%;" src="{{URL::asset($company->link_image)}}">
              <h3 class="description-company-mobile text text-center">{{$company->name}}</h3>
            </center>
          </div>
        </div>

        <div id="collapse{{ $company->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
            <div class="bhoechie-tab-content" style="margin-top: 0;">
              <div class="description-company-mobile text-center text-white-50 mt-4">{!!$company->description!!}</div>
              <br>
    	          @if(!(\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 0)->get()->isEmpty()))
        		      <h1 class="title-item text-center">PROS</h1>
	              @endif
              <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px !important;">
                @foreach( (\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 0)->get()) as $advantage)
                <li class="description-company">
                  <i style="color: #7AB3FD;" class="fas fa-check"></i> {{$advantage->content}}
                </li>
                <br>
                @endforeach
              </ul>
    	          @if(!(\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 1)->get()->isEmpty()))
        		      <h1 class="title-item text-center">CONTRAS</h1>
	              @endif

              <ul id="services-list" style="list-style-type: none; padding-inline-start: 0px !important;">
                @foreach( (\App\Models\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 1)->get()) as $advantage)
                <li class="description-company">
                  <i style="color: #614FA2" class="fas fa-times"></i> {{$advantage->content}}
                </li>
                <br>
                @endforeach
              </ul>
              <div class="mt-5" align="center">
                <a href="{{ route('companies.profile', $company->id) }}" class="btn btn-round btn-morado">Más Información</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </section>

@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 2) )
        @include('layouts.modal_fijo')
    @endif
@endif
  @include('layouts.encuentra_tu_alarma')
  <div style="margin-top: 1rem !important;">
    @include('layouts.alarm_provider')
  </div>
  @include('layouts.footer')

  <script type="text/javascript">

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 750) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 750) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });



    $(document).ready(function() {
      $("div.bhoechie-tab>div.bhoechie-tab-content").eq(0).addClass("active");
      $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
      });

      window.onresize = function() {
        if (window.innerWidth > 750) {
          $('#desktopCompany').show();
          $('#mobileCompany').hide();
        } else {
          $('#desktopCompany').hide();
          $('#mobileCompany').show();
        }
      };
      if (window.innerWidth > 750) {
        $('#desktopCompany').show();
        $('#mobileCompany').hide();
      } else {
        $('#desktopCompany').hide();
        $('#mobileCompany').show();
      }
    });
  </script>
</body>

</html>