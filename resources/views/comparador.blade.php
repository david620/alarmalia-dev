<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>


    @include('layouts.header')

    <br>

    <section>
    </section>

    <section class="cliente-logo pt-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-1"></div>
                <div class="col-10">
                    <h3 class="title-headin">¡ Falta poco para saber sobre tu alarma ideal! </h3>

                    <br>
                    <div style="width: 100%;" class="progress">
                        <div class="progress-bar" style="height: 22px; background-color: #614fa2; width:{{$porcentaje}}%;">
                            <span></span>
                            <div class="progress-value"><span><img src="{{URL::asset('img/alarmalia/claro.png') }}" alt="" height="100%" width="100%"></span></div>
                        </div>
                    </div>

                                <h3 class="title-headin">{{$porcentaje}} % Completado </h3>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row justify-content-center mt-0">
                <div class="col-lg-8">
                    {!! Form::open(array('route' => 'comparator.store','method'=>'POST','id'=>'form-pregunta' )) !!}
                    {{ csrf_field() }}
                    <div class="col-lg-12">
                        <h4 class="title-heading">{{$first_question->title}}</h4>

                    </div>
                        <div class="col-lg-12" id="quiz">
                            @foreach($answers as $answer)
                                @if($answer->answer_type == 4)                        
                                    {!! Form::text('content', null, array('placeholder' => 'Respuesta','class' => 'form-control')) !!} <button type="submit" class="btn btn-roundes">ENVIAR</button>
                                        <input type="hidden" name="q_answer" value="{{$answer->id}}">
                                        <input type="hidden" name="next_question" value="{{$answer->next_question}}">
                                @elseif($answer->answer_type == 1)
                                    {!! Form::text('content', null, array('placeholder' => 'Respuesta','class' => 'form-control')) !!} <button type="submit" class="btn btn-roundes">ENVIAR</button>
                                        <input type="hidden" name="q_answer" value="{{$answer->id}}">
                                        <input type="hidden" name="next_question" value="{{$answer->next_question}}">
                                @elseif($answer->answer_type == 3)
                                    {!! Form::text('content', null, array('placeholder' => 'Número Telefónico','class' => 'form-control')) !!} <button type="submit" class="btn btn-roundes">ENVIAR</button>
                                        <input type="hidden" name="q_answer" value="{{$answer->id}}">
                                        <input type="hidden" name="next_question" value="{{$answer->next_question}}">
                                @elseif($answer->answer_type == 2)
                                    {!! Form::text('content', null, array('placeholder' => 'E-mail','class' => 'form-control')) !!} <button type="submit" class="btn btn-roundes">ENVIAR</button>
                                        <input type="hidden" name="q_answer" value="{{$answer->id}}">
                                        <input type="hidden" name="next_question" value="{{$answer->next_question}}">
                                @else
                                   <a class="sm2_link">
                                        <label class="element-animation2 btn-block" style="color: #2E3A52;">
                                            <button name="q_answer" onclick="changeColor(this);" style="color: #9DABC8; background: #EFF5F8" value="{{$answer->id}}" type="input" class="btn btn-default btn-circle btn-lg">{{$alfabeto[$loop->index]}} 
                                            </button> {{$answer->content}}
                                        </label>
                                    </a>

                                @endif
                            @endforeach
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    @include('layouts.footer')

    <script type="text/javascript">
        $('.sm2_link').on('click', function() {
            this.parentNode.click()
        });

        function changeColor(x) {
            x.style.background = "#FC8400";
            x.style.color = "#fff";
            $('#form-pregunta').submit();
        }
    </script>
    <script type="text/javascript">
        
        function checkanswer() {
            
            if ($("input:radio[name=q_answer]:checked").val()=="" || $("input:radio[name=q_answer]:checked").val()==null) {
                $('#texto').text("Elige una respuesta, por favor");
                $('#overlay').show();
                return false;
            }
 
            $('#form-pregunta').submit();
        } // END checkanswer

        $('#close').click(function() {
             $('#texto').empty();
             $('#overlay').fadeOut();
        });

        $('.respuesta_texto').keydown(function(e){
            // Enter o tabulador
            if (e.which==13 || e.which==9) {e.preventDefault()}
        });
    </script>

</body>

</html>