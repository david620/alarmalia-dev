<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 9) )
        @include('layouts.modal_flotante')
    @endif
@endif
  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->
    
@include('layouts.encabezado_fijo')
<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-7">
                    <div class="col-lg-12">
                    <h4 class="text-center">Contacto</h4>
                    <p class="title-desc text-center text-white-50 mt-4" ></p>
                </div>
                    <div class="custom-form mt-3">
                        <div id="message"></div>
                            {!! Form::open(array('route' => 'contact.store','method'=>'POST')) !!}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                    <label style="font-family: Archivo; font-size: 16px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;">Nombre: </label>
                                        {!! Form::text('name', null, array('class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                    <label style="font-family: Archivo; font-size: 16px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;">Apellido: </label>
                                        {!! Form::text('lastname', null, array('class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                    <label style="font-family: Archivo; font-size: 16px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;">Móvil: </label>
                                        {!! Form::text('phone', null, array('class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                    <label style="font-family: Archivo; font-size: 16px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;">Mensaje: </label>
                                        {!! Form::textarea('message', null, array('class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                    <div class="checker" id="uniform-customer_privacy">
                                      <input type="checkbox" value="0" required  name="terms" autocomplete="off"> <a class="f-18" style="  width: 379px; height: 28px; font-family: Archivo; font-size: 18px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;" href="{{ route('companies.policies') }}"><b> He leído y acepto la política de privacidad</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <input style="  width: 224px; height: 48px; border-radius: 24px; background-color: #614fa2;" id="submit" name="send" class="submitBnt btn btn-secondary btn-round" value="ENVIAR" type="submit">
                                    <div id="simple-msg"></div>
                                </div>
                            </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 9) )
        @include('layouts.modal_fijo')
    @endif
@endif 
@include('layouts.encuentra_tu_alarma')
@include('layouts.footer')
    <script type="text/javascript">

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });
    </script>
</body>

</html>