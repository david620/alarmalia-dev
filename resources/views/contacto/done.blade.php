<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

@include('layouts.encabezado_fijo')

 
<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <center>
                        <img src="{{URL::asset('img/unnamed.png') }}" alt="">
                        </center>
                        <br><br>
                        <center>
                        <div class="col-lg-6 center">
                    <h4 class="title-heading">¡BIEN HECHO!<br>
                En breve nos pondremos en contacto con usted.</h4>
            </div>
            </center>
            
               
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

     <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
            <p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->

@include('layouts.footer')
</body>

</html>