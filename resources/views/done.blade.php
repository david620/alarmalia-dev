<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

    <section class="bg-home-1" id="home">
        <div class="home-bg-contacto"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">
                        <div class="col-lg-7">
<!-- 
                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                                <img src="{{URL::asset('img/banner.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
-->                       
                        </div>                            
                         <div class="col-lg-5">
                            <div class="home-content">
                                <h3 class="home-title" style="font-family: Poppins; font-size: 46px; font-weight: 600; font-stretch: normal; font-style: italic line-height: 1.14; letter-spacing: normal; text-align: justify; color: #614fa2;">Tu comparador de alarmas de casa y negocio</h3>
                                
                                <div class="mt-5">
                                    <center>
                                    <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </center>
                                </div>
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </section>


 
<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <center>
                        <img src="{{URL::asset('img/unnamed.png') }}" alt="">
                        </center>
                        <br><br>
                        <center>
                        <div class="col-lg-6 center">
                    <h4 class="title-heading">¡BIEN HECHO!<br>
                En breve nos pondremos en contacto con tu empresa para explicarte cómo formar parte de nuestro comparador.</h4>
            </div>
            </center>
            
               
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

@include('layouts.encuentra_tu_alarma')
@include('layouts.footer')
</body>

</html>