<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>

    <!--Navbar Start-->
    @include('layouts.header')
    <!-- Navbar End -->

    <section class="bg-home-1" id="comparator">
        <div class="home-bg-comparador-final"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">
                        <div class="col-lg-3">

                        </div>
                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                                <img src="{{URL::asset('img/imgComparador.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-3">

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section counter my-0 py-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <br><br>
                    <h1 class="title-itemGlosary text-center" >Buenas Noticias!</h1>
                    <p class="title-company text-center mt-4" >Hemos encontrado {{count($contracts)}} proveedores de alarmas que se ajustan a tus necesidades. A continuación puedes ver la comparativa de precios y servicios.<br><br> Esta comparativa se ha hecho teniendo en cuenta tu localidad, las características de tu vivienda o negocio y buscando el mejor precio para que ahorres con la mejor opción. Ten en cuenta que el precio que se muestra a continuación es el de la cuota mensual.</p>
                </div>
            </div>
        </div>
    </section>



    <section class="section my-0 py-0" id="contact">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-4 col-sm-0 col-xs-0">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<!--
                    <label class="title-company" for="inputGroupSelect01">Clasificar por:</label>
                    <select class="custom-select selectComparator" name="inputGroupSelect01" id="inputGroupSelect01">
                        <option selected>Choose...</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-0 col-xs-0">
                </div>
                    @foreach($contracts as $contract)
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="card cardComparator">
                        <div class="card-body mx-0 px-0">
                            <div class="card-title">
                                <center>
                                    <img class="d-block " width="200px" height="80px" src="{{URL::asset($contract->link_image)}}">
                                    <form class="form-1">
                                        <div class="form-field">
                                            <select id="glsr-ltr" class="star-rating">
                                            @for($i = 1; $i <= $contract->estrellas; $i++)
                                                <option selected value="{{$i}}">{{$i}}</option>
                                            @endfor
                                            </select>
                                        </div>
                                    </form>
                                    <h1 class="pLlamada text-center"><u>Opiniones de usuarios</u></h1>

                                </center>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-4 text-center my-0 py-0">
                                        <img class="d-block mx-auto" height="50%" src="{{URL::asset('img/icon/antenna-1.png')}}">
                                        <p class="pIconCard mx-auto">Radio</p>
                                    </div>
                                    <div class="col-4 text-center my-0 py-0">
                                        <img class="d-block mx-auto" height="50%" src="{{URL::asset('img/icon/security-1.png')}}">
                                        <p class="pIconCard mx-auto">Vigilante</p>
                                    </div>
                                    <div class="col-4 text-center my-0 py-0">
                                        <img class="d-block mx-auto" height="50%" src="{{URL::asset('img/icon/Group5-1.png')}}">
                                        <p class="pIconCard mx-auto">Conexión GPRS</p>
                                    </div>
                                    <div class="col-12 text-center my-0 py-0">
                                        <a class="mx-auto" style="color: #2E3A52; font-family: Archivo;font-size: 14px;" href="{{route('companies.profile', $contract->client_id)}}"><u>Ver más +</u></a>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="container mx-0 px-0">
                                <div class="row mx-0 px-0 lineaCard">
                                    <span class="btn-circle btn-xl text-center circleCard">
                                        <p class="precioCard pPrice">Desde</p>
                                        <p class="precioCard pPrice" style="font-size: 35px;">{{$contract->precio_desde}}€</p>
                                        <p class="precioCard pPrice">al mes</p>
                                    </span>
                                </div>
                            </div>
                            <br><br>
                            <div class="container">
                                <div class="row text-center">
                                    <a class="btn btn-secondary btn-sm btn-round mx-auto" href="{{$contract->url_web}}" target="_blank">IR AL SITIO WEB</a>
                                    <br><br><br>
                                    <p class="pLlamada mx-auto">Te llamarán por teléfono</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
    </section>


    <section class="section bg-light" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 style=" font-family: Archivo; font-size: 20px; font-weight: 500; font-stretch: normal; font-style: normal; line-height: 1.4; letter-spacing: normal; text-align: center; color: #565656;">Además te ofrecemos de forma gratuita una guía para elegir la mejor alarma para tu casa o negocio.</h1>

                    <div class="mt-5" align="center">
                        <a style="  width: 224px; height: 48px; border-radius: 24px; background-color: #614fa2;" href="{{URL::asset('guia/guia-seleccion-de-alarmas.pdf') }}" target="_blank" class="btn btn-custom btn-round">DESCARGAR AHORA</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var starratings = new StarRating('.star-rating', {
            clearable: false,
            initialText: "Select a Rating",
            maxStars: 5,
            showText: false,
            onClick: function(el) {
                console.log('Selected: ' + el[el.selectedIndex].text);
            },
        });
    </script>
    @include('layouts.footer')
</body>

</html>