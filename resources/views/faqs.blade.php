<!DOCTYPE html>
<html lang="es">

@include('layouts.head')

<style type="text/css">
    .main-content {
        position: relative;
    }

    .main-content .owl-theme .custom-nav {
        position: absolute;
        top: 20%;
        left: 0;
        right: 0;
    }

    .main-content .owl-theme .custom-nav .owl-prev,
    .main-content .owl-theme .custom-nav .owl-next {
        position: absolute;
        color: inherit;
        background: none;
        border: none;
        z-index: 100;
    }

    .main-content .owl-theme .custom-nav .owl-prev i,
    .main-content .owl-theme .custom-nav .owl-next i {
        font-size: 2.5rem;
        color: #cecece;
    }

    .main-content .owl-theme .custom-nav .owl-prev {
        left: 0;
    }

    .main-content .owl-theme .custom-nav .owl-next {
        right: 0;
    }

    .carousel-caption {
        top: 60%;
        transform: translateY(-50%);
        bottom: initial;
        left: 60%;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        transform-style: preserve-3d;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #2E3A52;
        font-family: Poppins;
        font-size: 24px;
        font-style: italic;
        font-weight: 600;
        line-height: 35px;
        text-align: center;
        background: #EFF5F8;
        padding: .2rem 0rem;
        border-bottom: 0px transparent;
    }

    .nav-pills .nav-link {
        color: #2E3A52;
        font-family: Poppins;
        font-size: 24px;
        font-style: italic;
        font-weight: 600;
        line-height: 35px;
        text-align: center;
        padding: .2rem 0rem;
        border-bottom: 1px solid #D2D2D2;
    }

    .descripcionG {
        color: #565656;
        font-family: Archivo;
        font-size: 20px;
        font-weight: 500;
        line-height: 28px;
        margin: 20px;
        overflow-wrap: break-word;
    }

    .owl-item.active.center>div {
        background-color: #FFFFFF;
        box-shadow: 5px 5px 10px #9DABC8;
    }

    @media screen and (max-width: 575px) {
        .owl-item.active.center>div {
            background-color: #FFFFFF;
            box-shadow: 5px 5px 10px #9DABC8;
        }
    }
</style>

<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 5) )
        @include('layouts.modal_flotante')
    @endif
@endif
    <!--Navbar Start-->
    @include('layouts.header')
    <!-- Navbar End -->

    @include('layouts.encabezado_fijo')
    

    <!--   <section class="bg-home-1" id="home">
        <div class="home-bg-anunciate"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">
                         <div class="col-lg-5">
                            <div class="home-content">
                                <h3 class="home-title" style="font-family: Poppins; font-size: 46px; font-weight: 600; font-stretch: normal; font-style: italic line-height: 1.14; letter-spacing: normal; text-align: justify; color: #614fa2;">Tu comparador de alarmas de casa y negocio</h3>
                                
                                <div class="mt-5">
                                    <center>
                                    <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">

                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                                <img src="{{URL::asset('img/banner.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                    
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>-->


    <section id="desktopFaqs" class="section counter" style="padding-top:0px;">
        <div class="container">
            <div class="row">
                <dic class="col-12">
                    <h1 class="title-itemGlosary">Preguntas Frecuentes</h1>
                </dic>
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="main-content">
                        <div class="owl-carousel owl-theme">
                            @foreach($categories as $categorie)
                            <div class="item text-center" data-hash="{{$loop->index}}" style="text-align: center;">
                                <a id="item{{$loop->index}}" onclick="changeItem('{{$loop->index}}')" href="#tabsNavigationSimpleIcons{{$loop->index}}" data-toggle="tab">
                                    <img class="img-responsive img-rounded mx-auto" src="{{$categorie->category_icon}}" href="#tabsNavigationSimpleIcons{{$loop->index}}" data-toggle="tab" style="height:100% !important;width: auto !important;">
                                    <h3 class="title-headin text-center mt-2">{{ $categorie->category_name }}</h3>
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <div class="owl-theme">
                            <div class="owl-controls">
                                <div class="custom-nav owl-nav"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
        <div class="tab-content">
            @foreach($categories as $categorie)
            <div class="tab-pane" id="tabsNavigationSimpleIcons{{$loop->index}}">
                <div class="center" style="margin-top: -50px;">
                    <div id="tabDesktop{{$loop->index}}" class="container">
                        <div class="row">
                            <div class="container">
                                <div class="row reduce">
                                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 my-3">
                                        <ul class="nav nav-pills flex-column rectangulo" id="myTab{{$loop->index}}" role="tablist">
                                            @foreach( (\App\Models\Faq::where('faq_category_id', '=',$categorie->id)->get()) as $faq)
                                            <li class="nav-item">
                                                <a class="nav-link" id="home-tab{{$loop->index}}{{$faq->id}}" data-toggle="tab" href="#home{{$loop->index}}{{$faq->id}}" role="tab" aria-controls="home{{$loop->index}}{{$faq->id}}" aria-selected="false" style=" font-family: Poppins; font-size: 18px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: normal; letter-spacing: normal; text-align: center; color: #2e3a52;">
                                                    {{$faq->question}}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- /.col-md-4 -->
                                    <div class="col-lg-6 col-md-6 col-sm-7 col-xs-7 mx-auto reduce">
                                        <div class="tab-content" id="myTabContent{{$loop->index}}">
                                            @foreach( (\App\Models\Faq::where('faq_category_id', '=',$categorie->id)->get()) as $faq)
                                            <div class="tab-pane fade" id="home{{$loop->index}}{{$faq->id}}" role="tabpanel" aria-labelledby="home-tab{{$loop->index}}{{$faq->id}}">
                                                <p class="descripcionG text-letf">{{$faq->content}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- /.col-md-8 -->
                                </div>
                            </div>
                            <!-- /.container -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </section>

    <!-- START COUNTER MOBILE-->
    <section id="mobileFaqs" class="section counter">
        <div class="container">
            <div class="row">
                <dic class="col-12">
                    <h1 class="title-itemGlosary">Preguntas Frecuentes</h1>
                </dic>
                <div class="accordion mx-auto" aria-multiselectable="true" style="width: 100%;" id="accordionExample">
                    @foreach($categories as $categorie)
                    <div class="card mx-auto" style="width: 100%;">
                        <div class="card-header" id="headingOne" style="width: 100%;">
                            <div class="container" data-toggle="collapse" data-target="#collapse{{$categorie->id}}" aria-expanded="true" aria-controls="collapse{{$categorie->id}}">
                                <div class="row">
                                    <div class="col-4" style="text-align: center;">
                                        <img  style="max-height:55px" class="img-responsive img-rounded" src="{{$categorie->category_icon}}">
                                    </div>
                                    <div class="col-8" style="margin: auto;">
                                        <h1 style="text-align: left" class="title-item">{{ $categorie->category_name }} </h1>
                                    </div>
                                </div>
                            </div>

                            <div id="collapse{{$categorie->id}}" class="collapse multi-collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="accordion mx-auto" aria-multiselectable="true" style="width: 100%;" id="sub-acordion{{$categorie->id}}">
                                        <div class="card mx-auto" style="width: 100%;">
                                            @foreach( (\App\Models\Faq::where('faq_category_id', '=',$categorie->id)->get()) as $faq)
                                            <!--_______________________ Sub Acordion 1 __________________________-->
                                            <div class="card-header py-0" id="heading{{$categorie->id}}{{$loop->index}}" style="width: 100%;">
                                                <div class="container py-0" data-toggle="collapse" data-target="#collapse{{$categorie->id}}{{$loop->index}}" aria-expanded="true" aria-controls="collapse{{$categorie->id}}{{$loop->index}}">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h1 class="description-company-mobile text-left">{{$faq->question}}</h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapse{{$categorie->id}}{{$loop->index}}" class="collapse multi-collapse" aria-labelledby="heading{{$categorie->id}}{{$loop->index}}" data-parent="#sub-acordion{{$categorie->id}}">
                                                <div class="card-body">
                                                    <p class="description-company-mobile text-left">{{$faq->content}}</p>
                                                </div>
                                            </div>
                                            <!--_______________________________________________________-->
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

    </section>
    <!-- END COUNTER -->

    <!-- START COUNTER -->
    <section class="section counter footerMobile">
        <div class="container ampliar" style="background-color: #F3F1F8;">
            <div class="col-lg-12" class="row mt-5" id="counter">
                <h4 class="tittle-img" style="color: #614FA2;">¿Eres un proveedor de alarmas?</h4>
                <center>
                    <div class="mt-2">
                        <a href="" class="btn btn-custom btn-round btn-transparent">Anuciarme</a>
                    </div>
                </center>
            </div>

        </div>
    </section> <!-- END COUNTER -->
    <!-- mostrar solo mobil -->
    <!-- START COUNTER -->
    <section class="section counter footerMobile">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                <h4 class="tittle-img" style="color: #614FA2;">¿No has resuelto tus dudas?</h4>
                <p class="parrafo mt-2">¡ Ponte en contacto con nosotros!</p>

                <center>
                    <div class="mt-2">
                        <a href="" class="btn btn-custom " style="background-color: #614FA2 !important; width: 220px !important;">Contactar</a>
                    </div>
                    <br><br>
                </center>
            </div>

        </div>
    </section> <!-- END COUNTER -->
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 5) )
        @include('layouts.modal_fijo')
    @endif
@endif 
    </div>
    <script>

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });

        function changeItem(item) {
            var url = window.location.href;
            url = url.split('#');
            window.location.href = url[0] + '#' + item;
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#desktopFaqs').show();
                    $('#mobileFaqs').hide();
                } else {
                    $('#desktopFaqs').hide();
                    $('#mobileFaqs').show();
                }
            }
            if (window.innerWidth > 575) {
                $('#desktopFaqs').show();
                $('#mobileFaqs').hide();
            } else {
                $('#desktopFaqs').hide();
                $('#mobileFaqs').show();
            }
            var owl = $('.owl-carousel');
            owl.on('changed.owl.carousel',
                function(e) {
                    const itemSelect = $('#item' + e.relatedTarget._current);
                    if (itemSelect.length > 0) {
                        itemSelect[0].click();
                    }
                    const activo = $($("#myTab" + (e.relatedTarget._current)));
                    if (activo.length > 0) {
                        const activoH1 = $(activo[0].children[0]);
                        if (activoH1.length > 0) {
                            const activoH2 = $(activoH1[0].children[0]);
                            if (activoH2.length > 0) {
                                activoH2[0].click();
                            }
                        }
                    }
                });

            $('.main-content .owl-carousel').owlCarousel({
                center: true,
                items: 1,
                loop: false,
                margin: 10,
                dots: false,
                nav: true,
                navText: [
                    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    '<i class="fa fa-angle-right" aria-hidden="true"></i>'
                ],
                navContainer: '.main-content .custom-nav',
                URLhashListener: true,
                responsive: {
                    800: {
                        items: 4
                    }
                }
            });
            const itemSelect = $('#item0').click();
            if (itemSelect.length > 0) {
                itemSelect[0].click();
            }
        });
    </script>

    @include('layouts.encuentra_tu_alarma')

    @include('layouts.footer')

</body>

</html>