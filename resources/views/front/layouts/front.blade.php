<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es">

@include('front.parts.head')

<body>
<header class="d-flex flex-row align-items-center @yield('header-class')">
    @include('front.parts.header')
</header>
<div id="container">
    @yield('content')
</div>
<footer>
    @include('front.parts.footer')
</footer>
@include('front.parts.popup')

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
@if(Auth::user() && Auth::user()->active && Entrust::can('manage_translates') || Entrust::can('manage_images')) {!! Html::script('js/front-admin.js') !!} @endif

{{-- IMPORTANTE: Si se añade un archivo aquí, se tiene que poner también en el array específico en gulpfile.js para poder minificarlos --}}
@if(File::exists('js/front-end.min.js') && File::exists('js/front-end-plugins.min.js'))
    {!! Html::script('js/front-end-plugins.min.js?v=1') !!}
    {!! Html::script('js/front-end.min.js?v=1') !!}
@else
    {!! Html::script('js/plugins/bootstrap/bootstrap.min.js') !!}
    {!! Html::script('js/plugins/outdated-browser/outdatedbrowser.min.js') !!}
    {!! Html::script('js/plugins/cookiebar/cookiebar.min.js') !!}
    {!! Html::script('js/plugins/slick/slick.min.js') !!}
    {!! Html::script('js/front-end.js') !!}
@endif

@yield('custom-js')
@yield('schema-json')
</body>
</html>
