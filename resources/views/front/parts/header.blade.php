<div class="container-fluid d-flex flex-md-row align-items-center justify-content-md-between">
    <div class="d-flex justify-content-center justify-content-md-start flex-md-row " id="logo-container">
        <a id="logo" href="{{ route('site.home') }}">
            <img src="{{URL::asset('img/logo.png') }}">
        </a>
    </div>
    <div id="button-menu" class="d-md-none">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <nav>
        <ul class="d-flex flex-column flex-md-row align-items-center justify-content-md-between">
            <li>
                <a href="{{ route('companies.list')}}">COMPAÑÍAS</a>
            </li>
            <li>
                <a href="{{ route('companies.homealarms')}}">ALARMAS PARA CASA</a>
            </li>
            <li>
                <a href="{{ route('companies.business_alarms')}}">ALARMAS PARA NEGOCIO</a>
            </li>
            <li>
                <a href="{{ route('companies.questions')}}">FAQS</a>
            </li>
            <li>
                <a href="{{ route('companies.blog')}}">BLOG</a>
            </li>
        </ul>
    </nav>
</div>
