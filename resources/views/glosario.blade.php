<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title>Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
    <meta name="keywords" content="alarma, tyco alarmas, prosegur alarmas, vigilante" />
    <meta content="RkPeople" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{URL::asset('img/favicon.png')}}">

    <!-- css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('js/carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/carousel/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}" />
    <script src="{{ asset('js/carousel/jquery.min.js') }}"></script>
    <script src="{{ asset('js/carousel/owl.carousel.js') }}"></script>
    <link href="{{ asset('fonts/css/all.css') }}" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleFrank.css') }}" />

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">


    <link rel="stylesheet" href="{{ asset('css/circulos.css') }}">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129577938-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-129577938-1');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M77GWGH');
    </script>
    <!-- End Google Tag Manager -->

    <style type="text/css">
        .main-content {
            position: relative;
        }

        .main-content .owl-theme .custom-nav {
            position: absolute;
            top: 20%;
            left: 0;
            right: 0;
        }

        .main-content .owl-theme .custom-nav .owl-prev,
        .main-content .owl-theme .custom-nav .owl-next {
            position: absolute;
            color: inherit;
            background: none;
            border: none;
            z-index: 100;
        }

        .main-content .owl-theme .custom-nav .owl-prev i,
        .main-content .owl-theme .custom-nav .owl-next i {
            font-size: 2.5rem;
            color: #cecece;
        }

        .main-content .owl-theme .custom-nav .owl-prev {
            left: 0;
        }

        .main-content .owl-theme .custom-nav .owl-next {
            right: 0;
        }

        .carousel-caption {
            top: 60%;
            transform: translateY(-50%);
            bottom: initial;
            left: 60%;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #2E3A52;
            font-family: Poppins;
            font-size: 24px;
            font-style: italic;
            font-weight: 600;
            line-height: 35px;
            text-align: center;
            background: #EFF5F8;
            padding: .2rem 0rem;
            border-bottom: 0px transparent;
        }

        .nav-pills .nav-link {
            color: #2E3A52;
            font-family: Poppins;
            font-size: 24px;
            font-style: italic;
            font-weight: 600;
            line-height: 35px;
            text-align: center;
            padding: .2rem 0rem;
            border-bottom: 1px solid #D2D2D2;
        }

        .descripcionG {
            color: #565656;
            font-family: Archivo;
            font-size: 20px;
            font-weight: 500;
            line-height: 28px;
            margin: 20px;
            overflow-wrap: break-word;
        }

        .owl-item.active.center>div>a {
            background-color: #614FA2;
            box-shadow: 0 2px 10px 0 #614FA2;
            color: white;
        }
        .owl-carousel .owl-stage-outer{/*glosario cortado*/
            min-height: 95px;
            padding-top: 5px;
        }
        .reduce{
            margin: 0px 12%;
        }
        .rectangulo .nav-link{
            min-height: 5rem;  display: flex;
            justify-content: center;
            align-content: center;
            flex-direction: column;
        } 
        .rectangulomobile{
            min-height: 5rem;  display: flex;
            justify-content: center;
            align-content: center;
            flex-direction: column;
        } 


        @media screen and (max-width: 575px) {
            .owl-item.active.center>div>a {
                width: 80px;
                height: 80px;
                padding: 10px 16px;
                background-color: #614FA2;
                box-shadow: 0 2px 10px 0 #614FA2;
                color: white;
            }
        }
    </style>


</head>

<body>
@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 10) )
        @include('layouts.modal_flotante')
    @endif
@endif

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M77GWGH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!--Navbar Start
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark" style="position: sticky;">
        <div class="container">
            <!-- LOGO
            <a class="navbar-brand logo text-uppercase" href="{{ route('site.home') }}">
                <img src="{{URL::asset('img/logo.png') }}" class="logo-light" alt="" height="80">
                <img src="{{URL::asset('img/logo.png') }}" class="logo-dark" alt="" height="80">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item">
                        <a href="{{ route('companies.list')}}" class="nav-link">COMPAÑÍAS</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('companies.homealarms')}}" class="nav-link">ALARMAS PARA CASA</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('companies.business_alarms')}}" class="nav-link">ALARMAS PARA NEGOCIO</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('companies.questions')}}" class="nav-link">FAQS</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('companies.blog')}}" class="nav-link">BLOG</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    --> 
    @include('layouts.header')
    <!-- Navbar End -->



    <!-- START HOME -->
  
    @include('layouts.encabezado_fijo')
    <!-- END HOME -->



    <!-- START COUNTER -->
    <section class="section counter" style="padding-top:0px;">
        <div class="container">
            <div class="row">
                <dic class="col-12">
                    <h1 class="title-itemGlosary">Glosario</h1>
                </dic>
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="main-content">
                        <div class="owl-carousel owl-theme">
                            <div class="item" data-hash="A" style="text-align: center;">
                                <a id="item0" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('A')" href="#tabsNavigationSimpleIcons1" data-toggle="tab">A</a>
                            </div>
                            <div class="item" data-hash="B" style="text-align: center;">
                                <a id="item1" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('B')" href="#tabsNavigationSimpleIcons2" data-toggle="tab">B</a>
                            </div>
                            <div class="item" data-hash="C" style="text-align: center;">
                                <a id="item2" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('C')" href="#tabsNavigationSimpleIcons3" data-toggle="tab">C</a>
                            </div>
                            <div class="item" data-hash="D" style="text-align: center;">
                                <a id="item3" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('D')" href="#tabsNavigationSimpleIcons4" data-toggle="tab">D</a>
                            </div>
                            <div class="item" data-hash="E" style="text-align: center;">
                                <a id="item4" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('E')" href="#tabsNavigationSimpleIcons5" data-toggle="tab">E</a>
                            </div>
                            <div class="item" data-hash="F" style="text-align: center;">
                                <a id="item5" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('F')" href="#tabsNavigationSimpleIcons6" data-toggle="tab">F</a>
                            </div>
                            <div class="item" data-hash="G" style="text-align: center;">
                                <a id="item6" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('G')" href="#tabsNavigationSimpleIcons7" data-toggle="tab">G</a>
                            </div>
                            <div class="item" data-hash="H" style="text-align: center;">
                                <a id="item7" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('H')" href="#tabsNavigationSimpleIcons8" data-toggle="tab">H</a>
                            </div>
                            <div class="item" data-hash="I" style="text-align: center;">
                                <a id="item8" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('I')" href="#tabsNavigationSimpleIcons9" data-toggle="tab">I</a>
                            </div>
                            <div class="item" data-hash="J" style="text-align: center;">
                                <a id="item9" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('J')" href="#tabsNavigationSimpleIcons10" data-toggle="tab">J</a>
                            </div>
                            <div class="item" data-hash="K" style="text-align: center;">
                                <a id="item10" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('K')" href="#tabsNavigationSimpleIcons11" data-toggle="tab">K</a>
                            </div>
                            <div class="item" data-hash="L" style="text-align: center;">
                                <a id="item11" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('L')" href="#tabsNavigationSimpleIcons12" data-toggle="tab">L</a>
                            </div>
                            <div class="item" data-hash="M" style="text-align: center;">
                                <a id="item12" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('M')" href="#tabsNavigationSimpleIcons13" data-toggle="tab">M</a>
                            </div>
                            <div class="item" data-hash="N" style="text-align: center;">
                                <a id="item13" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('N')" href="#tabsNavigationSimpleIcons14" data-toggle="tab">N</a>
                            </div>
                            <div class="item" data-hash="2" style="text-align: center;">
                                <a id="item14" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('2')" href="#tabsNavigationSimpleIcons15" data-toggle="tab">Ñ</a>
                            </div>
                            <div class="item" data-hash="O" style="text-align: center;">
                                <a id="item15" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('O')" href="#tabsNavigationSimpleIcons16" data-toggle="tab">O</a>
                            </div>
                            <div class="item" data-hash="P" style="text-align: center;">
                                <a id="item16" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('P')" href="#tabsNavigationSimpleIcons17" data-toggle="tab">P</a>
                            </div>
                            <div class="item" data-hash="Q" style="text-align: center;">
                                <a id="item17" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('Q')" href="#tabsNavigationSimpleIcons18" data-toggle="tab">Q</a>
                            </div>
                            <div class="item" data-hash="R" style="text-align: center;">
                                <a id="item18" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('R')" href="#tabsNavigationSimpleIcons19" data-toggle="tab">R</a>
                            </div>
                            <div class="item" data-hash="S" style="text-align: center;">
                                <a id="item19" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('S')" href="#tabsNavigationSimpleIcons20" data-toggle="tab">S</a>
                            </div>
                            <div class="item" data-hash="T" style="text-align: center;">
                                <a id="item20" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('T')" href="#tabsNavigationSimpleIcons21" data-toggle="tab">T</a>
                            </div>
                            <div class="item" data-hash="U" style="text-align: center;">
                                <a id="item21" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('U')" href="#tabsNavigationSimpleIcons22" data-toggle="tab">U</a>
                            </div>
                            <div class="item" data-hash="V" style="text-align: center;">
                                <a id="item22" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('V')" href="#tabsNavigationSimpleIcons23" data-toggle="tab">V</a>
                            </div>
                            <div class="item" data-hash="W" style="text-align: center;">
                                <a id="item23" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('W')" href="#tabsNavigationSimpleIcons24" data-toggle="tab">W</a>
                            </div>
                            <div class="item" data-hash="X" style="text-align: center;">
                                <a id="item24" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('X')" href="#tabsNavigationSimpleIcons25" data-toggle="tab">X</a>
                            </div>
                            <div class="item" data-hash="Y" style="text-align: center;">
                                <a id="item25" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('Y')" href="#tabsNavigationSimpleIcons26" data-toggle="tab">Y</a>
                            </div>
                            <div class="item" data-hash="Z" style="text-align: center;">
                                <a id="item26" class="btn btn-default btn-circle btn-xl itemGlosario" onclick="changeItem('Z')" href="#tabsNavigationSimpleIcons27" data-toggle="tab">Z</a>
                            </div>
                        </div>
                        <div class="owl-theme">
                            <div class="owl-controls">
                                <div class="custom-nav owl-nav"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
        <div class="tab-content">
            @foreach ($abc as $key => $value)
            <div class="tab-pane" id="tabsNavigationSimpleIcons{{$value}}">
                <div class="center" style="margin-top: -50px;">
                    <div id="tabDesktop{{$value}}" class="container">
                        <div class="row reduce">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 my-3 rectangulo">
                                        <ul class="nav nav-pills flex-column" id="myTab{{$value}}" role="tablist">
                                            @foreach( (\App\Models\Glosary::where('letter', '=',$value)->get()) as $glosary)
                                            <li class="nav-item">
                                                <a class="nav-link" id="home-tab{{$value}}{{$glosary->id}}" data-toggle="tab" href="#home{{$value}}{{$glosary->id}}" role="tab" aria-controls="home{{$value}}{{$glosary->id}}" aria-selected="false">
                                                    {{$glosary->name}}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- /.col-md-4 -->
                                    <div class="col-lg-6 col-md-6 col-sm-7 col-xs-7 mx-auto">
                                        <div class="tab-content" id="myTabContent{{$value}}">
                                            @foreach( (\App\Models\Glosary::where('letter', '=',$value)->get()) as $glosary)
                                            <div class="tab-pane fade" id="home{{$value}}{{$glosary->id}}" role="tabpanel" aria-labelledby="home-tab{{$value}}{{$glosary->id}}">
                                                <p class="descripcionG text-letf">{{$glosary->content}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- /.col-md-8 -->
                                </div>



                            </div>
                            <!-- /.container -->
                        </div>
                    </div>

                    <div id="acordionMobile{{$value}}" class="container">
                        <div class="row">
                            @foreach( (\App\Models\Glosary::where('letter', '=',$value)->get()) as $glosary)
                            <div class="accordion mx-auto" style="width: 100%;" id="accordionExample">
                                <div class="card mx-auto" style="width: 100%;">
                                    <div class="card-header rectangulomobile" id="headingOne" style="width: 100%;">
                                        <div class="container" data-toggle="collapse" data-target="#collapse{{ $glosary->id }}" aria-expanded="true" aria-controls="collapse{{ $glosary->id }}">
                                            <center>
                                                <h3 class="title-item2">{{$glosary->name}}</h3>
                                            </center>
                                        </div>
                                    </div>

                                    <div id="collapse{{ $glosary->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="description-company-mobile text-letf">{{$glosary->content}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
    </section>


@if(!is_null($form))
    @if( ($form->status == 1) && ($form->long_short == 0) && ($form->pageform == 10) )
        @include('layouts.modal_fijo')
    @endif
@endif  
    @include('layouts.alarm_provider')
    <script>

        function cerrarDiv() {
            $("#divFloat").hide();
        }

        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 575) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 575) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });


        window.addEventListener("resize", cambioTam);

        function changeItem(item) {
            var url = window.location.href;
            if (url[url.length - 2] === '#') {
                url = url.substring(0, (url.length - 1));
                window.location.href = url + item
            } else {
                window.location.href = url + '#' + item
            }

        }

        function cambioTam() {
            if (window.innerWidth > 750) {
                for (let i = 0; i < 26; i++) {
                    $('#tabDesktop' + (i + 1)).show();
                    $('#acordionMobile' + (i + 1)).hide();
                }
            } else {
                for (let i = 0; i < 26; i++) {
                    $('#tabDesktop' + (i + 1)).hide();
                    $('#acordionMobile' + (i + 1)).show();
                }
            }
        }
        cambioTam();
        $(document).ready(function($) {

            var owl = $('.owl-carousel');
            owl.on('changed.owl.carousel',
                function(e) {
                    const itemSelect = $('#item' + e.relatedTarget._current);
                    if (itemSelect.length > 0) {
                        itemSelect[0].click();
                    }
                    const activo = $($("#myTab" + (e.relatedTarget._current + 1)));
                    if (activo.length > 0) {
                        const activoH1 = $(activo[0].children[0]);
                        if (activoH1.length > 0) {
                            const activoH2 = $(activoH1[0].children[0]);
                            if (activoH2.length > 0) {
                                activoH2[0].click();
                            }
                        }
                    }
                });

            $('.main-content .owl-carousel').owlCarousel({
                center: true,
                items: 3,
                loop: false,
                margin: 10,
                dots: false,
                nav: true,
                index: 5,
                navText: [
                    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    '<i class="fa fa-angle-right" aria-hidden="true"></i>'
                ],
                navContainer: '.main-content .custom-nav',
                URLhashListener: true,
                responsive: {
                    800: {
                        items: 7
                    }
                }
            });
            const itemSelect = $('#item3').click();
            if (itemSelect.length > 0) {
                itemSelect[0].click();
            }
        });
    </script>

    @include('layouts.footer')

</body>

</html>