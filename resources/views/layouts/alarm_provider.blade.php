  <section class="section bg-light py-0 " id="pricing">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="title-heading text-center" style=" font-family: Poppins; font-size: 20px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: normal; letter-spacing: normal; text-align: center; color: #614fa2;">¿Eres un proveedor de alarmas?</h1>
          <center>
            <a href="{{ route('companies.advertise') }}" class="btn btn-secondary btn-sm btn-round">ANUNCIARME</a>
          </center>
        </div>
      </div>
    </div>
  </section>