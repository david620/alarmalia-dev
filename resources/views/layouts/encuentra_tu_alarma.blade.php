 <!-- START COUNTER -->
 <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                <h4 class="h1-title text-center" style="font-family: Poppins; font-size: 35px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: normal; letter-spacing: normal; text-align: center; color: #2e3a52;">ENCUENTRA TU ALARMA</h4>
                <p class="title-desc text-center text-white-50">Ahorra dinero y tiempo con nuestro comparador de alarmas.<br>
                    Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                <center>
                    <div class="mt-5">
                        <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR AHORA</a>
                    </div>
                </center>
            </div>

        </div>
    </section>
    <!-- END COUNTER -->