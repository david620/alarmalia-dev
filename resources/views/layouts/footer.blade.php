<!-- START FOOTER MOBILE-->
<section id="footerMobile" class="footerRedes footerMobile">
    <div class="container">
        <div class="row footer-content">
            <div class="col-3"></div>
            <div class="col-2">
                <a rel="nofollow" href="https://www.linkedin.com/company/alarmalia/about/"><img src="{{URL::asset('img/icon/linkedin.png') }}" alt="" height="30"></a>
            </div>
            <div class="col-2">
                <a rel="nofollow" href="https://www.facebook.com/Alarmalia-799649047068312/"><img src="{{URL::asset('img/icon/facebook.png') }}" alt="" height="30"></a>
            </div>
            <div class="col-2">
                <a rel="nofollow" href="https://twitter.com/AlarmaliaC"><img src="{{URL::asset('img/icon/twitter.png') }}" alt="" height="30"></a>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</section>
<!-- START FOOTER -->
<section id="footerMobile1" class="footer footerMobile">
    <div class="container">
        <!-- START FOOTER -->

        <div class="row footer-content">
            <div class="col-3">
                <a class="ml-0" href="{{ route('site.home') }}"><img src="{{URL::asset('img/alarmalia/claro.png') }}" alt="" height="95"></a>
            </div>
            <div class="text-center col-8">
                <div class="ml-1 my-3 row text-center footer-content">
                    <h5 class="my-auto mx-auto text-white" style="padding: 2px; font-size: 15px;"><a class="text-white" rel="nofollow" href="{{ route('companies.about_us') }}">Nosotros</a></h5>

                    <h5 class="my-auto mx-auto text-white" style="padding: 2px; font-size: 15px;"><a class="text-white" rel="nofollow" href="{{ route('companies.advertise') }}">Anunciate</a></h5>

                    <h5 class="my-auto mx-auto text-white" style="padding: 2px; font-size: 15px;"><a class="text-white" rel="nofollow" href="{{ route('companies.list')}}">Empresas</a></h5>

                    <h5 class="my-auto mx-auto text-white" style="padding: 2px; font-size: 15px;"><a class="text-white" rel="nofollow" href="{{ route('companies.glosary')}}">Glosario</a></h5>

                    <h5 class="my-auto mx-auto text-white" style="padding: 2px; font-size: 15px;"><a class="text-white" rel="nofollow" href="{{ route('companies.contact')}}">Contacto</a></h5>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-11">
                <div class="start-form text-white text-center">
                    {!! Form::open(array('route' => 'newsletter.store','method'=>'POST')) !!}
                    {{ csrf_field() }}
                    <p>Suscribete a nuestra newsletter</p>
                    <input type="email" style="font-style:italic;" placeholder="Tu email aquí" required="required" name="email">
                    <button type="submit" class="btn btn-roundes">ENVIAR</button>
                    <br>
                    <p>

                        <input class="input-checkbox100" id="other" type="checkbox" name="remember-me" required="required">
                        <label class="label-checkbox100" for="other">
                            <a rel="nofollow" style="line-height:1.1" href="{{ route('companies.policies')}}"> <u> He leido y acepto la politica de privacidad</u></a>
                        </label></p>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-1"></div>
        </div>
        <!-- END FOOTER -->
    </div>
</section>
<!-- END FOOTER MOBILE-->


<!-- START FOOTER DESKTOP -->
<section id="footerDesktop" class="footer footerDesktop">
    <div class="container">
        <!-- START FOOTER -->

        <div class="row footer-content col-lg-12">
            <a href="{{ route('site.home') }}"><img src="{{URL::asset('img/alarmalia/claro.png') }}" alt="" height="100"></a>

            <h5 class="f-18 text-white text-center"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.about_us') }}">Nosotros</a></h5>

            <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.advertise') }}">Anunciate</a></h5>

            <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.list')}}">Empresas</a></h5>

            <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.glosary')}}">Glosario</a></h5>

            <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.contact')}}">Contacto</a></h5>

            <div style="padding-left: 20px" class="pt-3 mt-4 text-center">
                <a style="padding-left: 10px;" rel="nofollow" href="https://www.linkedin.com/company/alarmalia/about/"><img src="{{URL::asset('img/icon/linkedin.png') }}" alt="" height="15"></a>
                <a style="padding-left: 10px;" rel="nofollow" href="https://www.facebook.com/Alarmalia-799649047068312/"><img src="{{URL::asset('img/icon/facebook.png') }}" alt="" height="15"></a>
                <a style="padding-left: 10px;" rel="nofollow" href="https://twitter.com/AlarmaliaC"><img src="{{URL::asset('img/icon/twitter.png') }}" alt="" height="15"></a>
            </div>
            <div class="pt-3  start-form f-18 text-white text-center">
                <center>
                    {!! Form::open(array('route' => 'newsletter.store','method'=>'POST')) !!}
                    {{ csrf_field() }}
                    <p>Suscribete a nuestra newsletter</p>
                    <input type="email" style="font-style:italic;" placeholder="Tu email aquí" required="required" name="email">
                    <button type="submit" class="btn btn-roundes">ENVIAR</button>
                    <br>
                    <p>

                        <input class="input-checkbox100" id="ckb1" type="checkbox" required="required">
                        <label class="label-checkbox100" for="ckb1">
                            <a rel="nofollow" href="{{ route('companies.policies')}}"> <u> He leido y acepto la politica de privacidad</u></a>
                        </label></p>

                    {!! Form::close() !!}
                </center>
            </div>

        </div>
        <!-- END FOOTER -->
    </div>
</section>
<!-- END FOOTER DESKTOP-->

<!-- START FOOTER-AlT -->
<section class="foter">
    <div>
        <div class="col-lg-12">
            <p class="footer-alt text-center mb-0" style="font-size: 10px; line-height: 17px;">© 2020 Alarmalia | Todos los derechos reservados. Aviso legal. Política de cookies.<a rel="nofollow" href="{{ route('companies.policies')}}">Términos y condiciones </a>| <a rel="nofollow" href="{{ route('companies.avisolegal')}}">Política de cookies.</a> </p>        
        </div>
    </div>
</section>
<!-- END FOOTER-ALT -->

<!-- javascript -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/scrollspy.min.js') }}"></script>
<script src="{{ asset('js/counter.init.js') }}"></script>

<!-- Owl Carousel -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>



<!-- Swiper JS -->
<script src="{{ asset('js/swiper.min.js') }}"></script>

<!-- Magnific Popup -->
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/contact.js') }}"></script>
<script src="{{ asset('js/plugins-init.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
    $('.button').on('click', function(e) {
        $('.popup').toggleClass("close");
        $('.button').toggleClass("button-closed");
        e.preventDefault();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            $(this).siblings('active').addClass("active");
            $(this).addClass("active");
        });
    });
</script>