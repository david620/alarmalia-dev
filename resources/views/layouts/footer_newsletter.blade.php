    <!-- START FOOTER -->
    <section class="footer">
        <div class="container">
            <!-- START FOOTER -->
            
            <div class="row footer-content col-lg-12">
                <a href="{{ route('site.home') }}"><img src="{{URL::asset('img/alarmalia/claro.png') }}" alt="" height="100"></a>

                <h5 class="f-18 text-white text-center"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.about_us') }}">Nosotros</a></h5>

                <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.advertise') }}">Anunciate</a></h5>

                <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.list')}}">Empresas</a></h5>
                
                <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.glosary')}}">Glosario</a></h5>

                <h5 class="f-18 text-white"><a class="f-18 text-white" rel="nofollow" href="{{ route('companies.contact')}}">Contacto</a></h5>

                 <div style="padding-left: 20px" class="pt-3 mt-4 text-center">
                  <a rel="nofollow" style="padding-left: 10px;" href="https://www.linkedin.com/company/alarmalia/about/"><img src="{{URL::asset('img/icon/linkedin.png') }}" alt="" height="15"></a>
                  <a rel="nofollow" style="padding-left: 10px;" href="https://www.facebook.com/Alarmalia-799649047068312/"><img src="{{URL::asset('img/icon/facebook.png') }}" alt="" height="15"></a>
                  <a rel="nofollow" style="padding-left: 10px;" href="https://twitter.com/AlarmaliaC"><img src="{{URL::asset('img/icon/twitter.png') }}" alt="" height="15"></a>
                </div>
                <div  class="pt-3  start-form f-18 text-white text-center">
                        <center>

                        </center>
                    </div>

            </div>



           
            <!-- END FOOTER -->            
        </div>
    </section>
    <!-- END FOOTER -->

    <!-- START FOOTER-AlT -->
    <section class="foter">
        <div>
            <div class="col-lg-12">
                <p class="footer-alt text-center mb-0">© 2019 Alarmalia | Todos los derechos reservados.  Aviso legal. Términos y condiciones | Política de cookies. </p>
            </div>
        </div>
    </section>
    <!-- END FOOTER-ALT -->

    <!-- javascript -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/scrollspy.min.js') }}"></script>
    <script src="{{ asset('js/counter.init.js') }}"></script>

    <!-- Owl Carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    
    

    <!-- Swiper JS -->
    <script src="{{ asset('js/swiper.min.js') }}"></script>

    <!-- Magnific Popup -->
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/contact.js') }}"></script>
    <script src="{{ asset('js/plugins-init.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">
        $('.button').on('click', function(e) {
          $('.popup').toggleClass("close"); 
          $('.button').toggleClass("button-closed");
          e.preventDefault();      
        });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                $(this).siblings('active').addClass("active");
                $(this).addClass("active");
            });
        });
    </script>