<head>
    <meta charset="utf-8" />
    <title>Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
    <meta name="keywords" content="alarma, tyco alarmas, prosegur alarmas, vigilante" />
    <meta content="RkPeople" name="author" />


    <meta property="og:type" content="page" /> 
    <meta property="og:title" content="Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia" />
    <meta property="og:description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
    <meta property="og:url" content="PERMALINK" />
    <meta property="og:site_name" content="Alarmalia" />




    <!-- favicon -->
    <link rel="shortcut icon" href="{{URL::asset('img/favicon.png')}}">

    <!-- css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('fonts/css/all.css') }}" rel="stylesheet"> 

    <!--load all styles -->
    <link href="{{ asset('css/star-rating.css') }}" rel="stylesheet">
    <script src="{{ asset('js/star-rating.min.js') }}"></script>


    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('js/carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/carousel/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}" />
    <script src="{{ asset('js/carousel/jquery.min.js') }}"></script>
    <script src="{{ asset('js/carousel/owl.carousel.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleFrank.css') }}" />

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">

    <!-- Fonts -->
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129577938-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-129577938-1');
	</script> 
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M77GWGH');
	</script>
	<!-- End Google Tag Manager -->

    <script type="application/ld+json">
        {
            "@context"          : "http://schema.org",
            "@type"             : "Organization",
            "url"               : "http:/www.alarmalia.com",
            "contactPoint"      : [
            {
                "@type"         : "ContactPoint",
                "email"         : "info@alarmalia.com",
                "contactType"   : "customer service"

            }]

        }
    </script>
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
    "@id": "http://www.alarmalia.com/",
    "name": "Home"
    }
  },
  {
   "@type": "ListItem",
  "position": 2,
  "item":
   {
     "@id": "http://alarmalia.com/companias",
     "name": "Compañias"
   }
  },
   {
   "@type": "ListItem",
   "position": 3,
   "item":
   {
    "@id": "http://www.alarmalia.com/alarmas-hogar",
    "name": "Alarmas Hogar"
    }
  },
  {
   "@type": "ListItem",
  "position": 4,
  "item":
   {
     "@id": "http://www.alarmalia.com/alarmas-negocios",
     "name": "Alarmas Negocio"
   }
  },
     {
   "@type": "ListItem",
   "position": 5,
   "item":
   {
    "@id": "http://www.alarmalia.com/faaqs",
    "name": "Faqs"
    }
  },
  {
   "@type": "ListItem",
  "position": 6,
  "item":
   {
     "@id": "http://www.alarmalia.com/blog",
     "name": "Blog"
   }
  },
 {
   "@type": "ListItem",
  "position": 7,
  "item":
   {
     "@id": "http://www.alarmalia.com/sobre-nosotros",
     "name": "Sobre Nosotros"
   }
  },
 {
   "@type": "ListItem",
  "position": 8,
  "item":
   {
     "@id": "http://www.alarmalia.com/anunciate",
     "name": "Anúnciate"
   }
  },
{
   "@type": "ListItem",
  "position": 9,
  "item":
   {
     "@id": "http://www.alarmalia.com/glosario",
     "name": "Glosario"
   }
  },
{
   "@type": "ListItem",
  "position": 10,
  "item":
   {
     "@id": "http://www.alarmalia.com/contacto",
     "name": "Contacto"
   }
  }
 ]
}
</script>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M77GWGH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</head>