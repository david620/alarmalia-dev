    <div id="divFloat" class="fixed-top sticky sticky-dark fomuFloat">
        <button onclick="cerrarDiv()" style="background: transparent; float: right; border: 0px transparent;"><i class="fa fa-close"></i></button>
    <br>
        <div class="container">
            <div class="row justify-content-center">
                    {!! Form::open(array('route' => 'form.short','method'=>'POST')) !!}
                    {{ csrf_field() }}
                    <br>
                    <h1 class="title-formFloat mx-1">{{$question->title}}</h1>
                    <p class="text-formFloat mx-1">{{$form->descform}}</p>
                    <div class="form-group">
                        @if(!$answers->isEmpty())
                        <select class="form-control select-formFloat" id="exampleFormControlSelect1" name="resp">
                            @foreach($answers as $answer)
                                <option value="{{$answer->content}}">{{$answer->content}}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control select-formFloat" id="exampleFormControlInput1" placeholder="Tu teléfono aquí" name="phone">
                    </div>
                    <div class="form-group form-check mx-2">
                        <input required="required" type="checkbox" class="form-check-input mt-2" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">
                            <a class="terminos-formFloat" href="{{ route('companies.policies')}}">
                                <u>He leido y acepto la politica de privacidad</u>
                            </a>
                        </label>
                    </div>
                    <center>
                    <button type="submit" class="btn btn-custom btn-round mx-auto" style="  width: 200px; height: 48px; border-radius: 24px; background-color: #614fa2;">Solicitar Ahora</button></center>
                    <input type="hidden" name="q_id" value="{{$question->id}}">
                    {!! Form::close() !!}
            </div>
        </div>
    </div>