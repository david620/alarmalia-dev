@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Detail Leads
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop
@section('main-content')

<div class="box box-primary">
    <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-6">
      			<h3 class="box-title">Detalles Leads</h3>
      		</div>
      		<div class="col-xs-6 text-right">
      			<h3 class="box-title">
      				
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
      	<table class="table">
            	<thead>
              <tr class="header">
                    <th>Código Postal</th>
                    <th>Teléfono</th>
                    <th>Url Origen</th>
                    <th>Dirección IP</th>
                    <th>Pais</th>
                    <th>Fecha Real de Creación</th>
                    <th>Sincronizado</th>

            		
                  <th class="hidden-xs"></th>
            		</tr>
            	</thead>
            	<tbody>
            			<tr>   
                    <td>{{ $lead->postal_code }}</td>
                    <td>{{ $lead->phone }}</td>
                    <td>{{ $lead->origin }}</td>
                    <td>{{ $lead->ip }}</td>
            				<td>{{ $lead->country }}</td>
            				<td>{{ $lead->created_real }}</td>
                    <td>{{ $lead->created_at }}</td>
            				<td></td>
            			</tr>

            	</tbody>
          </table>
          <div class="text-center">
  	     
      </div>
     
  </div>
</div>

<ul class="list-group">
  @foreach($lead->responses as $response)
    <li class="list-group-item">
      <h4 class="list-group-item-heading" style="color: #888">{{ $response->question }}</h4>
      @if($response->response)
        <p class="list-group-item-text">{{ $response->response ? $response->response  : trans('app.noResponse')}}</p>
      @endif
    </li>
  @endforeach
</ul>


@endsection