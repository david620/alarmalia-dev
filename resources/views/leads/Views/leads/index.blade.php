@extends('adminlte::layouts.app')

@section('htmlheader_title')
  Leads
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
                    <form id="filter_c" name="filter_c" class="img-rounded " method="GET" style="background-color: #efefef; padding: 20px">              
                      {{ csrf_field() }}            
                      <div class="row">                        
                        <div class="col-sm-4">
                          <label>Código Postal</label>
                          <input class="form-control" name="name_filter" type="text" value="{{ app('request')->input('name_filter') }}">
                        </div>
                        <div class="col-sm-8">
                          <label>Fecha:</label><br/>
                          De:
                          <input type="date" class="form-control datePicker" style="width: 40%; display: inline" value="{{ app('request')->input('dateIni') }}" name="dateIni" id="dateIni"> A:
                          <input type="date" class="form-control datePicker" style="width: 40%; display: inline" value="{{ app('request')->input('dateFin') }}" name="dateFin" id="dateFin">
                        
                        </div>

                        
                        <div class="col-sm-12">
                          <label>&nbsp;</label><br/>
                          <button class="btn btn-primary btn-block"><span class="fa fa-search"></span> Buscar</button>
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('leads.export')}}">Exportar a Excel</a>    
                          </center>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="box">
      @if ($message = Session::get('success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
      @endif
    <div class="box-header with-border">
   		<div class="row">
   			<div class="col-xs-6">
      			<h3 class="box-title">Listado de Leads</h3>
      		</div>
      		<div class="col-xs-6 text-right">
      			<h3 class="box-title">
      				
      			</h3>
      		</div>
      	</div>
    </div>
    <div class="box-body">
        <br/>
      <table id="example15" class="table table-bordered table-striped">
          	<thead>
              <tr class="header">
                <th>Código Postal</th>
                <th>Teléfono</th>
                <th>Url Origen</th>
                <th>Dirección IP</th>
                <th>Pais</th>
                <th>Fecha Real de Creación</th>
                <th>Sincronizado</th>
                <th>Acción</th>
          		</tr>
          	</thead>
                    <tbody>
                      @foreach ($leads as $lead)
                        <tr>
                          @if(Pipe::validateRoute('Leads_detail'))
                            <td><a href="{{ URL::route('Leads_detail',array($lead->id)) }}">{{ $lead->postal_code }}</a></td>
                            <td><a href="{{ URL::route('Leads_detail',array($lead->id)) }}">{{ $lead->phone }}</a></td>
                          @else
                            <td>{{ $lead->postal_code }}</td>
                            <td>{{ $lead->phone }}</td>
                          @endif
                          <td>{{ $lead->origin }}</td>
                          <td>{{ $lead->ip }}</td>
                          <td>{{ $lead->country }}</td>
                          <td>{{ $lead->created_real }}</td>
                          <td>{{ $lead->updated_at }}</td>
                          <td><center>
                            @if(!$lead->times_sold && Pipe::validateRoute('Leads_awake'))
                              {!!link_to_route('Leads_awake', $title = '&nbsp; &nbsp; Awake',
                              $parameters = ['id' => $lead->id],
                              $attributes = ['class'=>'btn btn bg-primary fa fa-clone']);!!}  
                            @endif  
                            {!! Form::open(['method' => 'DELETE','route' => ['leads.destroy', $lead->id],'style'=>'display:inline']) !!}
                                  {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                          </center></td>
                        </tr>                      
                      @endforeach
                    </tbody>
        </table>
        <div class="text-center">
	    </div>
    </div>
    <div class="box-footer">
          {{ $leads->appends(app('request')->all())->links() }}
          Mostrando: <i>{{$leads->firstItem()}}</i> al <i>{{$leads->lastItem()}}</i> de: <i>{{  $leads->total() }} </i> Registros.
    </div>
</div>
</div>
<form id="awakeForm" method="post">
  {{ csrf_field() }}
</form>
@endsection


