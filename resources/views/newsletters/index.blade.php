@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Lista de Usuarios Asociados a Newsletter
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{ route('newsletter.export')}}">Exportar a Excel</a>		
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Correo Usuario</th>
							<th>Url de la Cual fue Registrado</th>
							<th>Estado</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($newsletters as $newsletter)
						<tr>
							<td>{{ $newsletter->mail }}</td>
							<td>{{ $newsletter->url_source }}</td>
							<td>
								@if($newsletter->status == 0)
									Unsuscrito
								@endif
								@if($newsletter->status == 1)
									Suscrito
								@endif

							</td>
							<td><center>
                                @if($newsletter->status == 0)
                                    {!!link_to_route('newsletter.subscribe', $title = 'Suscribir',
                                    $parameters = ['mail' => $newsletter->mail],
                                    $attributes = ['class'=>'btn btn bg-primary']);!!}
			                     @endif
                                @if($newsletter->status == 1)
                                    {!!link_to_route('newsletter.unsubscribe', $title = 'Cancelar Suscripción',
                                    $parameters = ['mail' => $newsletter->mail],
                                    $attributes = ['class'=>'btn btn bg-primary']);!!}
			                     @endif
								{!! Form::open(['method' => 'DELETE','route' => ['newsletter.destroy', $newsletter->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
            				</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
