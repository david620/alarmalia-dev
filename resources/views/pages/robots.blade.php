@extends('adminlte::layouts.app')

@section('htmlheader_title')
	robots.txt
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

		         <div class="table-responsive">
		        		
		        		<table id="" class="table no-border">
		                    <tbody  id="">
		                    	<tr class="">
										        <a class="btn btn-lg bg-blue" href="{{ url('/home') }}">Regresar</a>				
								          </tr>
		                    </tbody>
		            </table>	
					   </div>


	{!! Form::open(array('route' => 'robots.create','method'=>'POST')) !!}
    {{ csrf_field() }}


         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Modificar robots.txt</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="description_home">Contenido robots.txt:</label>
		            {!! Form::textarea('robots', $content, array('placeholder' => 'Contenido robots.txt','class' => 'form-control', 'name' => 'robots')) !!}
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Editar robots.txt</button>
              </div>
            </form>
          </div>

	{!! Form::close() !!}
@section('js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'summary-ckeditor' );
    </script>
@stop
@endsection

