<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

@include('layouts.encabezado_fijo')
    <!-- START COUNTER -->
    <section class="section counter pt-1 pb-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-headin text-center" style="color: #222222;">POLÍTICA DE PRIVACIDAD Y COOKIES</h1>
                    <p class="title-desc text-justify text-white-50 mt-4">Aviso: Por la mera utilización de la página web, navegando a través de ésta, se le atribuirá la condición de “Usuario”, debiendo leer atentamente la “Política de Privacidad” que a continuación se expone. Todo ello sin perjuicio de la necesidad de facilitar su consentimiento cuando le sea recabado para la aportación de datos personales y/o la instalación de cookies:
                    <br>
                    <br>
                    <p class="title-desc text-justify text-white-50">1. ASPECTOS GENERALES</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Mediante la presente Política de Privacidad, MARKETPLACE SEGURIDAD, S.L., con C.I.F. B-67311290, domiciliada en Barcelona, calle Muntaner número 452, 1º 2ª, 08006 Barcelona e inscrita en el Registro Mercantil de Barcelona, al Tomo 46679, Folio 144, Hoja B-527552, inscripción 1ª (en adelante, “Alarmalia.com”), desea informar a los usuarios de su sitio web www.alarmalia.com (en adelante, el “Portal”) acerca de su política de protección de datos de carácter personal. En Alarmalia.com estamos totalmente comprometidos con la seguridad de sus datos personales y por eso todos nuestros servicios y productos están diseñados teniendo en cuenta siempre la privacidad del Usuario y el correcto tratamiento de sus datos. Nuestra política de privacidad le ayudará a comprender cómo recopilamos, usamos y protegemos sus datos personales. A estos efectos, le informamos de que el tratamiento de datos personales se realizará conforme lo dispuesto en el Reglamento UE 2016/679 del Parlamento Europeo y del Consejo de 27 de Abril de 2016 relativo a la Protección de las personas físicas, y en concreto como sigue:</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">2. OBJETO Y FINALIDAD DE LA RECOGIDA DE DATOS PERSONALES</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Alarmalia.com tratará determinados datos que se obtengan a través del Portal con la finalidad de: 
                        a) Gestión de los usuarios. 
                        b) Gestión de consultas, peticiones y solicitudes recibidas a través del Portal por parte de los usuarios del mismo. 
                        c) Realización de envíos publicitarios y de información comercial, por diferentes medios, acerca de cada empresa, sus actividades, concursos, productos, servicios, ofertas, promociones especiales, así como documentación de diversa naturaleza y por diferentes medios
                        d) Gestión y cumplimiento de la relación establecida. 
                    Para cada finalidad sólo se tratarán los datos estrictamente necesarios.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">3. SOPORTE</p>
                    <p class="title-desc text-justify text-white-50 mt-4">El Usuario garantiza que los datos aportados son verdaderos, exactos, completos y actualizados, siendo responsable de cualquier daño o perjuicio, directo o indirecto, que pudiera ocasionarse como consecuencia del incumplimiento de tal obligación. En el caso de que los datos aportados pertenecieran a un tercero, el Usuario garantiza que ha informado a dicho tercero de los aspectos contenidos en este documento y obtenido su autorización para facilitar sus datos a Alarmalia.com, para los fines señalados. El Usuario deberá notificar cualquier modificación que se produzca en los datos facilitados. Alarmalia.com se reserva, sin perjuicio de otras acciones que pudieran corresponderle, el derecho a no registrar o a dar de baja a aquel Usuario que facilite datos falsos o incompletos. Alarmalia.com no asume responsabilidad alguna por los daños o perjuicios que pudieran derivarse de la falsedad o inexactitud de los datos suministrados, de las que responderá únicamente el Usuario, tanto frente a Alarmalia.com, como frente a la empresa instaladora de alarmas, siendo con relación a ésta última exclusivamente responsable el Usuario de los efectos que tal inexactitud, falsedad o desactualización de datos.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">5. VOLUNTARIEDAD Y OBLIGATORIEDAD DE LA APORTACIÓN DE DATOS</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Cuando los datos personales sean recabados a través de un formulario, será necesario que el Usuario aporte, al menos, aquellos que se indiquen como obligatorios, ya que, si no se suministraran estos datos considerados necesarios, no se podrá aceptar y gestionar el servicio o consulta formulada.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">6. SEGURIDAD Y CONFIDENCIALIDAD</p>
                    <p class="title-desc text-justify text-white-50 mt-4">En respuesta a la preocupación de Alarmalia.com por garantizar la seguridad y confidencialidad de sus datos, se han adoptado los niveles de seguridad requeridos de protección de los datos personales y se han instalado los medios técnicos a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados por canales web, telefónicos u otros. Alarmalia.com se compromete en la utilización de los datos incluidos en sus ficheros, a respetar su confidencialidad y a utilizarlos de acuerdo con la finalidad de cada fichero, así como a dar cumplimiento a su obligación de guardarlos y adoptar todas las medidas para evitar la alteración, pérdida, tratamiento o acceso no autorizado, de acuerdo con lo establecido en la legislación vigente.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">7. CONSERVACIÓN DE DATOS</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Conservaremos sus datos personales durante el tiempo que los necesitemos para proporcionar los productos o servicios acordados y/o solicitados. Además, continuaremos conservando sus datos después de que hayamos finalizado un servicio, de acuerdo con nuestros requisitos legales y reglamentarios y durante el tiempo que legalmente se puedan presentar reclamaciones en nuestra contra. Conservaremos, además, sus datos, durante un periodo de 5 años desde la finalización de los servicios que le proporcionamos, para nuestro análisis y elaboración de perfiles.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">8. EJERCICIO DE DERECHOS</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Para ejercitar sus derechos de acceso, rectificación, supresión, oposición, limitación de tratamiento y portabilidad sobre los datos incorporados al fichero titularidad de Alarmalia.com, el Usuario puede dirigirse a nosotros por correo electrónico escribiendo a: info@alarmalia.com. En el supuesto de que el Usuario no desee recibir comunicaciones comerciales electrónicas en el futuro, por parte de Alarmalia.com, podrá manifestar tal deseo enviando un correo electrónico a la dirección indicada en el apartado anterior. Finalmente, si el Usuario tuviese alguna queja sobre el tratamiento de sus datos personales, también tiene derecho a presentar una reclamación ante la autoridad supervisora correspondiente. En España es la Agencia Española de Protección de Datos (AEPD) y puede comunicarse con ellos en su sede (Agencia Española de Protección de Datos / Calle Jorge Juan 6 / 28001 – Madrid) o en su web: https://sedeagpd.gob.es/sede-electronica-web.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">9. POLÍTICA DE “COOKIES”</p>
                    <p class="title-desc text-justify text-white-50 mt-4">Con la finalidad de agilizar los servicios ofrecidos e identificar con la máxima celeridad a cada Usuario, Alarmalia.com utilizará “cookies”. Las “cookies” son bloques de datos que determinados websites envían al disco duro del ordenador cuando éste se conecta con la finalidad de identificarlo, quedando almacenada dicha información en el disco duro del ordenador del usuario. Las “cookies” se asocian únicamente con un Usuario anónimo, no permitiendo acceder a datos de carácter personal ni, en general, a datos del disco duro del Usuario. Alarmalia.com utiliza las “cookies” con diversos fines entre los que se encuentran el de asegurar una mayor rapidez y una personalización en el servicio que se da al Usuario, así como el de analizar los datos que de forma agregada arrojan los registros de “cookies” al respecto de índices de tráfico y audiencia y hábitos de los usuarios del Portal, para con dicha información contribuir a la evaluación y revisión de los servicios que se ofrecen al Usuario en el Portal. El Usuario tiene la posibilidad de configurar su navegador para que se instale en su disco duro una “cookie” o bien para rechazar todas o algunas de las “cookies”. En este último caso, la rapidez, calidad y efectividad en el funcionamiento de los servicios de Alarmalia.com puede disminuir. Los Usuarios pueden fácilmente rechazar y eliminar las cookies instaladas en su ordenador. Los procedimientos para el bloqueo y eliminación de las cookies pueden diferir de un navegador de Internet a otro, aunque por norma general la configuración de las cookies se realiza desde el menú “Opciones o Preferencias” de su navegador. Es aconsejable que consulte el menú “Ayuda” en su navegador, para poder así quedar informado de las diferentes opciones posibles relativas a cómo gestionar las cookies. Aun así, a continuación le proporcionamos los siguientes pasos sobre cómo configurar las cookies en los principales navegadores:</p>
                    <ul class="title-desc text-justify text-white-50 mt-4">
                        <li>
                            Safari: Preferencias => Seguridad.
                        </li>
                        <li>
                            Internet Explorer: Herramientas => Opciones de internet => Privacidad => Configuración.
                        </li>
                        <li>
                            Google Chrome: Configuración => Mostrar opciones avanzadas => Privacidad => Configuración de contenido.
                        </li>
                        <li>
                            Firefox: Herramientas => Opciones => Privacidad => Historia => Configuración Personalizada.
                        </li>

                    </ul>
                    <p class="title-desc text-justify text-white-50 mt-4">Nuestra recomendación para nuestros usuarios es la aceptación de la “Política de Cookies” de Alarmalia.com, para que pueda acceder a todos los contenidos e informaciones del Portal. Deshabilitar o eliminar las cookies puede afectar e impactar en la funcionalidad del Portal e incluso, la imposibilidad de acceder a cierto contenido o información, haciendo su experiencia menos satisfactoria.</p>
                    <br>
                    <p class="title-desc text-justify text-white-50">10. ACEPTACIÓN POLITICA DE PRIVACIDAD.</p>
                    <p class="title-desc text-justify text-white-50 mt-4">El envío de datos personales, mediante los formularios del Portal, supone el consentimiento al tratamiento de los datos personales por Alarmalia.com en los términos descritos en la presente Política de Privacidad, siempre que se haya aceptado con carácter previo y de manera expresa.</p>
                </div>                          
            </div>          
        </div>
        <br><br><br>
        

    </section>
    <!-- END COUNTER -->
		

 
    @include('layouts.encuentra_tu_alarma')
    <!-- END COUNTER -->
    <section class="section bg-light" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-heading text-center">¿Eres un proveedor de alarmas?</h1>
                    
                    <center><div class="mt-5">
                            <a href="{{ route('companies.advertise') }}" class="btn btn-secondary btn-sm btn-round">ANUNCIARME</a>
                        </div></center>
                </div>
            </div>
        </div>
    </section>
 
@include('layouts.footer')

</body>

</html>