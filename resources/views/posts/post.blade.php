<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->


 <!-- START COUNTER -->
    <section class="section counter mt-4">
        <div class="container">
            
            <div class="row">
                
                <div class="col-lg-12">
                    <div id="carouselExampleControls" class="carousel slide" >
                        <div class="carousel-inner">
                            
                            <div class="carousel-item active">
                                <center><img class="d-block " width="100%rem!" height="500rem!" src="{{URL::asset($post->url_img) }}" ></center>
                            </div><br>
                        </div>
                    </div>                                 
                </div>     
            </div>
        </div> 
    </section>
    <!-- END COUNTER -->
    <section class="section counter mt-4">
        <div class="container">
            
            <div class="row">                
                <div class="col-lg-12">
                                    <h1 class="title-heading text-center">{{$post->title}}</h1>
                                    <div class="parrafo mt-2">
                                        {!!$post->content!!}
                                    </div>
                        <br>
                        @if($post->post_file == null || $post->post_file == '')
                        @else
                            <center>
                                <div class="mt-5">
                                    <a href="{{URL::to($post->post_file)}}" class="btn btn-secondary  btn-round" target="_blank">Archivo Adjunto</a>
                                </div>
                            </center>
                        @endif                               
                </div>     
            </div>
        </div> 
    </section>



 


     <!-- START COUNTER -->
     <section class="section counter">
        <div class="container">
        <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="h1-title" style="color: #2e3a52;">ENCUENTRA TU ALARMA</h4>
                    <p class="parrafo mt-2" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->
@include('layouts.footer')

</body>

</html>