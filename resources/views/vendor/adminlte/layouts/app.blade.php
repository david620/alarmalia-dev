<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<style media="screen">
    .ths{
        padding-top:11px;padding-bottom:11px;background-color:#2f3031;color:gray;
    }
    .ths2{
        padding-top:11px;padding-bottom:11px;background-color:#2f3031;color:white;
    }
    .ths3{
        padding-top:11px;padding-bottom:11px;background-color:#666666;color:white;
    }
    .shortenedSelect {
        max-width: 350px;
    }


    /* HACK PARA EVITAR PROBLEMA DE SCROLLBARS POR TABLE-RESPONSIVE + JQUERY DATATABLES */
    div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 {
        min-height: .01%;
        overflow-x: auto;
    }
    @media screen and (max-width: 767px) {
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            border: 1px solid #ddd;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table {
            margin-bottom: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > thead > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tbody > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tfoot > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > thead > tr > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tbody > tr > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tfoot > tr > td {
            white-space: nowrap;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered {
            border: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > td:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > td:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > td:first-child {
            border-left: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > td:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > td:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > td:last-child {
            border-right: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr:last-child > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr:last-child > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr:last-child > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr:last-child > td {
            border-bottom: 0;
        }
        /* FIN DEL HACK JQUERY-DATATABLES */
    }

</style>
<body class="skin-blue sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
</html>
