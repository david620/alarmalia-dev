<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
{{--  datatables  --}}
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
 
<script>
      $(".alert").delay(6000).slideUp(200, function() {
        $(this).alert('close');
      });


  $(function () {
    $('#example1').DataTable({
          "paging": true,
          "order": [],
          "language": {
            "pageLength": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info":           "Mostrando _START_ de _END_ de _TOTAL_ resultados",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })

    $('#example2').DataTable({
          "paging": true,
          "order": [],
          "language": {
            "pageLength": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info":           "Mostrando _START_ de _END_ de _TOTAL_ resultados",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })
  })



$(document).ready(function(){
                /** add active class and stay opened when selected */
                var url = window.location;
                // for sidebar menu entirely but not cover treeview
                $('ul.sidebar-menu a').filter(function() {
                     return this.href == url;
                }).parent().addClass('active');

                // for treeview
                $('ul.treeview-menu a').filter(function() {
                     return this.href == url;
                }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
            });

</script>
@yield('js')