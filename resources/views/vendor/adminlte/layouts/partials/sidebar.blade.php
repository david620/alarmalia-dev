<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
                </div>
            </div>
        @endif



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!-- Optionally, you can add icons to the links -->
            @role('Administrador')
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Administración</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('users.index') }}">Usuarios</a></li>
                    <li><a href="{{ route('roles.index') }}">Roles</a></li>
                    <li><a href="{{ route('permissions.index') }}">Permisos</a></li>
                    <li><a href="{{ route('robots.index') }}">Robots</a></li>
                    <!--<li><a href="#">Traduccion</a></li> -->
                </ul>
            </li>
            @endrole
            @role(['Administrador', 'Administrador de leads'])
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Gestor de Leads</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('forms.index') }}">Formulario Comparador</a></li>
                    <li><a href="{{ route('Leads_index') }}">Leads</a></li>
                    @role(['Administrador'])                    
                        <li><a href="{{ route('newsletter.index') }}">Newsletter</a></li>
                    @endrole
                </ul>
            </li>
            @endrole
            @role(['Administrador','Editor empresa', 'Editor blog', 'Administrador de leads'])
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Gestor de Compañias</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @role(['Administrador', 'Administrador de leads'])                    
                        <li><a href="{{ route('companies.marca') }}">Empresas / Marcas</a></li>
                    @endrole
                    <li><a href="{{ route('companies.index') }}">Compañias</a></li>
                    @role(['Administrador', 'Administrador de leads','Editor empresa'])                    
                        <li><a href="{{ route('Clients_contracts') }}?&onlyActives=1">Contratos</a></li>
                    @endrole
                </ul>
            </li>
            @endrole
            @role(['Administrador','Editor empresa','Editor blog', 'Administrador de leads'])
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Gestor de Contenidos</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @role(['Administrador'])
                    <li><a href="{{ route('homepage.index') }}">Home</a></li>
                    <li><a href="{{ route('glosaries.index') }}">Glosario</a></li>
                    <li><a href="{{ route('banners.index') }}">Banners</a></li>
<!--
                    <li><a href="{{ route('pages.index') }}">Páginas Dinámicas</a></li>
-->
                    <li><a href="{{ route('advertises.index') }}">Formulario Anunciate</a></li>
                    <li><a href="{{ route('contact.index') }}">Formulario Contacto</a></li>
                    <li><a href="{{ route('faqs_categories.index') }}">Categorías Faqs</a></li>
                    <li><a href="{{ route('faqs.index') }}">Faqs</a></li>
                    @endrole
                    <li><a href="{{ route('posts.index') }}">Posts</a></li>
                </ul>
            </li>
            @endrole
            @role(['Administrador','Call-Center'])
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Call-Center</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @role(['Administrador','Call-Center'])
                    <li><a href="{{ route('callcenter.index') }}">Bandeja de Entrada</a></li>
                    <li><a href="{{ route('callcenter.calls_list') }}">Registro del CallCenter</a></li>
                    @endrole
                </ul>
            </li>
            @endrole
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
