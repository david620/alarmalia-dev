<!DOCTYPE html>
<html lang="es">
@include('layouts.head')


<link rel="stylesheet" href="{{ asset('css/carousel/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/carousel/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/carousel/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/carousel/theme-elementos.css') }}">

<style>
@media(min-width:320px) and (max-width:600px)  {
    .item{
  opacity:0.4;
  transition:.4s ease all;
  margin:0 20px;
  transform:scale(.8);
}
}
@media(max-width:600px){
  .item{margin:0; transform:none}
}
.active .item{
  opacity:1;
  transform:scale(1);
} 



.inner{position:absolute; bottom:30px; left:0; right:0; text-align:center;}
.inner a{color:#fff; text-decoration:none; border-bottom:2px solid rgba(255,255,255,0.5); transition:.3s ease border-color}
.inner a:hover{border-color:#fff;}
.black .inner a{color:#000; border-color:rgba(0,0,0,0.4)}
.black .inner a:hover{border-color:#000;}


.owl-controls{position:absolute; margin-top:300px;}

    .main-content {
        position: relative;
    }

    .main-content .owl-theme .custom-nav {
        position: absolute;
        top: 45%;
        left: 0;
        right: 0;
    }

    .main-content .owl-theme .custom-nav .owl-prev,
    .main-content .owl-theme .custom-nav .owl-next {
        position: absolute;
        color: inherit;
        background: none;
        border: none;
        z-index: 100;
    }

    .main-content .owl-theme .custom-nav .owl-prev i,
    .main-content .owl-theme .custom-nav .owl-next i {
        font-size: 2.5rem;
        color: #000;
    }

    .main-content .owl-theme .custom-nav .owl-prev {
        left: 0;
    }

    .main-content .owl-theme .custom-nav .owl-next {
        right: 0;
    }

    .carousel-caption {
        top: 60%;
        transform: translateY(-50%);
        bottom: initial;
        left: 60%;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        transform-style: preserve-3d;
    }
    .owl-carousel .owl-item img {
        box-shadow: 2px 1px 11px 5px #F3F1F8;
        background-color: white;
    }
    .owl-theme .owl-dots .owl-dot {
    display:none;
    }
</style>
<body>

@if(!is_null($formu))
                @include('layouts.modal_flotante')
@endif
    @include('layouts.header')

    <section class="bg-home-1" id="home">
        <div class="home-bg-overlay"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">
                        <div class="col-lg-5">
                            <div class="home-content">
                                <h3 class="home-title title2">Tu comparador de alarmas de casa y negocio</h3>

                                <div class="mt-2">
                                    <center class="nocenter">
                                        <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="home-img mt-4">
                                <img src="{{URL::asset('img/ALARMAS_homeDT.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="client-logo-curvo pt-3 pb-3">

        <div class="container">
            <div class="row">

                <div class="col-lg-1">
                </div>
                <div class="col-lg-2 ">
                    <center>
                        <div class="client-img">
                            <p align="center">
                                <img src="{{URL::asset('img/alarmalia/click-copy.png') }}" alt="logo-img">
                            </p>
                        </div>
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4">Todas las ofertas en un click</p>
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-2">
                    <center>
                        <div class="client-img">
                            <img src="{{URL::asset('img/alarmalia/customer-copy.png') }}" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4">Recibe un estudio personalizado</p>
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-2 ">
                    <center>
                        <div class="client-img">
                            <img src="{{URL::asset('img/alarmalia/book-copys.png') }}" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </center>
                    <p class="title-desc mt-4">Guía gratis con selección de alarmas</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>
    </section>

    <section class="section counter nopadding" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <br><br>
                    <h1 class="title-heading">Guía gratuita de compras de alarmas</h1>
                    <br>
                    <div class="title-desc text-center text-white-50">
                        <p>{!!$homepages->description_home!!}</p>
                    </div>
                    <br>
                    <div class="mt-5" align="center">
                        <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">QUIERO MI GUIA</a>
                    </div>


                    <div class="col-lg-12" style="padding-right: 0px;padding-left:0px;">
                        <br><br>
                        <h2 class="title-desca text-center mt-4">Empresas con las que trabajamos</h2>
                        <br><br>
                        <center>
                            <div class="container" style="padding:0px">
                                <div class="col-12">
                                    <div class="main-content">
                                        <div class="owl-carousel owl-theme">
                                            @foreach($companies as $company)
                                            <div class="item" style="text-align: center;">
                                                <img alt="" class="img-responsive img-rounded" style="height: 100px; width: 130px" src="{{$company->link_image}}">
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="owl-theme">
                                            <div class="owl-controls">
                                                <div class="custom-nav owl-nav"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
                <div class="container">
                    <h2 class="title-desca text-center mt-4">¿Qué alarma buscas?</h2>
                    <br><br>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="service-box pt-2 pb-3 text-center">
                                <a class=" text-center" style="color: #ffffff" href="{{ route('companies.homealarms')}}">
                                    <p> <img src="{{URL::asset('img/icon/home.png') }}" alt="">Alarmas para Casa<br> <u>ver</u></p>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="servicess-box pt-2 pb-3 text-center">
                                <a class=" text-center" style="color: #ffffff" href="{{ route('companies.business_alarms')}}">
                                    <p> <img src="{{URL::asset('img/icon/shop.png') }}" alt="">Alarmas para Negocio<br> <u>ver</u></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
     
        <div class="col-lg-12 section nopadding">
            <br><br>
            <h1 class="title-heading title-heading-2">¿Como seleccionar una alarma?</h1>
            <br>
            <div class="title-desc text-center text-white-50">
                <p>{{$homepages->select_alarm}}</p>
            </div>
            <br>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 fullscreen">
                        <div class="row flex-end" style="justify-content: center !important;">
                            <div class="col-xs-offset-2 circle-number selector-form pruebacir-left spacing">
                                <button>
                                    <h4 class="number-circle" style="font-family: Poppins; font-size: 32px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: 1.25; letter-spacing: normal; text-align: center; color: #614fa2;"> 1</h4>
                                </button>
                            </div>
                            <div class="col-xs-offset-10 square-text">
                                <p class="title-desc text-center text-white-50 my-4"> ¿Qué pasa ante un salto de alarma? <br> ¿tengo que preocuparme de algo?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 fullscreen">
                        <div class="row" style="justify-content: center !important;margin: 0px 20px;" >
                            <div class="col-xs-offset-10 square-text-right">
                                <p class="title-desc text-center text-white-50 my-4"> ¿Todos los sistemas de alarma me protegen además de ante <br>una posible intrusión, de intentos de sabotaje o inhibición?</p>
                            </div>
                            <div class="col-xs-offset-2 circle-number selector-form pruebacir spacing">
                                <button>
                                    <h4 class="number-circle" style="font-family: Poppins; font-size: 32px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: 1.25; letter-spacing: normal; text-align: center; color: #614fa2;"> 2</h4>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 fullscreen">
                    <div class="row flex-end">
                        <div class="col-xs-offset-2 circle-number selector-form pruebacir-left spacing">
                            <button>
                                    <h4 class="number-circle" style="font-family: Poppins; font-size: 32px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: 1.25; letter-spacing: normal; text-align: center; color: #614fa2;"> 3</h4>
                            </button>
                        </div>
                        <div class="col-xs-offset-10 square-text">
                            <p class="title-desc text-center text-white-50  my-4"> ¿Qué servicios adicionales tiene cada sistema de alarma <br> para sentirme completamente seguro?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="mt-5" align="center">
            <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR AHORA</a>
        </div>

        <center>
            <div class="col-lg-12">
                <div class="home-bg-home-candado"></div>
            </div>
            <div class="col-lg-12 section nopadding">
            <br><br>
            <h4 class="text-center pb-3 title-heading" style="font-family: Poppins; font-size: 30px; font-weight: 600; font-stretch: normal; font-style: italic; line-height: normal; letter-spacing: normal; text-align: center; color: #2e3a52;">¿Por qué utilizar ALARMALIA?</h4>
                <div class="title-desc text-center text-white-50 mt-4">
                    <p class="pb-3">{{$homepages->description_middle}}</p>
                </div>
            </div>
            </div>
    </section>

    <section style="background-color: #EFF5F8;padding-bottom:60px" class="section nopadding" id="services">
        <div class="container">
            <center>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/conocimientomercado.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">CONOCIMIENTO DEL MERCADO</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_one}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/ahorro.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">AHORRO</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_two}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/serviciogratuito.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">SERVICIO GRATUITO</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_three}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/rapidez.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">RAPIDEZ</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_four}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/resultadospersonalizados.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">RESULTADOS PERSONALIZADOS</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_five}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="services-box bg-white p-5 btn-rounde mt-4 tamanos_card">
                            <div class="services-icon text-center">
                                <img src="{{URL::asset('img/alarmalia/somosindependientes.png') }}" class="img-fluid" alt="">
                            </div><br>
                            <h1 class="title-headingg text-center">SOMOS INDEPENDIENTES</h1>
                            <div class="title-desc text-center text-white-50 mt-4">
                                <p class="text-center">{{$homepages->card_six}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </center>
        </div>
    </section>
@if(!is_null($formu))
        @include('layouts.modal_fijo')
@endif

    @include('layouts.encuentra_tu_alarma') 

    @include('layouts.alarm_provider')

    

    <script src="{{ asset('js/carousel/jquery.min.js') }}"></script>
    <script src="{{ asset('js/carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/carousel/theme.js') }}"></script>
    <script src="{{ asset('js/carousel/theme.init.js') }}"></script>

    <script type="text/javascript">
        function cerrarDiv() {
            $("#divFloat").hide();
        }
        $(document).ready(function($) {
            window.onresize = function() {
                if (window.innerWidth > 750) {
                    $('#divFloat').show();
                } else {
                    $('#divFloat').hide();
                }
            }
            if (window.innerWidth > 750) {
                $('#divFloat').show();
            } else {
                $('#divFloat').hide();
            }
        });
    </script>

<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    stagePadding: 200,
    loop:true,
    margin:10,
    nav:false,
    items:1,
    lazyLoad: true,
    nav:true,
  responsive:{
        0:{
            items:1,
            stagePadding: 60
        },
        500:{
            items:1,
            stagePadding:100
        },
        600:{
            items:2,
            stagePadding:100
        },
        1000:{
            items:3,
            stagePadding: 150
        },
        1200:{
            items:3,
            stagePadding: 200
        },
        1400:{
            items:3,
            stagePadding: 200
        },
        1600:{
            items:3,
            stagePadding: 200
        },
        1800:{
            items:3,
            stagePadding: 200
        }
    }
})
</script>
<!--
    <script>
        $('.owl-carousel').owlCarousel({
            items: 1,
            margin: 16,
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.main-content .custom-nav',
            dots: false,
            responsive: {
                700: {
                    items: 4
                }
            }
        })
    </script> -->
    @include('layouts.footer')

</body>

</html>