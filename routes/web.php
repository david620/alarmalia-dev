<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/////////////////////// Redirecciones ///////////////////////////////
Route::get('alarma-negocio', function(){ 
        return redirect()->route('companies.business_alarms');
});


Route::get('preguntas-frecuentes', function(){ 
        return redirect()->route('companies.questions');
});


Route::get('mailing_ralset', function(){ 
        return redirect()->route('site.home');
});

Route::get('resultados', function(){ 
        return redirect()->route('site.home');
});

Route::get('redirect', function(){ 
        return redirect()->route('site.home');
});

Route::get('guia-de-compra-de-alarmas-gratis', function(){ 
        return redirect()->route('site.home');
});


Route::get('legal', function(){ 
        return redirect()->route('companies.avisolegal');
});

Route::get('alarmas-para-casa', function(){ 
        return redirect()->route('companies.homealarms');
});

Route::get('nosotros', function(){ 
        return redirect()->route('companies.about_us');
});


Route::get('empresas-de-seguridad', function(){ 
        return redirect()->route('companies.list');
});

Route::get('glosario-de-alarmas', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('politica-privacidad', function(){ 
        return redirect()->route('companies.policies');
});


Route::get('glosario-de-alarmas/alarmas-sin-cuotas', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/alarma-por-cable', function(){ 
        return redirect()->route('companies.glosary');
});


Route::get('glosario-de-alarmas/detector-de-alarmas', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/video-verificacion', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/polling', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/sirena', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/inhibicion', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/disuasion', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/sabotaje', function(){ 
        return redirect()->route('companies.glosary');
});

Route::get('glosario-de-alarmas/cctv', function(){ 
        return redirect()->route('companies.glosary');
});


Route::get('principales-empresas-de-alarmas/securitas-direct', function(){ 
        return redirect()->route('companies.list');
});

Route::get('principales-empresas-de-alarmas/sector-alarm', function(){ 
        return redirect()->route('companies.list');
});

Route::get('principales-empresas-de-alarmas/tyco-integratedfsc', function(){ 
        return redirect()->route('companies.list');
});

Route::get('principales-empresas-de-alarmas/prosegur', function(){ 
        return redirect()->route('companies.list');
});

/////////////////////// Redirecciones ///////////////////////////////



Route::get('sitemap', 'SitemapsController@index');
Route::get('sitemap/posts', ['as' => 'sitemaps.posts', 'uses' => 'SitemapsController@posts']);
Route::get('sitemap/pages', ['as' => 'sitemaps.pages', 'uses' => 'SitemapsController@pages']);
//////////////////// Rutas Frontend/////////////////////////////
Route::get('/', ['as' => 'site.home', 'uses' => 'SiteController@home']);
Route::get('companias', ['as'             => 'companies.list', 'uses'           => 'SiteController@companies_list']);
Route::get('compania/{id}', ['as'             => 'companies.profile', 'uses'           => 'SiteController@company']);
Route::get('alarmas-hogar', ['as'             => 'companies.homealarms', 'uses'           => 'SiteController@homealarms']);
Route::get('alarmas-negocios', ['as'             => 'companies.business_alarms', 'uses'           => 'SiteController@business_alarms']);
Route::get('anunciate', ['as'             => 'companies.advertise', 'uses'           => 'SiteController@advertise']);
Route::get('faaqs', ['as'             => 'companies.questions', 'uses'           => 'SiteController@faqs']);
Route::get('blog', ['as'             => 'companies.blog', 'uses'           => 'SiteController@blog']);
Route::get('blog/post/{id}', ['as'             => 'post.blog', 'uses'           => 'PostController@post_view']);
Route::get('cuestionario-comparador', ['as'             => 'companies.comparator', 'uses'           => 'SiteController@comparator']);
Route::get('cuestionario-comparador-final', ['as'             => 'companies.end_comparator', 'uses'           => 'SiteController@end_comparator']);
Route::get('cuestionario-comparador-camino/{id}', ['as'             => 'companies.comparator_camino', 'uses'           => 'SiteController@comparator_camino']);
Route::post('formulario-corto', ['as'   => 'form.short', 'uses'   => 'SiteController@form_short']);
Route::post('comparator', ['as'   => 'comparator.store', 'uses'   => 'SiteController@store_comparator']);
Route::get('sobre-nosotros', ['as'             => 'companies.about_us', 'uses'           => 'SiteController@about_us']);
Route::get('terminos-condiciones', ['as'             => 'companies.policies', 'uses'           => 'SiteController@policies']);
Route::get('aviso-legal', ['as'             => 'companies.avisolegal', 'uses'           => 'SiteController@avisolegal']);
Route::get('contacto', ['as'             => 'companies.contact', 'uses'           => 'SiteController@contact']);
Route::get('glosario', ['as'             => 'companies.glosary', 'uses'           => 'SiteController@glosary']);
Route::post('anunciate', ['as'   => 'advertise.store', 'uses'   => 'SiteController@advertise_store']);
Route::post('contacto', ['as'   => 'contact.store', 'uses'   => 'SiteController@contact_store']);
Route::post('store-newsletter', ['as' => 'newsletter.store', 'uses' => 'NewsLetterController@store']);
//////////////////// Rutas Frontend/////////////////////////////



//////////////////// Rutas Publicas Clientes /////////////////////////////


Route::get('/cron/clients/synchro', array('as' => 'Client_cron_synchro', 'uses' => 'ClientsCronController@synchro'));

Route::get('/cron/clients/distribute', array('as' => 'Client_cron_distribute', 'uses' => 'ClientsCronController@distribute'));

Route::get('/cron/clients/distribute_email', array('as' => 'Client_cron_distribute_email', 'uses' => 'ClientsCronController@distributeEmail'));

Route::get('/cron/clients/synchro_contracts', array('as' => 'Client_cron_synchro_contracts', 'uses' => 'ClientsCronController@synchroContracts'));

Route::get('/public/invalid_lead/{id}/{clientCode}',array('as' => 'Client_public_lead_invalid', 'uses' => 'ClientsCronController@invalidLead'));


//////////////////// Rutas Publicas Clientes /////////////////////////////


//////////////////// Rutas Publicas Leads /////////////////////////////

Route::get('/cron/leads/synchro', array('as' => 'Leads_cron_synchro', 'uses' => 'LeadsCronController@synchro'));

//////////////////// Rutas Publicas Leads /////////////////////////////




Route::group(['middleware' => 'auth'], function () {

		Route::get('callcenter', ['as'           => 'callcenter.index', 'uses'           => 'CallCenterController@index'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/calls_list', ['as'           => 'callcenter.calls_list', 'uses'           => 'CallCenterController@list_calls'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/call/{id}', ['as'           => 'callcenter.call', 'uses'           => 'CallCenterController@call'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/namelastname/{id}', ['as'           => 'callcenter.namelastname', 'uses'           => 'CallCenterController@namelastname'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/called/{id}', ['as'           => 'callcenter.called', 'uses'           => 'CallCenterController@called'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/dont_answer/{id}', ['as'           => 'callcenter.dont_answer', 'uses'           => 'CallCenterController@dont_answer'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/negative/{id}', ['as'           => 'callcenter.negative', 'uses'           => 'CallCenterController@negative'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/positive/{id}', ['as'           => 'callcenter.positive', 'uses'           => 'CallCenterController@positive'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('callcenter/negative_answer/{id}/{option}', ['as'           => 'callcenter.negative_answer', 'uses'           => 'CallCenterController@negative_answer'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::post('callcenter/positive_answer/{id}', ['as'           => 'callcenter.positive_answer', 'uses'           => 'CallCenterController@positive_answer'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::post('callcenter/end_positive_answer/{id}', ['as'           => 'callcenter.end_positive_answer', 'uses'           => 'CallCenterController@end_positive_answer'])->middleware('auth', 'role:Administrador|Call-Center');
		Route::get('export-callcenter', ['as' => 'callcenter.export', 'uses' => 'CallCenterController@export'])->middleware('auth', 'role:Administrador|Call-Center');
		

		Route::get('index-newsletter', ['as' => 'newsletter.index', 'uses' => 'NewsLetterController@index'])->middleware('auth', 'role:Administrador');
		Route::get('subscribe-newsletter/{mail}', ['as' => 'newsletter.subscribe', 'uses' => 'NewsLetterController@subscribe']);
		Route::get('unsubscribe-newsletter/{mail}', ['as' => 'newsletter.unsubscribe', 'uses' => 'NewsLetterController@unsubscribe']);
		Route::delete('delete-newsletter/{id}', ['as'   => 'newsletter.destroy', 'uses'   => 'NewsLetterController@destroy'])->middleware('auth', 'role:Administrador');
		Route::get('export-newsletter', ['as' => 'newsletter.export', 'uses' => 'NewsLetterController@export'])->middleware('auth', 'role:Administrador');

		Route::get('roles', ['as'           => 'roles.index', 'uses'           => 'RoleController@index'])->middleware('auth', 'role:Administrador');
		Route::get('roles/create', ['as'    => 'roles.create', 'uses'    => 'RoleController@create'])->middleware('auth', 'role:Administrador');
		Route::post('roles/create', ['as'   => 'roles.store', 'uses'   => 'RoleController@store'])->middleware('auth', 'role:Administrador');
		Route::get('roles/{id}', ['as'      => 'roles.show', 'uses'      => 'RoleController@show'])->middleware('auth', 'role:Administrador');
		Route::get('roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'RoleController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('roles/{id}', ['as'    => 'roles.update', 'uses'    => 'RoleController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('roles/{id}', ['as'   => 'roles.destroy', 'uses'   => 'RoleController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('leads/index', ['as'           => 'leads.index', 'uses'           => 'LeadController@index']);
		Route::get('leads/{id}', ['as'      => 'leads.show', 'uses'      => 'LeadController@show']);
		Route::get('leads/{id}/edit', ['as' => 'leads.edit', 'uses' => 'LeadController@edit']);
		Route::patch('leads/{id}', ['as'    => 'leads.update', 'uses'    => 'LeadController@update']);

		Route::get('glosaries', ['as'           => 'glosaries.index', 'uses'           => 'GlosaryController@index'])->middleware('auth', 'role:Administrador');
		Route::get('glosaries/create', ['as'    => 'glosary.create', 'uses'    => 'GlosaryController@create'])->middleware('auth', 'role:Administrador');
		Route::post('glosaries/create', ['as'   => 'glosaries.store', 'uses'   => 'GlosaryController@store'])->middleware('auth', 'role:Administrador');
		Route::get('glosaries/{id}', ['as'      => 'glosary.show', 'uses'      => 'GlosaryController@show'])->middleware('auth', 'role:Administrador');
		Route::get('glosaries/{id}/edit', ['as' => 'glosaries.edit', 'uses' => 'GlosaryController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('glosaries/{id}', ['as'    => 'glosaries.update', 'uses'    => 'GlosaryController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('glosaries/{id}', ['as'   => 'glosaries.destroy', 'uses'   => 'GlosaryController@destroy'])->middleware('auth', 'role:Administrador');		

		Route::get('faqs', ['as'           => 'faqs.index', 'uses'           => 'FaqController@index'])->middleware('auth', 'role:Administrador');
		Route::get('faqs/create', ['as'    => 'faqs.create', 'uses'    => 'FaqController@create'])->middleware('auth', 'role:Administrador');
		Route::post('faqs/create', ['as'   => 'faqs.store', 'uses'   => 'FaqController@store'])->middleware('auth', 'role:Administrador');
		Route::get('faqs/{id}/edit', ['as' => 'faqs.edit', 'uses' => 'FaqController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('faqs/{id}', ['as'    => 'faqs.update', 'uses'    => 'FaqController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('faqs/{id}', ['as'   => 'faqs.destroy', 'uses'   => 'FaqController@destroy'])->middleware('auth', 'role:Administrador');


		Route::get('faqs/categories', ['as'           => 'faqs_categories.index', 'uses'           => 'FaqCategoryController@index'])->middleware('auth', 'role:Administrador');
		Route::get('faqs/categories/create', ['as'    => 'faqs_categories.create', 'uses'    => 'FaqCategoryController@create'])->middleware('auth', 'role:Administrador');
		Route::post('faqs/categories/create', ['as'   => 'faqs_categories.store', 'uses'   => 'FaqCategoryController@store'])->middleware('auth', 'role:Administrador');
		Route::get('faqs/categories/{id}/edit', ['as' => 'faqs_categories.edit', 'uses' => 'FaqCategoryController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('faqs/categories/{id}', ['as'    => 'faqs_categories.update', 'uses'    => 'FaqCategoryController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('faqs/categories/{id}', ['as'   => 'faqs_categories.destroy', 'uses'   => 'FaqCategoryController@destroy'])->middleware('auth', 'role:Administrador');
		Route::get('faqs/categories/modificaEstado/{id}', ['as' => 'faqs_categories.modificaEstado', 'uses'      => 'FaqCategoryController@modificaEstado'])->middleware('auth', 'role:Administrador');

		Route::get('questions/{id}', ['as'           => 'questions.index', 'uses'           => 'QuestionController@index'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('questions/create', ['as'    => 'questions.create', 'uses'    => 'QuestionController@create'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::post('questions/create', ['as'   => 'questions.store', 'uses'   => 'QuestionController@store'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('questions/{id_question}/{id_form}/edit', ['as' => 'questions.edit', 'uses' => 'QuestionController@edit'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::patch('questions/{id}', ['as'    => 'questions.update', 'uses'    => 'QuestionController@update'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::delete('questions/{id}', ['as'   => 'questions.destroy', 'uses'   => 'QuestionController@destroy'])->middleware('auth', 'role:Administrador|Administrador de leads');

		Route::get('questions_short/{id}', ['as'           => 'questions_short.index', 'uses'           => 'QuestionController@index_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('questions_short/create', ['as'    => 'questions_short.create', 'uses'    => 'QuestionController@create'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::post('questions_short/create', ['as'   => 'questions_short.store', 'uses'   => 'QuestionController@store_short'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('questions_short/{id_question}/{id_form}/edit', ['as' => 'questions_short.edit', 'uses' => 'QuestionController@edit_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::patch('questions_short/{id}', ['as'    => 'questions_short.update', 'uses'    => 'QuestionController@update_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::delete('questions_short/{id}', ['as'   => 'questions_short.destroy', 'uses'   => 'QuestionController@destroy_short'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('answers_short/{id}/{form_id}/index', ['as' => 'answers_short.index', 'uses' => 'AnswerController@index_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::post('answers_short/create', ['as'   => 'answers_short.store', 'uses'   => 'AnswerController@store_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::delete('answers_short/{id}', ['as'   => 'answers_short.destroy', 'uses'   => 'AnswerController@destroy_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('answers_short/{id_question}/{id_form}/{id_answer}/edit', ['as' => 'answers_short.edit', 'uses' => 'AnswerController@edit_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::patch('answers_short/{id}', ['as'    => 'answers_short.update', 'uses'    => 'AnswerController@update_corto'])->middleware('auth', 'role:Administrador|Administrador de leads');

		Route::get('answers/{id}/{form_id}/index', ['as' => 'answers.index', 'uses' => 'AnswerController@index'])->middleware('auth', 'role:Administrador|Administrador de leads');
//		Route::get('answers/{id}/create', ['as'    => 'answers.create', 'uses'    => 'AnswerController@create']);
		Route::post('answers/create', ['as'   => 'answers.store', 'uses'   => 'AnswerController@store'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('answers/{id_question}/{id_form}/{id_answer}/edit', ['as' => 'answers.edit', 'uses' => 'AnswerController@edit'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::patch('answers/{id}', ['as'    => 'answers.update', 'uses'    => 'AnswerController@update'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::delete('answers/{id}', ['as'   => 'answers.destroy', 'uses'   => 'AnswerController@destroy'])->middleware('auth', 'role:Administrador|Administrador de leads');

		Route::get('forms', ['as'           => 'forms.index', 'uses'           => 'FormController@index'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('forms/create', ['as'    => 'forms.create', 'uses'    => 'FormController@create'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::post('forms/create', ['as'   => 'forms.store', 'uses'   => 'FormController@store'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('forms/{id}/edit', ['as' => 'forms.edit', 'uses' => 'FormController@edit'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::patch('forms/{id}', ['as'    => 'forms.update', 'uses'    => 'FormController@update'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::delete('forms/{id}', ['as'   => 'forms.destroy', 'uses'   => 'FormController@destroy'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('forms/modificaEstado/{id}', ['as' => 'form.modificaEstado', 'uses'      => 'FormController@modificaEstado'])->middleware('auth', 'role:Administrador|Administrador de leads');


		Route::get('way/{id}', ['as'           => 'way.index', 'uses'           => 'FormController@way'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::get('forms_questions_answers/{id}', ['as'           => 'forms_questions_answers.index', 'uses'           => 'FormController@forms_questions_answers'])->middleware('auth', 'role:Administrador|Administrador de leads');
		Route::post('forms_questions_answers', ['as'   => 'forms_questions_answers.store', 'uses'   => 'FormController@store_camino'])->middleware('auth', 'role:Administrador|Administrador de leads');

		Route::get('permissions', ['as'           => 'permissions.index', 'uses'           => 'PermissionController@index'])->middleware('auth', 'role:Administrador');
		Route::get('permissions/create', ['as'    => 'permissions.create', 'uses'    => 'PermissionController@create'])->middleware('auth', 'role:Administrador');
		Route::post('permissions/create', ['as'   => 'permissions.store', 'uses'   => 'PermissionController@store'])->middleware('auth', 'role:Administrador');
		Route::get('permissions/{id}', ['as'      => 'permission.show', 'uses'      => 'PermissionController@show'])->middleware('auth', 'role:Administrador');
		Route::get('permissions/{id}/edit', ['as' => 'permission.edit', 'uses' => 'PermissionController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('permissions/{id}', ['as'    => 'permissions.update', 'uses'    => 'PermissionController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('permissions/{id}', ['as'   => 'permission.destroy', 'uses'   => 'PermissionController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('banners', ['as'           => 'banners.index', 'uses'           => 'BannerController@index'])->middleware('auth', 'role:Administrador');
		Route::get('banners/create', ['as'    => 'banner.create', 'uses'    => 'BannerController@create'])->middleware('auth', 'role:Administrador');
		Route::post('banners/create', ['as'   => 'banners.store', 'uses'   => 'BannerController@store'])->middleware('auth', 'role:Administrador');
		Route::get('banners/{id}', ['as'      => 'banner.show', 'uses'      => 'BannerController@show'])->middleware('auth', 'role:Administrador');
		Route::get('banners/{id}/edit', ['as' => 'banner.edit', 'uses' => 'BannerController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('banners/{id}', ['as'    => 'banners.update', 'uses'    => 'BannerController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('banners/{id}', ['as'   => 'banner.destroy', 'uses'   => 'BannerController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('posts', ['as'           => 'posts.index', 'uses'           => 'PostController@index'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('posts/create', ['as'    => 'posts.create', 'uses'    => 'PostController@create'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::post('posts/create', ['as'   => 'posts.store', 'uses'   => 'PostController@store'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('posts/{id}/edit', ['as' => 'posts.edit', 'uses' => 'PostController@edit'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::patch('posts/{id}', ['as'    => 'posts.update', 'uses'    => 'PostController@update'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::delete('posts/{id}', ['as'   => 'posts.destroy', 'uses'   => 'PostController@destroy'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('posts/modificaEstado/{id}', ['as' => 'posts.modificaEstado', 'uses'      => 'PostController@modificaEstado'])->middleware('auth', 'role:Administrador');

		Route::get('users', ['as'           => 'users.index', 'uses'           => 'UserController@index'])->middleware('auth', 'role:Administrador');
		Route::get('users/create', ['as'    => 'users.create', 'uses'    => 'UserController@create'])->middleware('auth', 'role:Administrador');
		Route::post('users/create', ['as'   => 'users.store', 'uses'   => 'UserController@store'])->middleware('auth', 'role:Administrador');
		Route::get('users/{id}', ['as'      => 'users.show', 'uses'      => 'UserController@show'])->middleware('auth', 'role:Administrador');
		Route::get('users/{id}/edit', ['as' => 'users.edit', 'uses' => 'UserController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('users/{id}', ['as'    => 'users.update', 'uses'    => 'UserController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('users/{id}', ['as'   => 'user.destroy', 'uses'   => 'UserController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('companies/index', ['as'           => 'companies.index', 'uses'           => 'CompaniesController@index'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('companies/create', ['as'    => 'companies.create', 'uses'    => 'CompaniesController@create'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::post('companies/create', ['as'   => 'companies.store', 'uses'   => 'CompaniesController@store'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('companies/{id}/show', ['as'      => 'companies.show', 'uses'      => 'CompaniesController@show'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('companies/{id}/edit', ['as' => 'companies.edit', 'uses' => 'CompaniesController@edit'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::patch('companies/{id}', ['as'    => 'companies.update', 'uses'    => 'CompaniesController@update'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::delete('companies/{id}', ['as'   => 'companies.destroy', 'uses'   => 'CompaniesController@destroy'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('companies/modificaEstado/{id}', ['as' => 'companies.modificaEstado', 'uses'      => 'CompaniesController@modificaEstado'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');


		Route::get('advantages/{id}/index', ['as'      => 'advantages.index', 'uses'           => 'AdvantagesController@index'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::get('advantages/{id}/create', ['as'    => 'advantages.create', 'uses'    => 'AdvantagesController@create'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::post('advantages/create', ['as'   => 'advantages.store', 'uses'   => 'AdvantagesController@store'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::get('advantages/{id}/edit', ['as' => 'advantages.edit', 'uses' => 'AdvantagesController@edit'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::patch('advantages/{id}', ['as'    => 'advantages.update', 'uses'    => 'AdvantagesController@update'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::delete('advantages/{id}', ['as'   => 'advantages.destroy', 'uses'   => 'AdvantagesController@destroy'])->middleware('auth', 'role:Administrador|Editor empresa');

		Route::get('advertises/index', ['as'      => 'advertises.index', 'uses'           => 'AdvertiseController@index'])->middleware('auth', 'role:Administrador');
		Route::get('advertises/{id}/create', ['as'    => 'advertises.create', 'uses'    => 'AdvertiseController@create'])->middleware('auth', 'role:Administrador');
		Route::get('advertises/{id}/edit', ['as' => 'advertises.edit', 'uses' => 'AdvertiseController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('advertises/{id}', ['as'    => 'advertises.update', 'uses'    => 'AdvertiseController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('advertises/{id}', ['as'   => 'advertises.destroy', 'uses'   => 'AdvertiseController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('contact/index', ['as'      => 'contact.index', 'uses'           => 'ContactController@index'])->middleware('auth', 'role:Administrador');
		Route::get('contact/{id}/create', ['as'    => 'contact.create', 'uses'    => 'ContactController@create'])->middleware('auth', 'role:Administrador');
		Route::get('contact/{id}/edit', ['as' => 'contact.edit', 'uses' => 'ContactController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('contact/{id}', ['as'    => 'contact.update', 'uses'    => 'ContactController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('contact/{id}', ['as'   => 'contact.destroy', 'uses'   => 'ContactController@destroy'])->middleware('auth', 'role:Administrador');		

		Route::get('components/{id}/index', ['as'      => 'components.index', 'uses'           => 'ComponentsController@index'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::get('components/{id}/create', ['as'    => 'components.create', 'uses'    => 'ComponentsController@create'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::post('components/create', ['as'   => 'components.store', 'uses'   => 'ComponentsController@store'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::get('components/{id}/edit', ['as' => 'components.edit', 'uses' => 'ComponentsController@edit'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::patch('components/{id}', ['as'    => 'components.update', 'uses'    => 'ComponentsController@update'])->middleware('auth', 'role:Administrador|Editor empresa');
		Route::delete('components/{id}', ['as'   => 'components.destroy', 'uses'   => 'ComponentsController@destroy'])->middleware('auth', 'role:Administrador|Editor empresa');



		Route::get('components/caract/{id}/index', ['as'      => 'components_caract.index', 'uses'           => 'ComponentsCaractController@index'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('components/caract/{id}/create', ['as'    => 'components_caract.create', 'uses'    => 'ComponentsCaractController@create'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::post('components/caract/create', ['as'   => 'components_caract.store', 'uses'   => 'ComponentsCaractController@store'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::get('components/caract/{id}/edit', ['as' => 'components_caract.edit', 'uses' => 'ComponentsCaractController@edit'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::patch('components/caract/{id}', ['as'    => 'components_caract.update', 'uses'    => 'ComponentsCaractController@update'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');
		Route::delete('components/caract/{id}', ['as'   => 'components_caract.destroy', 'uses'   => 'ComponentsCaractController@destroy'])->middleware('auth', 'role:Administrador|Editor empresa|Editor blog|Administrador de leads');


		Route::get('robots', ['as'           => 'robots.index', 'uses'           => 'SiteController@robots'])->middleware('auth', 'role:Administrador');
		Route::post('robots/create', ['as'           => 'robots.create', 'uses'           => 'SiteController@robots_create'])->middleware('auth', 'role:Administrador');

		Route::get('homepage', ['as'           => 'homepage.index', 'uses'           => 'SiteController@homepage'])->middleware('auth', 'role:Administrador');
		Route::get('homepage/create', ['as'    => 'homepage.create', 'uses'    => 'SiteController@create'])->middleware('auth', 'role:Administrador');
		Route::post('homepage/create', ['as'   => 'homepage.home_store', 'uses'   => 'SiteController@home_store'])->middleware('auth', 'role:Administrador');
		Route::get('homepage/{id}', ['as'      => 'homepage.show', 'uses'      => 'SiteController@show'])->middleware('auth', 'role:Administrador');
		Route::get('homepage/{id}/edit', ['as' => 'homepage.edit', 'uses' => 'SiteController@edit'])->middleware('auth', 'role:Administrador');
		Route::patch('homepage/{id}', ['as'    => 'homepage.update', 'uses'    => 'SiteController@update'])->middleware('auth', 'role:Administrador');
		Route::delete('homepage/{id}', ['as'   => 'homepage.destroy', 'uses'   => 'SiteController@destroy'])->middleware('auth', 'role:Administrador');

		Route::get('pages/index',  ['as'          => 'pages.index',  'uses'          => 'PagesController@indexpagestemplate'])->middleware('auth', 'role:Administrador');
		Route::get('pages/create', ['as'          => 'pages.create', 'uses'         => 'PagesController@create'])->middleware('auth', 'role:Administrador');
		Route::post('pages/create', ['as'         => 'pages.store', 'uses'         => 'PagesController@store'])->middleware('auth', 'role:Administrador');





//////////////////// Rutas Clientes /////////////////////////////

	
	$moduleName = 'Clients';

	Route::match(array('get','post'),'/clients', array('as' => 'Clients_index', 'uses' => 'ClientsController@index', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');

	Route::get('clients/{id}/delete', ['as'   => 'Clients_delete', 'uses'   => 'ClientsController@clientsDelete'])->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');
	
	Route::match(array('get','post'),'/contracts', array('as' => 'Clients_contracts', 'uses' => 'ClientsController@contracts', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');
	
//	Route::match(array('get','post'),'/companies', array('as' => 'Clients_companies', 'uses' => 'ClientsController@companies', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');

	Route::get('companies',  ['as'          => 'companies.marca',  'uses'          => 'ClientsController@companies'])->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');

	Route::match(array('get','post'),'/companies/{companyId}', array('as' => 'Clients_companies_edit', 'uses' => 'ClientsController@companiesEdit', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');

	Route::match(array('get','post'),'/contracts/{contractId}', array('as' => 'Clients_contracts_edit', 'uses' => 'ClientsController@contractsEdit', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');
	
	Route::get('contracts/{contractId}/delete', ['as'   => 'Clients_contracts_delete', 'uses'   => 'ClientsController@contractsDelete'])->middleware('auth', 'role:Administrador|Editor empresa|Administrador de leads');



//////////////////// Rutas Clientes /////////////////////////////



//////////////////// Rutas Leads /////////////////////////////

	$moduleName = 'Leads';

	Route::match(array('get','post'),'/leads', array('as' => 'Leads_index', 'uses' => 'LeadsController@index', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Administrador de leads');

	Route::match(array('get','post'),'/leads/{id}/detail', array('as' => 'Leads_detail', 'uses' => 'LeadsController@detail', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Administrador de leads');
	
	Route::match(array('get','post'),'/leads/{id}/awake', array('as' => 'Leads_awake', 'uses' => 'LeadsController@awake', 'module' => $moduleName))->middleware('auth', 'role:Administrador|Administrador de leads');
	
	Route::delete('leads/{id}', ['as'   => 'leads.destroy', 'uses'   => 'LeadsController@destroy'])->middleware('auth', 'role:Administrador|Administrador de leads');

	Route::get('export-leads', ['as' => 'leads.export', 'uses' => 'LeadsController@export'])->middleware('auth', 'role:Administrador|Administrador de leads');


//////////////////// Rutas Leads /////////////////////////////






    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
